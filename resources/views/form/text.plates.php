<label><?=trans($lang . '.' . $name)?><span class="text-danger">*</span></label>

<input type="text" name="<?=$name?>" value="<?=old('<?=$name?>')?>" class="form-control <?=($errors->has('<?=$name?>'))?'parsley-error':''?>"  data-parsley-required="true">
<ul class="parsley-error-list">
    <?php foreach ($errors->get('<?=$name?>') as $message):?>
        <li class="required" style="display: list-item;"><?=$message?></li>
    <?php endforeach?>
</ul>