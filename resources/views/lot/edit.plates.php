<?php $this->layout('layout/main') ?>        
<?=$main_menu?>

<section class="wrapper retracted scrollable">
       
    <?=$panel?>
    
    <div class="row">
        <div class="col-md-12">
                  
            <div class="panel panel-default panel-block">
                <form action="<?=url('/lots/edit/' . $lot->id)?>" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate>
                    <?=csrf_field()?>
                    <input type="hidden" id="lot_id" name="lot_id" value="<?=$lot->id?>">
                    <div class="panel panel-default panel-block">
                        <div class="list-group">
                            <div class="list-group-item">
            
                                <div class="form-group">

                                    <label>Номер лота<span class="text-danger">*</span></label>

                                    <input type="text" name="number" value="<?=(old('number'))?old('number'):$lot->number?>" class="form-control <?=($errors->has('number'))?'parsley-error':''?>"  data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('number') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    <br />
                                    <label>Исполнитель СМР<span class="text-danger">*</span></label>

                                    <select class="select2" name="executor_id" id="executor_select">
                                        <?php foreach ($executor as $item):?>
                                            <option value="<?=$item->id?>"><?=$item->name?></option>
                                        <?php endforeach ?>
                                    </select>
                                    <button type="button" class="btn btn-success" href="#addUserModal" data-toggle="modal">Добавить пользователя</button>

                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('executor_id') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    <br />

                                    <label>Номер договора с исполнителем<span class="text-danger">*</span></label>

                                    <input type="text" name="contract_number" value="<?=(old('contract_number'))?old('contract_number'):$lot->contract_number?>" class="form-control <?=($errors->has('contract_number'))?'parsley-error':''?>"  data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('contract_number') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>

                                    <label>Стоимость по договору</label>

                                    <input type="text" name="cost_contract" value="<?=(old('cost_contract'))?old('cost_contract'):$lot->cost_contract?>" class="form-control <?=($errors->has('cost_contract'))?'parsley-error':''?>"  data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('cost_contract') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>

                                    <label>Стоимость по начально-максимальному значению</label>

                                    <input type="text" name="cost_initial_maximum" value="<?=(old('cost_initial_maximum'))?old('cost_initial_maximum'):$lot->cost_initial_maximum?>" class="form-control <?=($errors->has('cost_initial_maximum'))?'parsley-error':''?>"  data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('cost_initial_maximum') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>

                                    <label>Коэффициент падения стоимости</label>

                                    <input type="text" disabled="disabled" name="fall_factor_cost" value="<?=$lot->fall_factor_cost?>" class="form-control"  data-parsley-required="true">
                                </div>
 
                            </div>
                        </div>
                        <footer class="panel-footer text-right">
                            <button type="button" class="btn btn-success btn-redirect" data-href="<?=url('lots')?>">Назад</button>
                            <button type="submit" class="btn btn-success">Сохранить</button>
                        </footer>
                    </div>
                </form>                	
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default panel-block">
                <div class="panel-heading">
                    <div>
                        <h1>Объекты лота</h1>
                        <div class="pull-right">
                            <a class="btn btn-primary btn-sm" href="#searchObjectModal" data-toggle="modal">Выбрать объект</a>
                            <a class="btn btn-primary btn-sm" href="#addObjectModal" data-toggle="modal">Добавить объект</a>
                        </div>
                    </div>
                </div>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Населенный пункт</th>
                            <th>Улица</th>
                            <th>Дом</th>
                            <th>Ресурсная ведомость</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($collection as $item):?>
                            <tr class="gradeX">
                                <td><a href="<?=url('/objects/edit/' . $item->id)?>"><?=$item->id?></a></td>
                                <td><?=$item->city_name?></td>
                                <td><?=$item->street?></td>
                                <td><?=$item->house_number?></td>
                                <td><?=($item->file_processed)?'<span class="green">Загружена</span>':'<span class="red">Обработка...</span>'?></td>
                                <td>

                                    <form action="<?=url('/lots/remove_object/' . $item->id)?>" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate>
                                        <?=csrf_field()?>
                                        <input type="hidden" name="lot_id" value="<?=$lot->id?>"/>
                                        <button type="submit" class="btn-confirm btn btn-sm btn-primary">Удалить</button>
                                    </form>

                                </td>
                            </tr>
                        <?php endforeach?>
                        
                    </tbody>
                </table>
            <?=$collection->render()?>
            </div>
        </div>
    </div>

</section>

<div id="searchObjectModal" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Объекты</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped" id="objects_list">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Населенный пункт</th>
                            <th>Улица</th>
                            <th>Дом</th>
                            <th>Действие</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($searchObject as $item):?>
                            <tr class="gradeX">
                                <td><a href="<?=url('/objects/edit/' . $item->id)?>"><?=$item->id?></a></td>
                                <td><?=$item->city_name?></td>
                                <td><?=$item->street?></td>
                                <td><?=$item->house_number?></td>
                                <td>
                                    <form action="<?=url('/lots/add_object/' . $item->id)?>" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate>
                                        <?=csrf_field()?>
                                        <input type="hidden" name="lot_id" value="<?=$lot->id?>">
                                        <input type="hidden" name="object_id" value="<?=$item->id?>">
                                        <button type="submit" class="btn btn-sm btn-primary">Выбрать</button>
                                    </form>
                                </td>
                            </tr>
                        <?php endforeach?>
                        
                    </tbody>
                </table>
                <div class="ajax_pagin">
                    <?=$searchObject->render()?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-lg btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="addObjectModal" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Новый объект</h4>
                <div class="loading">
                    <img src="/images/2.gif" height="30" width="30" />
                </div>
            </div>
            <div class="modal-body">
                    <form action="<?=url('/lots/addAJAX')?>" method="post" enctype="multipart/form-data" id="add_object" data-parsley-namespace="data-parsley-" data-parsley-validate>
                        <?=csrf_field()?>
                        <input type="hidden" id="lot_id" name="lot_id" value="<?=$lot->id?>">
                        <div class="panel panel-default panel-block">
                            <div class="list-group">
                                <div class="list-group-item">
                                    <div class="form-group">

                                        <?php foreach(App\Object::$visibleFields as $field):?>

                                            <?=$this->insert('form/' . Helpers::getInputType($field), [
                                                'name'   => $field,
                                                'errors' => $errors,
                                                'lang'   => 'object'
                                            ])?>                                            

                                        <?php endforeach?>
                                        
                                        <label>Населенный пункт<span class="text-danger">*</span></label>

                                        <select class="select2" name="city_id">
                                            <?php foreach ($city as $item):?>
                                                <option value="<?=$item->id?>"><?=$item->name?></option>
                                            <?php endforeach ?>
                                        </select>
                                        <ul id="parsley-6099321319480859" class="parsley-error-list">
                                            <?php foreach ($errors->get('city_id') as $message):?>
                                                <li class="required" style="display: list-item;"><?=$message?></li>
                                            <?php endforeach?>
                                        </ul>

                                        <label>ФИО ответственного лица</label>

                                        <input type="text" name="person_charge" value="<?=old('person_charge')?>" class="form-control <?=($errors->has('person_charge'))?'parsley-error':''?>"  data-parsley-required="true">
                                        <ul id="parsley-6099321319480859" class="parsley-error-list">
                                            <?php foreach ($errors->get('person_charge') as $message):?>
                                                <li class="required" style="display: list-item;"><?=$message?></li>
                                            <?php endforeach?>
                                        </ul>

                                        <label>Источник финансирования</label>

                                        <input type="text" name="funding" value="<?=old('funding')?>" class="form-control <?=($errors->has('funding'))?'parsley-error':''?>"  data-parsley-required="true">
                                        <ul id="parsley-6099321319480859" class="parsley-error-list">
                                            <?php foreach ($errors->get('funding') as $message):?>
                                                <li class="required" style="display: list-item;"><?=$message?></li>
                                            <?php endforeach?>
                                        </ul>

                                        <label>Телефон ответственного лица</label>

                                        <input type="text" name="phone_person_charge" value="<?=old('phone_person_charge')?>" class="form-control <?=($errors->has('phone_person_charge'))?'parsley-error':''?>"  data-parsley-required="true">
                                        <ul id="parsley-6099321319480859" class="parsley-error-list">
                                            <?php foreach ($errors->get('phone_person_charge') as $message):?>
                                                <li class="required" style="display: list-item;"><?=$message?></li>
                                            <?php endforeach?>
                                        </ul>

                                        <div class="form-control uneditable-input span3" data-trigger="fileinput">
                                            <i class="icon-file fileinput-exists"></i> 
                                            <span class="fileinput-filename"></span>
                                        </div>
                                        <span class="input-group-addon btn btn-primary btn-file">
                                            <span class="fileinput-new">Select file</span>
                                            <span class="fileinput-exists">Change</span>
                                            <input type="file" name="userfile" id="userfile">
                                        </span>
                                       <ul id="parsley-6099321319480859" class="parsley-error-list">
                                            <?php foreach ($errors->get('userfile') as $message):?>
                                                <li class="required" style="display: list-item;"><?=$message?></li>
                                            <?php endforeach?>
                                        </ul>                           
                         
                                    </div>
     
                                </div>
                            </div>
                            <footer class="panel-footer text-right">
                                <button type="button" class="btn btn-success btn-submit">Сохранить</button>
                            </footer>
                        </div>
                    </form>     
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-lg btn-default" data-dismiss="modal" id="addObjectModalClose">Закрыть</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="addUserModal" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Новый пользователь</h4>
            </div>
            <div class="modal-body">
                    <form action="/users/add" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate id="addUserForm">
                        <?=csrf_field()?>
                        <input type="hidden" id="role_id" name="role_id" value="4">
                        <div class="panel panel-default panel-block">
                            <div class="list-group">
                                <div class="list-group-item">
                                    <div class="form-group">

                                        <label>Логин<span class="text-danger">*</span></label>
                                        <input type="text" name="name" value="" class="form-control" data-parsley-required="true">

                                        <label>Почта<span class="text-danger">*</span></label>
                                        <input type="text" name="email" value="" class="form-control"  data-parsley-required="true">

                                        <label>Пароль<span class="text-danger">*</span></label>
                                        <input type="password" name="password" value="" class="form-control"  data-parsley-required="true">

                                        <label>Подтверждение пароля<span class="text-danger">*</span></label>
                                        <input type="password" name="password_confirmation" value=""  class="form-control" data-parsley-required="true">

                                        <label>Название организации<span class="text-danger">*</span></label>
                                        <input type="text" name="organization_name" value="" class="form-control" data-parsley-required="true">

                                        <label>Адрес организации<span class="text-danger">*</span></label>
                                        <input type="text" name="organization_address" value="" class="form-control"  data-parsley-required="true">

                                        <label>Телефон<span class="text-danger">*</span></label>
                                        <input type="text" name="phone" value="" class="form-control" data-parsley-required="true">

                                        <label>ИНН<span class="text-danger">*</span></label>
                                        <input type="text" name="inn" value="" class="form-control" data-parsley-required="true">

                                        <label>КПП<span class="text-danger">*</span></label>
                                        <input type="text" name="ppc" value="" class="form-control" data-parsley-required="true">

                                        
                                        <label>ОГРН<span class="text-danger">*</span></label>
                                        <input type="text" name="bin" value="" class="form-control" data-parsley-required="true">

                                        <label>Руководитель организации<span class="text-danger">*</span></label>
                                        <input type="text" name="head" value="" class="form-control" data-parsley-required="true">

                                        
                                        <label>Главный бухгалтер организации<span class="text-danger">*</span></label>
                                        <input type="text" name="chief_accountant" value="" class="form-control" data-parsley-required="true">

                                        <label>Расчетный счет<span class="text-danger">*</span></label>
                                        <input type="text" name="checking_account" value="" class="form-control" data-parsley-required="true">

                                        <label>Наименование банка<span class="text-danger">*</span></label>
                                        <input type="text" name="bank" value="" class="form-control" data-parsley-required="true">

                                        <label>БИК банка<span class="text-danger">*</span></label>
                                        <input type="text" name="bic_bank" value="" class="form-control" data-parsley-required="true">

                                        <label>ИНН Банка<span class="text-danger">*</span></label>
                                        <input type="text" name="inn_bank" value="" class="form-control" data-parsley-required="true">

                                        <label>Корреспондентсикй счет<span class="text-danger">*</span></label>
                                        <input type="text" name="correspondent_account" value="" class="form-control" data-parsley-required="true">
                                    </div>
     
                                </div>
                            </div>
                            <footer class="panel-footer text-right">

                                <button type="button" id="addUserSubmit" class="btn btn-success">Сохранить</button>
                            </footer>
                        </div>
                    </form>          
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-lg btn-default" data-dismiss="modal" id="addUserModalClose">Закрыть</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="/scripts/bootstrap.min.js"></script>

<!-- Proton base scripts: -->

<script src="/scripts/main.js"></script>
<script src="/scripts/proton/common.js"></script>
<script src="/scripts/proton/main-nav.js"></script>
<script src="/scripts/proton/user-nav.js"></script>



<!-- Page-specific scripts: -->
<script src="/scripts/proton/sidebar.js"></script>
<script src="/scripts/proton/tables.js"></script>
<!-- jsTree -->
<script src="/scripts/vendor/jquery.jstree.js"></script>
<!-- Data Tables -->
<!-- http://datatables.net/ -->
<script src="/scripts/vendor/jquery.dataTables.min.js"></script>

<!-- Data Tables for BS3 -->
<!-- https://github.com/Jowin/Datatables-Bootstrap3/ -->
<!-- NOTE: Original JS file is modified -->
    <script src="/scripts/vendor/datatables.js"></script>
<!-- Select2 Required To Style Datatable Select Box(es) -->
<!-- https://github.com/fk/select2-bootstrap-css -->
<script src="/scripts/vendor/select2.min.js"></script>
<script src="/scripts/vendor/jquery.uniform.min.js"></script>