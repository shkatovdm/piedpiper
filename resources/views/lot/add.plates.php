<?php $this->layout('layout/main') ?>        
<?=$main_menu?>

<section class="wrapper retracted scrollable">
       
    <?=$panel?>
    
    <div class="row">
        <div class="col-md-12">
          
            <div class="panel panel-default panel-block">
                    <form action="/lots/add" method="post" enctype="multipart/form-data" data-parsley-namespace="data-parsley-" data-parsley-validate>
                        <?=csrf_field()?>
                        <div class="panel panel-default panel-block">
                            <div class="list-group">
                                <div class="list-group-item">
                                    <div class="form-group">


                                        <label>Номер лота<span class="text-danger">*</span></label>

                                        <input type="text" name="number" value="<?=old('number')?>" class="form-control <?=($errors->has('number'))?'parsley-error':''?>"  data-parsley-required="true">
                                        <ul id="parsley-6099321319480859" class="parsley-error-list">
                                            <?php foreach ($errors->get('number') as $message):?>
                                                <li class="required" style="display: list-item;"><?=$message?></li>
                                            <?php endforeach?>
                                        </ul>
                                        <br />
                                        <label>Исполнитель СМР<span class="text-danger">*</span></label>

                                        <select class="select2" name="executor_id" id="executor_select">
                                            <?php foreach ($executor as $item):?>
                                                <option value="<?=$item->id?>"><?=$item->org_name?></option>
                                            <?php endforeach ?>
                                        </select>
                                        <button type="button" class="btn btn-success" href="#addUserModal" data-toggle="modal">Добавить пользователя</button>

                                        <ul id="parsley-6099321319480859" class="parsley-error-list">
                                            <?php foreach ($errors->get('executor_id') as $message):?>
                                                <li class="required" style="display: list-item;"><?=$message?></li>
                                            <?php endforeach?>
                                        </ul>
                                        <br />
                                        
                                        <label>Номер договора с исполнителем<span class="text-danger">*</span></label>

                                        <input type="text" name="contract_number" value="<?=old('contract_number')?>" class="form-control <?=($errors->has('contract_number'))?'parsley-error':''?>"  data-parsley-required="true">
                                        <ul id="parsley-6099321319480859" class="parsley-error-list">
                                            <?php foreach ($errors->get('contract_number') as $message):?>
                                                <li class="required" style="display: list-item;"><?=$message?></li>
                                            <?php endforeach?>
                                        </ul>

                                        <label>Стоимость по договору</label>

                                        <input type="text" name="cost_contract" value="<?=old('cost_contract')?>" class="form-control <?=($errors->has('cost_contract'))?'parsley-error':''?>"  data-parsley-required="true">
                                        <ul id="parsley-6099321319480859" class="parsley-error-list">
                                            <?php foreach ($errors->get('cost_contract') as $message):?>
                                                <li class="required" style="display: list-item;"><?=$message?></li>
                                            <?php endforeach?>
                                        </ul>

                                        <label>Стоимость по начально-максимальному значению</label>

                                        <input type="text" name="cost_initial_maximum" value="<?=old('cost_initial_maximum')?>" class="form-control <?=($errors->has('cost_initial_maximum'))?'parsley-error':''?>"  data-parsley-required="true">
                                        <ul id="parsley-6099321319480859" class="parsley-error-list">
                                            <?php foreach ($errors->get('cost_initial_maximum') as $message):?>
                                                <li class="required" style="display: list-item;"><?=$message?></li>
                                            <?php endforeach?>
                                        </ul>
                                               
                                    </div>
     
                                </div>
                            </div>
                            <footer class="panel-footer text-right">
                                <button type="submit" class="btn btn-success">Сохранить</button>
                            </footer>
                        </div>
                    </form>                	
            </div>
        </div>
    </div>

</section>

<div id="addUserModal" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Новый пользователь</h4>
            </div>
            <div class="modal-body">
                    <form action="/users/add" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate id="addUserForm">
                        <?=csrf_field()?>
                        <input type="hidden" id="role_id" name="role_id" value="4">
                        <div class="panel panel-default panel-block">
                            <div class="list-group">
                                <div class="list-group-item">
                                    <div class="form-group">

                                        <label>Логин<span class="text-danger">*</span></label>
                                        <input type="text" name="name" value="" class="form-control" data-parsley-required="true">

                                        <label>Почта<span class="text-danger">*</span></label>
                                        <input type="text" name="email" value="" class="form-control"  data-parsley-required="true">

                                        <label>Пароль<span class="text-danger">*</span></label>
                                        <input type="password" name="password" value="" class="form-control"  data-parsley-required="true">

                                        <label>Подтверждение пароля<span class="text-danger">*</span></label>
                                        <input type="password" name="password_confirmation" value=""  class="form-control" data-parsley-required="true">

                                        <label>Название организации<span class="text-danger">*</span></label>
                                        <input type="text" name="organization_name" value="" class="form-control" data-parsley-required="true">

                                        <label>Адрес организации<span class="text-danger">*</span></label>
                                        <input type="text" name="organization_address" value="" class="form-control"  data-parsley-required="true">

                                        <label>Телефон<span class="text-danger">*</span></label>
                                        <input type="text" name="phone" value="" class="form-control" data-parsley-required="true">

                                        <label>ИНН<span class="text-danger">*</span></label>
                                        <input type="text" name="inn" value="" class="form-control" data-parsley-required="true">

                                        <label>КПП<span class="text-danger">*</span></label>
                                        <input type="text" name="ppc" value="" class="form-control" data-parsley-required="true">

                                        
                                        <label>ОГРН<span class="text-danger">*</span></label>
                                        <input type="text" name="bin" value="" class="form-control" data-parsley-required="true">

                                        <label>Руководитель организации<span class="text-danger">*</span></label>
                                        <input type="text" name="head" value="" class="form-control" data-parsley-required="true">

                                        
                                        <label>Главный бухгалтер организации<span class="text-danger">*</span></label>
                                        <input type="text" name="chief_accountant" value="" class="form-control" data-parsley-required="true">

                                        <label>Расчетный счет<span class="text-danger">*</span></label>
                                        <input type="text" name="checking_account" value="" class="form-control" data-parsley-required="true">

                                        <label>Наименование банка<span class="text-danger">*</span></label>
                                        <input type="text" name="bank" value="" class="form-control" data-parsley-required="true">

                                        <label>БИК банка<span class="text-danger">*</span></label>
                                        <input type="text" name="bic_bank" value="" class="form-control" data-parsley-required="true">

                                        <label>ИНН Банка<span class="text-danger">*</span></label>
                                        <input type="text" name="inn_bank" value="" class="form-control" data-parsley-required="true">

                                        <label>Корреспондентсикй счет<span class="text-danger">*</span></label>
                                        <input type="text" name="correspondent_account" value="" class="form-control" data-parsley-required="true">
                                    </div>
     
                                </div>
                            </div>
                            <footer class="panel-footer text-right">

                                <button type="button" id="addUserSubmit" class="btn btn-success">Сохранить</button>
                            </footer>
                        </div>
                    </form>          
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-lg btn-default" data-dismiss="modal" id="addUserModalClose">Закрыть</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="/scripts/bootstrap.min.js"></script>

<!-- Proton base scripts: -->

<script src="/scripts/main.js"></script>
<script src="/scripts/proton/common.js"></script>
<script src="/scripts/proton/main-nav.js"></script>
<script src="/scripts/proton/user-nav.js"></script>



<!-- Page-specific scripts: -->
<script src="/scripts/proton/sidebar.js"></script>
<script src="/scripts/proton/tables.js"></script>
<!-- jsTree -->
<script src="/scripts/vendor/jquery.jstree.js"></script>
<!-- Data Tables -->
<!-- http://datatables.net/ -->
<script src="/scripts/vendor/jquery.dataTables.min.js"></script>

<!-- Data Tables for BS3 -->
<!-- https://github.com/Jowin/Datatables-Bootstrap3/ -->
<!-- NOTE: Original JS file is modified -->
    <script src="/scripts/vendor/datatables.js"></script>
<!-- Select2 Required To Style Datatable Select Box(es) -->
<!-- https://github.com/fk/select2-bootstrap-css -->
<script src="/scripts/vendor/select2.min.js"></script>
<script src="/scripts/vendor/jquery.uniform.min.js"></script>