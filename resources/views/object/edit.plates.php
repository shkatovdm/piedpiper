<?php $this->layout('layout/main') ?>        
<?=$main_menu?>

<section class="wrapper retracted scrollable">
       
    <?=$panel?>
    
    <div class="row">
        <div class="col-md-12">
                  
            <div class="panel panel-default panel-block">
                <form action="<?=url('/objects/edit/' . $entity->id)?>" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate>
                    <?=csrf_field()?>
                    <div class="panel panel-default panel-block">
                        <div class="list-group">
                            <div class="list-group-item">
            
                                <div class="form-group">

                                        <label>Населенный пункт<span class="text-danger">*</span></label>

                                        <select class="select2" name="city_id">
                                            <?php foreach ($city as $item):?>
                                                <option value="<?=$item->id?>"><?=$item->name?></option>
                                            <?php endforeach ?>
                                        </select>
                                        <ul id="parsley-6099321319480859" class="parsley-error-list">
                                            <?php foreach ($errors->get('city_id') as $message):?>
                                                <li class="required" style="display: list-item;"><?=$message?></li>
                                            <?php endforeach?>
                                        </ul>

                                        <label>Исполнитель<span class="text-danger">*</span></label>

                                        <select class="select2" name="user_id">
                                            <?php foreach ($executor as $item):?>
                                                <option value="<?=$item->id?>"><?=$item->name?></option>
                                            <?php endforeach ?>
                                        </select>
                                        <ul id="parsley-6099321319480859" class="parsley-error-list">
                                            <?php foreach ($errors->get('city_id') as $message):?>
                                                <li class="required" style="display: list-item;"><?=$message?></li>
                                            <?php endforeach?>
                                        </ul>

                                        <label>Улица<span class="text-danger">*</span></label>

                                        <input type="text" name="street" value="<?=(old('street'))?old('street'):$entity->street?>" class="form-control <?=($errors->has('street'))?'parsley-error':''?>"  data-parsley-required="true">
                                        <ul id="parsley-6099321319480859" class="parsley-error-list">
                                            <?php foreach ($errors->get('street') as $message):?>
                                                <li class="required" style="display: list-item;"><?=$message?></li>
                                            <?php endforeach?>
                                        </ul>

                                        <label>Дом<span class="text-danger">*</span></label>

                                        <input type="text" name="house_number" value="<?=(old('house_number'))?old('house_number'):$entity->house_number?>" class="form-control <?=($errors->has('house_number'))?'parsley-error':''?>"  data-parsley-required="true">
                                        <ul id="parsley-6099321319480859" class="parsley-error-list">
                                            <?php foreach ($errors->get('house_number') as $message):?>
                                                <li class="required" style="display: list-item;"><?=$message?></li>
                                            <?php endforeach?>
                                        </ul>

                                        <label>Корпус</label>

                                        <input type="text" name="housing" value="<?=(old('housing'))?old('housing'):$entity->housing?>" class="form-control <?=($errors->has('housing'))?'parsley-error':''?>"  data-parsley-required="true">
                                        <ul id="parsley-6099321319480859" class="parsley-error-list">
                                            <?php foreach ($errors->get('housing') as $message):?>
                                                <li class="required" style="display: list-item;"><?=$message?></li>
                                            <?php endforeach?>
                                        </ul>

                                        <label>Строение</label>

                                        <input type="text" name="building" value="<?=(old('building'))?old('building'):$entity->building?>" class="form-control <?=($errors->has('building'))?'parsley-error':''?>"  data-parsley-required="true">
                                        <ul id="parsley-6099321319480859" class="parsley-error-list">
                                            <?php foreach ($errors->get('building') as $message):?>
                                                <li class="required" style="display: list-item;"><?=$message?></li>
                                            <?php endforeach?>
                                        </ul>

                                        <label>ФИО ответственного лица</label>

                                        <input type="text" name="person_charge" value="<?=(old('person_charge'))?old('person_charge'):$entity->person_charge?>" class="form-control <?=($errors->has('person_charge'))?'parsley-error':''?>"  data-parsley-required="true">
                                        <ul id="parsley-6099321319480859" class="parsley-error-list">
                                            <?php foreach ($errors->get('person_charge') as $message):?>
                                                <li class="required" style="display: list-item;"><?=$message?></li>
                                            <?php endforeach?>
                                        </ul>

                                        <label>Источник финансирования</label>

                                        <input type="text" name="funding" value="<?=(old('funding'))?old('funding'):$entity->funding?>" class="form-control <?=($errors->has('funding'))?'parsley-error':''?>"  data-parsley-required="true">
                                        <ul id="parsley-6099321319480859" class="parsley-error-list">
                                            <?php foreach ($errors->get('funding') as $message):?>
                                                <li class="required" style="display: list-item;"><?=$message?></li>
                                            <?php endforeach?>
                                        </ul>

                                        <label>Телефон ответственного лица</label>

                                        <input type="text" name="phone_person_charge" value="<?=(old('phone_person_charge'))?old('phone_person_charge'):$entity->phone_person_charge?>" class="form-control <?=($errors->has('phone_person_charge'))?'parsley-error':''?>"  data-parsley-required="true">
                                        <ul id="parsley-6099321319480859" class="parsley-error-list">
                                            <?php foreach ($errors->get('phone_person_charge') as $message):?>
                                                <li class="required" style="display: list-item;"><?=$message?></li>
                                            <?php endforeach?>
                                        </ul>

                                        <label>Файл ресурсной ведомости</label>
                                        <div>
                                            <a href="<?=url('/file/' . $entity->file_name)?>"><?=$entity->original_file_name?></a>
                                        </div>
 
                                </div>
 
                            </div>
                        </div>
                        <footer class="panel-footer text-right">
                            <button type="button" class="btn btn-success btn-redirect" data-href="<?=url('objects')?>">Назад</button>
                            <button type="submit" class="btn btn-success">Сохранить</button>
                        </footer>
                    </div>
                </form>                	
            </div>
        </div>
    </div>

                <div class="row">
                <div class="col-md-12">
                    <h3>Список ресурсов</h3>      
                    <div class="panel panel-default panel-block">
                        
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Код ресурса</th>
                                    <th>Наименование</th>
                                    <th>Единица измерения</th>
                                    <th>Количество</th>
                                    <th>Базисная цена</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($collection as $item):?>
                                    <tr class="gradeX">
                                        <td><?=$item->code_resource?></td>
                                        <td><?=$item->name_resource?></td>
                                        <td><?=$item->measurement?></td>
                                        <td><?=$item->quantity?></td>
                                        <td><?=$item->base_price?></td>
                                    </tr>
                                <?php endforeach?>
                                
                            </tbody>
                        </table>
                    <?=$collection->render()?>
                    </div>
                </div>
            </div>
</section>

<script src="/scripts/bootstrap.min.js"></script>

<!-- Proton base scripts: -->

<script src="/scripts/main.js"></script>
<script src="/scripts/proton/common.js"></script>
<script src="/scripts/proton/main-nav.js"></script>
<script src="/scripts/proton/user-nav.js"></script>



<!-- Page-specific scripts: -->
<script src="/scripts/proton/sidebar.js"></script>
<script src="/scripts/proton/tables.js"></script>
<!-- jsTree -->
<script src="/scripts/vendor/jquery.jstree.js"></script>
<!-- Data Tables -->
<!-- http://datatables.net/ -->
<script src="/scripts/vendor/jquery.dataTables.min.js"></script>

<!-- Data Tables for BS3 -->
<!-- https://github.com/Jowin/Datatables-Bootstrap3/ -->
<!-- NOTE: Original JS file is modified -->
    <script src="/scripts/vendor/datatables.js"></script>
<!-- Select2 Required To Style Datatable Select Box(es) -->
<!-- https://github.com/fk/select2-bootstrap-css -->
    <script src="/scripts/vendor/select2.min.js"></script>