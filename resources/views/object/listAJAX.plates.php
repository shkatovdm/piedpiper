<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>ID</th>
            <th>Населенный пункт</th>
            <th>Улица</th>
            <th>Дом</th>
            <th>Действие</th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($collection as $item):?>
            <tr class="gradeX">
                <td><a href="<?=url('/objects/edit/' . $item->id)?>"><?=$item->id?></a></td>
                <td><?=$item->city_name?></td>
                <td><?=$item->street?></td>
                <td><?=$item->house_number?></td>
                <td>
                    <form action="<?=url('/lots/add_object/' . $item->id)?>" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate>
                        <?=csrf_field()?>
                        <input type="hidden" name="lot_id" value="<?=$lot_id?>">
                        <input type="hidden" name="object_id" value="<?=$item->id?>">
                        <button type="submit" class="btn btn-sm btn-primary">Выбрать</button>
                    </form>
                </td>
            </tr>
        <?php endforeach?>
        
    </tbody>
</table>
<div class="ajax_pagin">
    <?=$collection->render()?>
</div>
                   