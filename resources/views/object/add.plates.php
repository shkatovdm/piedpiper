<?php $this->layout('layout/main') ?>        
<?=$main_menu?>

<section class="wrapper retracted scrollable">
    
   <?=$panel?>

    <form action="/objects/add" method="post" enctype="multipart/form-data" data-parsley-namespace="data-parsley-" data-parsley-validate>
        <?=csrf_field()?>

    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default panel-block">
                <div class="list-group">
                    <div class="list-group-item">
                        <div class="form-group">
                            <?php

                                $fields = Fields::getFieldsFor('object');
                                $pgroup = $fields[0]->group;
                                ?><h4><?=$pgroup?></h4><?php
                                foreach($fields as $f) :
                                    if ($f->group != $pgroup) :
                                        $pgroup = $f->group;

                            ?>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-default panel-block">
                <div class="list-group">
                    <div class="list-group-item">
                        <div class="form-group">
                            <h4><?=$f->group?></h4>
                                    <?php endif; ?>
                                <label for="field<?=$f->id?>"><?=$f->name?></label>
                                <?=Fields::createField($f->id,$f->input,$f->params,($f->input!='file'?'form-control':'').($errors->has('field'.$f->id)?' parsley-error':''),old('field'.$f->id))?>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
            </div>

            <footer class="panel-footer text-right" style="margin-bottom:40px;">
                <button type="button" class="btn btn-success btn-redirect" data-href="<?=url('/objects')?>">Назад</button>
                <button type="submit" class="btn btn-success">Сохранить</button>
            </footer>

        </div>
    </div>

    </form>

</section>

<script src="/scripts/bootstrap.min.js"></script>

<!-- Proton base scripts: -->

<script src="/scripts/main.js"></script>
<script src="/scripts/proton/common.js"></script>
<script src="/scripts/proton/main-nav.js"></script>
<script src="/scripts/proton/user-nav.js"></script>



<!-- Page-specific scripts: -->
<script src="/scripts/proton/sidebar.js"></script>
<script src="/scripts/proton/tables.js"></script>
<!-- jsTree -->
<script src="/scripts/vendor/jquery.jstree.js"></script>
<!-- Data Tables -->
<!-- http://datatables.net/ -->
<script src="/scripts/vendor/jquery.dataTables.min.js"></script>

<!-- Data Tables for BS3 -->
<!-- https://github.com/Jowin/Datatables-Bootstrap3/ -->
<!-- NOTE: Original JS file is modified -->
    <script src="/scripts/vendor/datatables.js"></script>
<!-- Select2 Required To Style Datatable Select Box(es) -->
<!-- https://github.com/fk/select2-bootstrap-css -->
<script src="/scripts/vendor/select2.min.js"></script>
<script src="/scripts/vendor/jquery.uniform.min.js"></script>