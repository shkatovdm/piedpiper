<form action="<?=url('/objects/addAJAX')?>" method="post" enctype="multipart/form-data" id="add_object" data-parsley-namespace="data-parsley-" data-parsley-validate>
    <?=csrf_field()?>
    <div class="panel panel-default panel-block">
        <div class="list-group">
            <div class="list-group-item">
                <div class="form-group">

                    <label>Населенный пункт<span class="text-danger">*</span></label>

                    <select class="select2" name="city_id">
                        <?php foreach ($city as $item):?>
                            <option value="<?=$item->id?>"><?=$item->name?></option>
                        <?php endforeach ?>
                    </select>
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('city_id') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>

                    <label>Улица<span class="text-danger">*</span></label>

                    <input type="text" name="street" value="<?=$request['street']?>" class="form-control <?=($errors->has('street'))?'parsley-error':''?>"  data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('street') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>

                    <label>Дом<span class="text-danger">*</span></label>

                    <input type="text" name="house_number" value="<?=$request['house_number']?>" class="form-control <?=($errors->has('house_number'))?'parsley-error':''?>"  data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('house_number') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>

                    <label>Корпус</label>

                    <input type="text" name="housing" value="<?=$request['housing']?>" class="form-control <?=($errors->has('housing'))?'parsley-error':''?>"  data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('housing') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>

                    <label>Строение</label>

                    <input type="text" name="building" value="<?=$request['building']?>" class="form-control <?=($errors->has('building'))?'parsley-error':''?>"  data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('building') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>

                    <label>ФИО ответственного лица</label>

                    <input type="text" name="person_charge" value="<?=$request['person_charge']?>" class="form-control <?=($errors->has('person_charge'))?'parsley-error':''?>"  data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('person_charge') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>

                    <label>Источник финансирования</label>

                    <input type="text" name="funding" value="<?=$request['funding']?>" class="form-control <?=($errors->has('funding'))?'parsley-error':''?>"  data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('funding') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>

                    <label>Телефон ответственного лица</label>

                    <input type="text" name="phone_person_charge" value="<?=$request['phone_person_charge']?>" class="form-control <?=($errors->has('phone_person_charge'))?'parsley-error':''?>"  data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('phone_person_charge') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>

                    <div class="form-control uneditable-input span3" data-trigger="fileinput">
                        <i class="icon-file fileinput-exists"></i> 
                        <span class="fileinput-filename"></span>
                    </div>
                    <span class="input-group-addon btn btn-primary btn-file">
                        <span class="fileinput-new">Select file</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="userfile" >
                    </span>
                   <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('userfile') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>                           
     
                </div>

            </div>
        </div>
        <footer class="panel-footer text-right">
            <button type="button" class="btn btn-success btn-submit">Сохранить</button>
        </footer>
    </div>
</form>     