<?php $this->layout('layout/main') ?>        
        <?=$main_menu?>
       
        <section class="wrapper retracted scrollable">
            
            <?=$panel?>            
            <div class="row">
                <div class="col-md-12">
                          
                    <div class="panel panel-default panel-block">
                    	
	                    <table class="table table-bordered table-striped sorting">
	                        <thead>
                                <tr>                                    
                                    <th data-sort="district">МО</th>
                                    <th>№ п/п</th>
                                    <th data-sort="address_condominium">Адрес МКД</th>
                                    <th data-sort="year_commissioning"><div>Год ввода в эксплуатацию</div></th>
                                    <th data-sort="area_full"><div>Общая площадь МКД</div></th>
                                    <th data-sort="area"><div>Площадь помещений МКД</div></th>
                                    <th data-sort="floors_count"><div>Количество этажей</div></th>
                                    <th><div>Количество подъездов</div></th>
                                    <th data-sort="heat_supply"><div>Теплоснабжения</div></th>
                                    <th data-sort="year_work_heat_meter"><div>Установка (замена) коллективного ПУ теплоснабжения, УУ</div></th>
                                    <th data-sort="year_work_hot_water"><div>Горячее водоснабжение</div></th>
                                    <th data-sort="year_work_hot_water_meter"><div>Установка (замена) коллективного ПУ горячего водоснабжения, УУ</div></th>
                                    <th data-sort="year_work_cold_water"><div>Холодное водоснабжение</div></th>
                                    <th data-sort="year_work_cold_water_meter"><div>Установка (замена) коллективного ПУ холодного водоснабжения, УУ</div></th>
                                    <th data-sort="year_work_sewerage"><div>Водоотведения</div></th>
                                    <th data-sort="year_work_electric"><div>Электроснабжения</div></th>
                                    <th data-sort="year_work_electric_meter"><div>Установка (замена) коллективного ПУ электроснабжения, УУ</div></th>
                                    <th data-sort="year_work_gas"><div>Газоснабжение</div></th>
                                    <th data-sort="year_work_gas_meter"><div>Установка (замена) коллективного ПУ газоснабжения, УУ</div></th>
                                    <th data-sort="year_work_roof"><div>Ремонт крыши, кровли</div></th>
                                    <th data-sort="year_work_facade"><div>Утепление и ремонт фасада</div></th>
                                    <th data-sort="year_work_lift"><div>Ремонт лифтового оборудования</div></th>
                                    <th data-sort="year_work_basement"><div>Ремонт подвальных помещений</div></th>
                                    <th data-sort="year_work_foundation"><div>Ремонт фундаментов</div></th>
                                    <th data-sort="total_cost"><div>Общая стоимость капитального ремонта (тыс. рублей)</div></th>
                                    <th>Управление</th>
                                </tr>
                            </thead>
	                        <tbody>
	                            <?php foreach ($collection as $item):?>
                                    <tr class="gradeX">
                                        <td><?=$item->district?></td>
                                        <td></td>
                                        <td><?=$item->address_condominium?></td>
                                        <td><?=$item->year_commissioning?></td>
                                        <td><?=$item->area_full?></td>
                                        <td><?=$item->area?></td>
                                        <td><?=$item->floors_count?></td>
                                        <td></td>
                                        <td><?=$item->heat_supply?></td>
                                        <td><?=$item->year_work_heat_meter?></td>
                                        <td><?=$item->year_work_hot_water?></td>
                                        <td><?=$item->year_work_hot_water_meter?></td>
                                        <td><?=$item->year_work_cold_water?></td>
                                        <td><?=$item->year_work_cold_water_meter?></td>
                                        <td><?=$item->year_work_sewerage?></td>
                                        <td><?=$item->year_work_electric?></td>
                                        <td><?=$item->year_work_electric_meter?></td>
                                        <td><?=$item->year_work_gas?></td>
                                        <td><?=$item->year_work_gas_meter?></td>
                                        <td><?=$item->year_work_roof?></td>
                                        <td><?=$item->year_work_facade?></td>
                                        <td><?=$item->year_work_lift?></td>
                                        <td><?=$item->year_work_basement?></td>
                                        <td><?=$item->year_work_foundation?></td>
                                        <td><?=$item->total_cost?></td>
                                        <td>
                                            <?php if(isset($permission['update']) && $permission['update']):?>
                                                <button class="btn btn-sm btn-primary btn-redirect btn-greed" data-href="<?=url('/objects/edit/' . $item->id)?>">Редактировать</button>
                                            <?php endif?>

                                            <?php if(isset($delete) && $delete):?>
                                                <form action="<?=url('/objects/remove/' . $item->id)?>" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate>
                                                    <?=csrf_field()?>
                                                    <button type="submit" class="btn-confirm btn btn-sm btn-primary">Удалить</button>
                                                </form>
                                            <?php endif?>
                                        </td>
                                    </tr>
                                <?php endforeach?>
                                
	                        </tbody>
	                    </table>
                        <?php if($sort !== false):?>
                            <?=$collection->appends('sort',$sort)->render()?>
                        <?php else:?>
                            <?=$collection->render()?>
                        <?php endif;?>
                    </div>
                </div>
            </div>
        </section>

        <script src="/scripts/bootstrap.min.js"></script>

		<!-- Proton base scripts: -->
        
        <script src="/scripts/main.js"></script>
		<script src="/scripts/proton/common.js"></script>
		<script src="/scripts/proton/main-nav.js"></script>
		<script src="/scripts/proton/user-nav.js"></script>
		


        <!-- Page-specific scripts: -->
        <script src="/scripts/proton/sidebar.js"></script>
        <script src="/scripts/proton/tables.js"></script>
        <!-- jsTree -->
        <script src="/scripts/vendor/jquery.jstree.js"></script>
        <!-- Data Tables -->
        <!-- http://datatables.net/ -->
        <script src="/scripts/vendor/jquery.dataTables.min.js"></script>
        
        <!-- Data Tables for BS3 -->
        <!-- https://github.com/Jowin/Datatables-Bootstrap3/ -->
        <!-- NOTE: Original JS file is modified -->
            <script src="/scripts/vendor/datatables.js"></script>
        <!-- Select2 Required To Style Datatable Select Box(es) -->
        <!-- https://github.com/fk/select2-bootstrap-css -->
            <script src="/scripts/vendor/select2.min.js"></script>