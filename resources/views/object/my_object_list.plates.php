<?php $this->layout('layout/main') ?>        
        <?=$main_menu?>
       
        <section class="wrapper retracted scrollable">
            
           <?=$panel?>            
            <div class="row">
                <div class="col-md-12">
                          
	                <?php foreach ($lots as $lot):?>
                        <h2>Лот: <?=$lot->number?></h2>
                        <div>Договор №: <?=$lot->contract_number?></div>
                        <div>Стоимость по договору: <?=$lot->cost_contract?></div>
                        <div>Стоимость по начально максимальному значению: <?=$lot->cost_initial_maximum?></div>
	                   <br />
	                    <div class="panel panel-default panel-block">
		                    <table class="table table-bordered table-striped">
		                        <thead>
		                            <tr>
		                                <th>ID</th>
	                                    <th>Населенный пункт</th>
	                                    <th>Улица</th>
                                        <th>Дом</th>
                                        <th>Количество позиций</th>
	                                    <th>Действие</th>
		                            </tr>
		                        </thead>
		                        <tbody>
		                            <?php foreach ($collection[$lot->id] as $item):?>
	                                    <tr class="gradeX">
	                                        <td><a href="<?=url('/objects/edit/' . $item->id)?>"><?=$item->id?></a></td>
	                                        <td><?=$item->city_name?></td>
	                                        <td><?=$item->street?></td>
                                            <td><?=$item->house_number?></td>
	                                        <td><?=$item->total?></td>
	                                        <td>
	                                           <button class="btn btn-sm btn-primary btn-redirect btn-greed" data-href="<?=url('/objects/order/' . $item->id)?>">Подбор номенклатуры</button>
	                                        </td>
	                                    </tr>
	                                <?php endforeach?>
	                                
		                        </tbody>
		                    </table>
	                    	<?=$collection[$lot->id]->render()?>
	                    </div>
                    <?php endforeach?>
	                <?=$lots->render()?>
                </div>
            </div>
        </section>

        <script src="/scripts/bootstrap.min.js"></script>

		<!-- Proton base scripts: -->
        
        <script src="/scripts/main.js"></script>
		<script src="/scripts/proton/common.js"></script>
		<script src="/scripts/proton/main-nav.js"></script>
		<script src="/scripts/proton/user-nav.js"></script>
		


        <!-- Page-specific scripts: -->
        <script src="/scripts/proton/sidebar.js"></script>
        <script src="/scripts/proton/tables.js"></script>
        <!-- jsTree -->
        <script src="/scripts/vendor/jquery.jstree.js"></script>
        <!-- Data Tables -->
        <!-- http://datatables.net/ -->
        <script src="/scripts/vendor/jquery.dataTables.min.js"></script>
        
        <!-- Data Tables for BS3 -->
        <!-- https://github.com/Jowin/Datatables-Bootstrap3/ -->
        <!-- NOTE: Original JS file is modified -->
            <script src="/scripts/vendor/datatables.js"></script>
        <!-- Select2 Required To Style Datatable Select Box(es) -->
        <!-- https://github.com/fk/select2-bootstrap-css -->
            <script src="/scripts/vendor/select2.min.js"></script>