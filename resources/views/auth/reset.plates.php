<?php $this->layout('layout/login') ?>
    <body class="login-page">
        
        <script>
	        var theme = $.cookie('protonTheme') || 'default';
	        $('body').removeClass (function (index, css) {
	            return (css.match (/\btheme-\S+/g) || []).join(' ');
	        });
	        if (theme !== 'default') $('body').addClass(theme);
        </script>
        <!--[if lt IE 8]>
            <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <form role="form" action="/password/reset" method="post">
            <?=csrf_field()?>
            <input type="hidden" name="token" value="<?=$token?>">
            <section class="wrapper scrollable animated fadeInDown">
                <section class="panel panel-default">
                    <div class="panel-heading">
                        <div>
                            <span class="title">Сброс пароля</span>
                        </div>
                    </div>
                    <ul class="list-group">
            
                        <li class="list-group-item">
            
                            <div class="form-group">
                                <label for="email">Email</label>
                                <input type="email" name="email" value="<?=old('email')?>" class="form-control input-lg <?=($errors->has('email'))?'parsley-error':''?>" id="email" placeholder="Email">
                                <ul class="login-error-list">
                                    <?php foreach ($errors->get('email') as $message):?>
                                        <li class="required" style="display: list-item;"><?=$message?></li>
                                    <?php endforeach?>
                                </ul>
                            </div>
                            <div class="form-group">
                                <label for="password">Password</label>
                                <input type="password" name="password" value="<?=old('password')?>" class="form-control input-lg <?=($errors->has('password'))?'parsley-error':''?>" id="password" placeholder="Пароль">
                                <ul class="login-error-list">
                                    <?php foreach ($errors->get('password') as $message):?>
                                        <li class="required" style="display: list-item;"><?=$message?></li>
                                    <?php endforeach?>
                                </ul>
                            </div>
                            <div class="form-group">
                                <label for="password_confirmation">password_confirmation</label>
                                <input type="password" name="password_confirmation" value="<?=old('password_confirmation')?>" class="form-control input-lg <?=($errors->has('password_confirmation'))?'parsley-error':''?>" id="password_confirmation" placeholder="Пароль">
                                <ul class="login-error-list">
                                    <?php foreach ($errors->get('password_confirmation') as $message):?>
                                        <li class="required" style="display: list-item;"><?=$message?></li>
                                    <?php endforeach?>
                                </ul>
                            </div>
                        </li>
                    </ul>
                    <div class="panel-footer">
                        <input type="submit" class="btn btn-lg btn-success" value="Сбросить пароль">
                    </div>
                </section>
            </section>
        </form>
    </body>
</html>
