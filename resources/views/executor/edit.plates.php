<?php $this->layout('layout/main') ?>        
<?=$main_menu?>

<section class="wrapper retracted scrollable">
       
    <?=$panel?>
    
    <div class="row">
        <div class="col-md-12">
                  
            <div class="panel panel-default panel-block">
                <form action="<?=url('/executors/edit/' . $entity->id)?>" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate>
                    <?=csrf_field()?>
                    <div class="panel panel-default panel-block">
                        <div class="list-group">
                            <div class="list-group-item">
            
                                <div class="form-group">

                                    <label>Название организации<span class="text-danger">*</span></label>

                                    <input type="text" name="name" value="<?=(old('name'))?old('name'):$entity->name?>" class="form-control <?=($errors->has('name'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('name') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    <label>Адрес организации<span class="text-danger">*</span></label>

                                    <input type="text" name="address" value="<?=(old('address'))?old('address'):$entity->address?>" class="form-control <?=($errors->has('address'))?'parsley-error':''?>"  data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('address') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    <label>Телефон<span class="text-danger">*</span></label>

                                    <input type="text" name="phone" value="<?=(old('phone'))?old('phone'):$entity->phone?>" class="form-control <?=($errors->has('phone'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('phone') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    <label>ИНН<span class="text-danger">*</span></label>

                                    <input type="text" name="inn" value="<?=(old('inn'))?old('inn'):$entity->inn?>" class="form-control <?=($errors->has('inn'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('inn') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    <label>КПП<span class="text-danger">*</span></label>

                                    <input type="text" name="ppc" value="<?=(old('ppc'))?old('ppc'):$entity->ppc?>" class="form-control  <?=($errors->has('ppc'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('ppc') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    
                                    <label>ОГРН<span class="text-danger">*</span></label>

                                    <input type="text" name="bin" value="<?=(old('bin'))?old('bin'):$entity->bin?>" class="form-control <?=($errors->has('bin'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('bin') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    <label>Руководитель организации<span class="text-danger">*</span></label>

                                    <input type="text" name="head" value="<?=(old('head'))?old('head'):$entity->head?>" class="form-control <?=($errors->has('head'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('head') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    
                                    <label>Главный бухгалтер организации<span class="text-danger">*</span></label>

                                    <input type="text" name="chief_accountant" value="<?=(old('chief_accountant'))?old('chief_accountant'):$entity->chief_accountant?>" class="form-control <?=($errors->has('chief_accountant'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('chief_accountant') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    <label>Расчетный счет<span class="text-danger">*</span></label>

                                    <input type="text" name="checking_account" value="<?=(old('checking_account'))?old('checking_account'):$entity->checking_account?>" class="form-control <?=($errors->has('checking_account'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('checking_account') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    <label>Наименование банка<span class="text-danger">*</span></label>

                                    <input type="text" name="bank" value="<?=(old('bank'))?old('bank'):$entity->bank?>" class="form-control <?=($errors->has('bank'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('bank') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    <label>БИК банка<span class="text-danger">*</span></label>

                                    <input type="text" name="bic_bank" value="<?=(old('bic_bank'))?old('bic_bank'):$entity->bic_bank?>" class="form-control <?=($errors->has('bic_bank'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('bic_bank') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    <label>ИНН Банка<span class="text-danger">*</span></label>

                                    <input type="text" name="inn_bank" value="<?=(old('inn_bank'))?old('inn_bank'):$entity->inn_bank?>" class="form-control <?=($errors->has('inn_bank'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('inn_bank') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    <label>Корреспондентсикй счет<span class="text-danger">*</span></label>

                                    <input type="text" name="correspondent_account" value="<?=(old('correspondent_account'))?old('correspondent_account'):$entity->correspondent_account?>" class="form-control <?=($errors->has('correspondent_account'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('correspondent_account') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                </div>
 
                            </div>
                        </div>
                        <footer class="panel-footer text-right">
                            <button type="button" class="btn btn-success btn-redirect" data-href="<?=url('entitys')?>">Назад</button>
                            <button type="submit" class="btn btn-success">Сохранить</button>
                        </footer>
                    </div>
                </form>                	
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">

            <div class="panel panel-default panel-block">
                <div class="panel-heading">
                    <div>
                        <h1>Пользователи</h1>
                        <div class="pull-right">
                            <a class="btn btn-primary btn-sm" href="#searchUserModal" data-toggle="modal">Выбрать пользователя</a>
                            <a class="btn btn-primary btn-sm" href="#addUserModal" data-toggle="modal">Добавить пользователя</a>
                        </div>
                    </div>
                </div>
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Логин</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($users as $user):?>
                            <tr class="gradeX">
                                <td><a href="<?=url('/objects/edit/')?>"><?=$user->id?></a></td>
                                <td><?=$user->name?></td>
                                <td>

                                    <form action="<?=url('/executors/unjoinUser')?>" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate>
                                        <?=csrf_field()?>
                                        <input type="hidden" name="user_id" value="<?=$user->id?>">
                                        <button type="submit" class="btn-confirm btn btn-sm btn-primary">Удалить</button>
                                    </form>

                                </td>
                            </tr>
                        <?php endforeach?>
                        
                    </tbody>
                </table>
            <?=$users->render()?>
            </div>
        </div>
    </div>

</section>

<div id="searchUserModal" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Объекты</h4>
            </div>
            <div class="modal-body">
                <table class="table table-bordered table-striped" id="objects_list">
                    <thead>
                        <tr>
                            <th>ID</th>
                            <th>Логин</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($freeUser as $item):?>
                            <tr class="gradeX">
                                <td><a href="<?=url('/objects/edit/' . $item->id)?>"><?=$item->id?></a></td>
                                <td><?=$item->name?></td>
                                <td>
                                    <form action="<?=url('/executors/joinUser')?>" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate>
                                        <?=csrf_field()?>
                                        <input type="hidden" name="user_id" value="<?=$item->id?>">
                                        <input type="hidden" name="org_id" value="<?=$entity->id?>">
                                        <button type="submit" class="btn btn-sm btn-primary">Выбрать</button>
                                    </form>
                                </td>
                            </tr>
                        <?php endforeach?>
                        
                    </tbody>
                </table>
                <div class="ajax_pagin">
                    <?=$freeUser->render()?>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-lg btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="addUserModal" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Новый пользователь</h4>
            </div>
            <div class="modal-body">
                    <form action="/executors/addUser" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate id="addUserForm">
                        <?=csrf_field()?>
                        <input type="hidden" id="role_id" name="role_id" value="4">
                        <input type="hidden" id="org_id" name="org_id" value="<?=$entity->id?>">
                        <div class="panel panel-default panel-block">
                            <div class="list-group">
                                <div class="list-group-item">
                                    <div class="form-group">

                                        <label>Логин<span class="text-danger">*</span></label>
                                        <input type="text" name="name" value="" class="form-control" data-parsley-required="true">

                                        <label>Почта<span class="text-danger">*</span></label>
                                        <input type="text" name="email" value="" class="form-control"  data-parsley-required="true">

                                        <label>Пароль<span class="text-danger">*</span></label>
                                        <input type="password" name="password" value="" class="form-control"  data-parsley-required="true">

                                        <label>Подтверждение пароля<span class="text-danger">*</span></label>
                                        <input type="password" name="password_confirmation" value=""  class="form-control" data-parsley-required="true">

                                    </div>
     
                                </div>
                            </div>
                            <footer class="panel-footer text-right">
                                <button type="button" id="addUserSubmit" class="btn btn-success">Сохранить</button>
                            </footer>
                        </div>
                    </form>          
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-lg btn-default" data-dismiss="modal" id="addUserModalClose">Закрыть</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="/scripts/bootstrap.min.js"></script>

<!-- Proton base scripts: -->

<script src="/scripts/main.js"></script>
<script src="/scripts/proton/common.js"></script>
<script src="/scripts/proton/main-nav.js"></script>
<script src="/scripts/proton/user-nav.js"></script>



<!-- Page-specific scripts: -->
<script src="/scripts/proton/sidebar.js"></script>
<script src="/scripts/proton/tables.js"></script>
<!-- jsTree -->
<script src="/scripts/vendor/jquery.jstree.js"></script>
<!-- Data Tables -->
<!-- http://datatables.net/ -->
<script src="/scripts/vendor/jquery.dataTables.min.js"></script>

<!-- Data Tables for BS3 -->
<!-- https://github.com/Jowin/Datatables-Bootstrap3/ -->
<!-- NOTE: Original JS file is modified -->
    <script src="/scripts/vendor/datatables.js"></script>
<!-- Select2 Required To Style Datatable Select Box(es) -->
<!-- https://github.com/fk/select2-bootstrap-css -->
    <script src="/scripts/vendor/select2.min.js"></script>