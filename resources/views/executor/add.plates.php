<?php $this->layout('layout/main') ?>        
        <?=$main_menu?>
       
        <section class="wrapper retracted scrollable">
                        
            <?=$panel?>
            
            <div class="row">
                <div class="col-md-12">
                  
                    <div class="panel panel-default panel-block">
                            <form action="/providers/add" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate>
                                <?=csrf_field()?>
                                <div class="panel panel-default panel-block">
                                    <div class="list-group">
                                        <div class="list-group-item">
                                            <div class="form-group">

                                                <label>Название организации<span class="text-danger">*</span></label>

                                                <input type="text" name="organization_name" value="<?=old('organization_name')?>" class="form-control <?=($errors->has('organization_name'))?'parsley-error':''?>" data-parsley-required="true">
                                                <ul id="parsley-6099321319480859" class="parsley-error-list">
                                                    <?php foreach ($errors->get('organization_name') as $message):?>
                                                        <li class="required" style="display: list-item;"><?=$message?></li>
                                                    <?php endforeach?>
                                                </ul>
                                                <label>Адрес организации<span class="text-danger">*</span></label>

                                                <input type="text" name="organization_address" value="<?=old('organization_address')?>" class="form-control <?=($errors->has('organization_address'))?'parsley-error':''?>"  data-parsley-required="true">
                                                <ul id="parsley-6099321319480859" class="parsley-error-list">
                                                    <?php foreach ($errors->get('organization_address') as $message):?>
                                                        <li class="required" style="display: list-item;"><?=$message?></li>
                                                    <?php endforeach?>
                                                </ul>
                                                <label>Телефон<span class="text-danger">*</span></label>

                                                <input type="text" name="phone" value="<?=old('phone')?>" class="form-control <?=($errors->has('phone'))?'parsley-error':''?>" data-parsley-required="true">
                                                <ul id="parsley-6099321319480859" class="parsley-error-list">
                                                    <?php foreach ($errors->get('phone') as $message):?>
                                                        <li class="required" style="display: list-item;"><?=$message?></li>
                                                    <?php endforeach?>
                                                </ul>
                                                <label>ИНН<span class="text-danger">*</span></label>

                                                <input type="text" name="inn" value="<?=old('inn')?>" class="form-control <?=($errors->has('inn'))?'parsley-error':''?>" data-parsley-required="true">
                                                <ul id="parsley-6099321319480859" class="parsley-error-list">
                                                    <?php foreach ($errors->get('inn') as $message):?>
                                                        <li class="required" style="display: list-item;"><?=$message?></li>
                                                    <?php endforeach?>
                                                </ul>
                                                <label>КПП<span class="text-danger">*</span></label>

                                                <input type="text" name="ppc" value="<?=old('ppc')?>" class="form-control  <?=($errors->has('ppc'))?'parsley-error':''?>" data-parsley-required="true">
                                                <ul id="parsley-6099321319480859" class="parsley-error-list">
                                                    <?php foreach ($errors->get('ppc') as $message):?>
                                                        <li class="required" style="display: list-item;"><?=$message?></li>
                                                    <?php endforeach?>
                                                </ul>
                                                
                                                <label>ОГРН<span class="text-danger">*</span></label>

                                                <input type="text" name="bin" value="<?=old('bin')?>" class="form-control <?=($errors->has('bin'))?'parsley-error':''?>" data-parsley-required="true">
                                                <ul id="parsley-6099321319480859" class="parsley-error-list">
                                                    <?php foreach ($errors->get('bin') as $message):?>
                                                        <li class="required" style="display: list-item;"><?=$message?></li>
                                                    <?php endforeach?>
                                                </ul>
                                                <label>Руководитель организации<span class="text-danger">*</span></label>

                                                <input type="text" name="head" value="<?=old('head')?>" class="form-control <?=($errors->has('head'))?'parsley-error':''?>" data-parsley-required="true">
                                                <ul id="parsley-6099321319480859" class="parsley-error-list">
                                                    <?php foreach ($errors->get('head') as $message):?>
                                                        <li class="required" style="display: list-item;"><?=$message?></li>
                                                    <?php endforeach?>
                                                </ul>
                                                
                                                <label>Главный бухгалтер организации<span class="text-danger">*</span></label>

                                                <input type="text" name="chief_accountant" value="<?=old('chief_accountant')?>" class="form-control <?=($errors->has('chief_accountant'))?'parsley-error':''?>" data-parsley-required="true">
                                                <ul id="parsley-6099321319480859" class="parsley-error-list">
                                                    <?php foreach ($errors->get('chief_accountant') as $message):?>
                                                        <li class="required" style="display: list-item;"><?=$message?></li>
                                                    <?php endforeach?>
                                                </ul>
                                                <label>Расчетный счет<span class="text-danger">*</span></label>

                                                <input type="text" name="checking_account" value="<?=old('checking_account')?>" class="form-control <?=($errors->has('checking_account'))?'parsley-error':''?>" data-parsley-required="true">
                                                <ul id="parsley-6099321319480859" class="parsley-error-list">
                                                    <?php foreach ($errors->get('checking_account') as $message):?>
                                                        <li class="required" style="display: list-item;"><?=$message?></li>
                                                    <?php endforeach?>
                                                </ul>
                                                <label>Наименование банка<span class="text-danger">*</span></label>

                                                <input type="text" name="bank" value="<?=old('bank')?>" class="form-control <?=($errors->has('bank'))?'parsley-error':''?>" data-parsley-required="true">
                                                <ul id="parsley-6099321319480859" class="parsley-error-list">
                                                    <?php foreach ($errors->get('bank') as $message):?>
                                                        <li class="required" style="display: list-item;"><?=$message?></li>
                                                    <?php endforeach?>
                                                </ul>
                                                <label>БИК банка<span class="text-danger">*</span></label>

                                                <input type="text" name="bic_bank" value="<?=old('bic_bank')?>" class="form-control <?=($errors->has('bic_bank'))?'parsley-error':''?>" data-parsley-required="true">
                                                <ul id="parsley-6099321319480859" class="parsley-error-list">
                                                    <?php foreach ($errors->get('bic_bank') as $message):?>
                                                        <li class="required" style="display: list-item;"><?=$message?></li>
                                                    <?php endforeach?>
                                                </ul>
                                                <label>ИНН Банка<span class="text-danger">*</span></label>

                                                <input type="text" name="inn_bank" value="<?=old('inn_bank')?>" class="form-control <?=($errors->has('inn_bank'))?'parsley-error':''?>" data-parsley-required="true">
                                                <ul id="parsley-6099321319480859" class="parsley-error-list">
                                                    <?php foreach ($errors->get('inn_bank') as $message):?>
                                                        <li class="required" style="display: list-item;"><?=$message?></li>
                                                    <?php endforeach?>
                                                </ul>
                                                <label>Корреспондентсикй счет<span class="text-danger">*</span></label>

                                                <input type="text" name="correspondent_account" value="<?=old('correspondent_account')?>" class="form-control <?=($errors->has('correspondent_account'))?'parsley-error':''?>" data-parsley-required="true">
                                                <ul id="parsley-6099321319480859" class="parsley-error-list">
                                                    <?php foreach ($errors->get('correspondent_account') as $message):?>
                                                        <li class="required" style="display: list-item;"><?=$message?></li>
                                                    <?php endforeach?>
                                                </ul>
                                            </div>
             
                                        </div>
                                    </div>
                                    <footer class="panel-footer text-right">
                                        <button type="button" class="btn btn-success btn-redirect" data-href="<?=url('providers')?>">Назад</button>
                                        <button type="submit" class="btn btn-success">Сохранить</button>
                                    </footer>
                                </div>
                            </form>                	
                    </div>
                </div>
            </div>
        </section>

        <script src="/scripts/bootstrap.min.js"></script>

		<!-- Proton base scripts: -->
        
        <script src="/scripts/main.js"></script>
		<script src="/scripts/proton/common.js"></script>
		<script src="/scripts/proton/main-nav.js"></script>
		<script src="/scripts/proton/user-nav.js"></script>
		


        <!-- Page-specific scripts: -->
        <script src="/scripts/proton/sidebar.js"></script>
        <script src="/scripts/proton/tables.js"></script>
        <!-- jsTree -->
        <script src="/scripts/vendor/jquery.jstree.js"></script>
        <!-- Data Tables -->
        <!-- http://datatables.net/ -->
        <script src="/scripts/vendor/jquery.dataTables.min.js"></script>
        
        <!-- Data Tables for BS3 -->
        <!-- https://github.com/Jowin/Datatables-Bootstrap3/ -->
        <!-- NOTE: Original JS file is modified -->
            <script src="/scripts/vendor/datatables.js"></script>
        <!-- Select2 Required To Style Datatable Select Box(es) -->
        <!-- https://github.com/fk/select2-bootstrap-css -->
            <script src="/scripts/vendor/select2.min.js"></script>