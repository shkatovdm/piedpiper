<form action="/executors/addUser" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate id="addUserForm">
    <?=csrf_field()?>
    <input type="hidden" id="role_id" name="role_id" value="4">
    <input type="hidden" id="org_id" name="org_id" value="<?=$request['org_id']?>">
    <div class="panel panel-default panel-block">
        <div class="list-group">
            <div class="list-group-item">
                <div class="form-group">

                    <label>Логин<span class="text-danger">*</span></label>

                    <input type="text" name="name" value="<?=$request['name']?>" class="form-control <?=($errors->has('name'))?'parsley-error':''?>" data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('name') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>
                    
                    <label>Почта<span class="text-danger">*</span></label>

                    <input type="text" name="email" value="<?=$request['email']?>" class="form-control <?=($errors->has('email'))?'parsley-error':''?>"  data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('email') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>
     
                    <label>Пароль<span class="text-danger">*</span></label>

                    <input type="password" name="password" value="<?=$request['password']?>" class="form-control <?=($errors->has('password'))?'parsley-error':''?>"  data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('password') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>

                    <label>Подтверждение пароля<span class="text-danger">*</span></label>

                    <input type="password" name="password_confirmation" value="<?=$request['password_confirmation']?>" class="form-control <?=($errors->has('password_confirmation'))?'parsley-error':''?>"  data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('password_confirmation') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>
     
                </div>

            </div>
        </div>
        <footer class="panel-footer text-right">
            <button type="button" id="addUserSubmit" class="btn btn-success">Сохранить</button>
        </footer>
    </div>
</form>                	
                    