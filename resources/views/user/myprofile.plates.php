<?php $this->layout('layout/main') ?>        
<?=$main_menu?>

<section class="wrapper retracted scrollable">
       
    <?=$panel?>
    
    <div class="row">
        <div class="col-md-12">
                  
            <div class="panel panel-default panel-block">
                <form action="<?=url('/profile/edit/' . $user->id)?>" method="post" enctype="multipart/form-data" data-parsley-namespace="data-parsley-" data-parsley-validate mu>
                    <?=csrf_field()?>
                    <div class="panel panel-default panel-block">
                        <div class="list-group">
                            <div class="list-group-item">
            
                                <div class="form-group">

                                    <label>Логин<span class="text-danger">*</span></label>

                                    <input type="text" name="name" value="<?=(old('name'))?old('name'):$user->name?>" class="form-control <?=($errors->has('name'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('name') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    
                                    <label>Почта<span class="text-danger">*</span></label>

                                    <input type="text" name="email" value="<?=(old('email'))?old('email'):$user->email?>" class="form-control <?=($errors->has('email'))?'parsley-error':''?>"  data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('email') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>

                                    <label>Название организации<span class="text-danger">*</span></label>

                                    <input type="text" name="name" value="<?=(old('organization_name'))?old('organization_name'):$org->name?>" class="form-control <?=($errors->has('organization_name'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('organization_name') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    <label>Адрес организации<span class="text-danger">*</span></label>

                                    <input type="text" name="address" value="<?=(old('address'))?old('address'):$org->address?>" class="form-control <?=($errors->has('address'))?'parsley-error':''?>"  data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('address') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    <label>Телефон<span class="text-danger">*</span></label>

                                    <input type="text" name="phone" value="<?=(old('phone'))?old('phone'):$org->phone?>" class="form-control <?=($errors->has('phone'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('phone') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    <label>ИНН<span class="text-danger">*</span></label>

                                    <input type="text" name="inn" value="<?=(old('inn'))?old('inn'):$org->inn?>" class="form-control <?=($errors->has('inn'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('inn') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    <label>КПП<span class="text-danger">*</span></label>

                                    <input type="text" name="ppc" value="<?=(old('ppc'))?old('ppc'):$org->ppc?>" class="form-control  <?=($errors->has('ppc'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('ppc') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    
                                    <label>ОГРН<span class="text-danger">*</span></label>

                                    <input type="text" name="bin" value="<?=(old('bin'))?old('bin'):$org->bin?>" class="form-control <?=($errors->has('bin'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('bin') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    <label>Руководитель организации<span class="text-danger">*</span></label>

                                    <input type="text" name="head" value="<?=(old('head'))?old('head'):$org->head?>" class="form-control <?=($errors->has('head'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('head') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    
                                    <label>Главный бухгалтер организации<span class="text-danger">*</span></label>

                                    <input type="text" name="chief_accountant" value="<?=(old('chief_accountant'))?old('chief_accountant'):$org->chief_accountant?>" class="form-control <?=($errors->has('chief_accountant'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('chief_accountant') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    <label>Расчетный счет<span class="text-danger">*</span></label>

                                    <input type="text" name="checking_account" value="<?=(old('checking_account'))?old('checking_account'):$org->checking_account?>" class="form-control <?=($errors->has('checking_account'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('checking_account') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    <label>Наименование банка<span class="text-danger">*</span></label>

                                    <input type="text" name="bank" value="<?=(old('bank'))?old('bank'):$org->bank?>" class="form-control <?=($errors->has('bank'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('bank') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    <label>БИК банка<span class="text-danger">*</span></label>

                                    <input type="text" name="bic_bank" value="<?=(old('bic_bank'))?old('bic_bank'):$org->bic_bank?>" class="form-control <?=($errors->has('bic_bank'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('bic_bank') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    <label>ИНН Банка<span class="text-danger">*</span></label>

                                    <input type="text" name="inn_bank" value="<?=(old('inn_bank'))?old('inn_bank'):$org->inn_bank?>" class="form-control <?=($errors->has('inn_bank'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('inn_bank') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    <label>Корреспондентсикй счет<span class="text-danger">*</span></label>

                                    <input type="text" name="correspondent_account" value="<?=(old('correspondent_account'))?old('correspondent_account'):$org->correspondent_account?>" class="form-control <?=($errors->has('correspondent_account'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('correspondent_account') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>

                                    <label>Логотип</label>
                                    <div class="form-group">

                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                          <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 188px; height: 150px;">
                                              
                                                <a href="<?=url('/fileentry/get/' . $user->logo . '/origin')?>"><img src="<?=url('/fileentry/get/' . $user->logo . '/admin-preview')?>" alt="" class="img-responsive" /></a>
                                          </div>
                                          <div>
                                            <span class="btn btn-primary btn-file"><span class="fileinput-new">Выбрать</span><span class="fileinput-exists">Изменить</span><input type="file" name="logo"></span>
                                            <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Удалить</a>
                                          </div>
                                        </div>
                                    </div>
                                   <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('logo') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>       
                                    
                                    <label>Описание</label>

                                    <textarea type="text" name="description" value="" class="form-control <?=($errors->has('description'))?'parsley-error':''?>" data-parsley-required="true"><?=(old('description'))?old('description'):$user->description?></textarea>
                                    <ul class="parsley-error-list">
                                        <?php foreach ($errors->get('description') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>

                                </div>
 
                            </div>
                        </div>
                        <footer class="panel-footer text-right">
                            <button type="button" class="btn btn-success btn-redirect" data-href="<?=url('users')?>">Назад</button>
                            <button type="submit" class="btn btn-success">Сохранить</button>
                        </footer>
                    </div>
                </form>                	
            </div>
        </div>
    </div>
</section>

<script src="/scripts/bootstrap.min.js"></script>

<!-- Proton base scripts: -->

<script src="/scripts/main.js"></script>
<script src="/scripts/proton/common.js"></script>
<script src="/scripts/proton/main-nav.js"></script>
<script src="/scripts/proton/user-nav.js"></script>



<!-- Page-specific scripts: -->
<script src="/scripts/proton/sidebar.js"></script>
<script src="/scripts/proton/tables.js"></script>
<!-- jsTree -->
<script src="/scripts/vendor/jquery.jstree.js"></script>
<!-- Data Tables -->
<!-- http://datatables.net/ -->
<script src="/scripts/vendor/jquery.dataTables.min.js"></script>

<!-- Data Tables for BS3 -->
<!-- https://github.com/Jowin/Datatables-Bootstrap3/ -->
<!-- NOTE: Original JS file is modified -->
    <script src="/scripts/vendor/datatables.js"></script>
<!-- Select2 Required To Style Datatable Select Box(es) -->
<!-- https://github.com/fk/select2-bootstrap-css -->
    <script src="/scripts/vendor/select2.min.js"></script>

<!-- File Input -->
<!-- http://jasny.github.io/bootstrap/javascript/#fileinput -->
<script src="/scripts/vendor/fileinput.js"></script>