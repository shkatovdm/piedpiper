<?php $this->layout('layout/main') ?>        
        <?=$main_menu?>
       
        <section class="wrapper retracted scrollable">
            
           <?=$panel?>            
            <div class="row">
                <div class="col-md-12">
                          
                    <div class="panel panel-default panel-block">
                    	
	                    <table class="table table-bordered table-striped">
	                        <thead>
	                            <tr>
	                                <th>ID</th>
                                    <th>Логин</th>
                                    <th>Почта</th>
                                    <th>Роль</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                            <?php foreach ($collection as $item):?>
                                    <tr class="gradeX">
                                        <td><a href="<?=url('/users/edit/' . $item->id)?>"><?=$item->id?></a></td>
                                        <td><?=$item->name?></td>
                                        <td><?=$item->email?></td>
                                        <td><?=$item->role_name?></td>
                                        <td>
                                          <?php if(isset($permission['update']) && $permission['update']):?>
                                                <button class="btn btn-sm btn-primary btn-redirect btn-greed" data-href="<?=url('/users/edit/' . $item->id)?>">Редактировать</button>
                                            <?php endif?>

                                            <?php if(isset($permission['delete']) && $permission['delete']):?>
                                                <form action="<?=url('/users/remove/' . $item->id)?>" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate>
                                                    <?=csrf_field()?>
                                                    <button type="submit" class="btn-confirm btn btn-sm btn-primary">Удалить</button>
                                                </form>
                                            <?php endif?>
                                        </td>
                                    </tr>
                                <?php endforeach?>
                                
	                        </tbody>
	                    </table>
                    <?=$collection->render()?>
                    </div>
                </div>
            </div>
        </section>

        <script src="/scripts/bootstrap.min.js"></script>

		<!-- Proton base scripts: -->
        
        <script src="/scripts/main.js"></script>
		<script src="/scripts/proton/common.js"></script>
		<script src="/scripts/proton/main-nav.js"></script>
		<script src="/scripts/proton/user-nav.js"></script>
		


        <!-- Page-specific scripts: -->
        <script src="/scripts/proton/sidebar.js"></script>
        <script src="/scripts/proton/tables.js"></script>
        <!-- jsTree -->
        <script src="/scripts/vendor/jquery.jstree.js"></script>
        <!-- Data Tables -->
        <!-- http://datatables.net/ -->
        <script src="/scripts/vendor/jquery.dataTables.min.js"></script>
        
        <!-- Data Tables for BS3 -->
        <!-- https://github.com/Jowin/Datatables-Bootstrap3/ -->
        <!-- NOTE: Original JS file is modified -->
            <script src="/scripts/vendor/datatables.js"></script>
        <!-- Select2 Required To Style Datatable Select Box(es) -->
        <!-- https://github.com/fk/select2-bootstrap-css -->
            <script src="/scripts/vendor/select2.min.js"></script>