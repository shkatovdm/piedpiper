<?php $this->layout('layout/main') ?>        
<?=$main_menu?>

<section class="wrapper retracted scrollable">
       
    <?=$panel?>
    
    <div class="row">
        <div class="col-md-12">
                  
            <div class="panel panel-default panel-block">
                <form action="<?=url('/users/edit/' . $user->id)?>" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate>
                    <?=csrf_field()?>
                    <div class="panel panel-default panel-block">
                        <div class="list-group">
                            <div class="list-group-item">
            
                                <div class="form-group">

                                    <label>Логин<span class="text-danger">*</span></label>

                                    <input type="text" name="name" value="<?=(old('name'))?old('name'):$user->name?>" class="form-control <?=($errors->has('name'))?'parsley-error':''?>" data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('name') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                                    
                                    <label>Почта<span class="text-danger">*</span></label>

                                    <input type="text" name="email" value="<?=(old('email'))?old('email'):$user->email?>" class="form-control <?=($errors->has('email'))?'parsley-error':''?>"  data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('email') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                     
                                    <label>Пароль<span class="text-danger">*</span></label>

                                    <input type="password" name="password" value="<?=old('password')?>" class="form-control <?=($errors->has('password'))?'parsley-error':''?>"  data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('password') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>

                                    <label>Подтверждение пароля<span class="text-danger">*</span></label>

                                    <input type="password" name="password_confirmation" value="<?=old('password_confirmation')?>" class="form-control <?=($errors->has('password_confirmation'))?'parsley-error':''?>"  data-parsley-required="true">
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('password_confirmation') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
                     
                                    <label>Роль<span class="text-danger">*</span></label>
                                    <select class="select2" name="role_id">
                                        <?php foreach ($roles as $role):?>
                                            <option value="<?=$role->id?>"><?=$role->name?></option>
                                        <?php endforeach ?>
                                    </select>
                                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                                        <?php foreach ($errors->get('role_id') as $message):?>
                                            <li class="required" style="display: list-item;"><?=$message?></li>
                                        <?php endforeach?>
                                    </ul>
 
                                </div>
 
                            </div>
                        </div>
                        <footer class="panel-footer text-right">
                            <button type="button" class="btn btn-success btn-redirect" data-href="<?=url('users')?>">Назад</button>
                            <button type="submit" class="btn btn-success">Сохранить</button>
                        </footer>
                    </div>
                </form>                	
            </div>
        </div>
    </div>
</section>

<script src="/scripts/bootstrap.min.js"></script>

<!-- Proton base scripts: -->

<script src="/scripts/main.js"></script>
<script src="/scripts/proton/common.js"></script>
<script src="/scripts/proton/main-nav.js"></script>
<script src="/scripts/proton/user-nav.js"></script>



<!-- Page-specific scripts: -->
<script src="/scripts/proton/sidebar.js"></script>
<script src="/scripts/proton/tables.js"></script>
<!-- jsTree -->
<script src="/scripts/vendor/jquery.jstree.js"></script>
<!-- Data Tables -->
<!-- http://datatables.net/ -->
<script src="/scripts/vendor/jquery.dataTables.min.js"></script>

<!-- Data Tables for BS3 -->
<!-- https://github.com/Jowin/Datatables-Bootstrap3/ -->
<!-- NOTE: Original JS file is modified -->
    <script src="/scripts/vendor/datatables.js"></script>
<!-- Select2 Required To Style Datatable Select Box(es) -->
<!-- https://github.com/fk/select2-bootstrap-css -->
    <script src="/scripts/vendor/select2.min.js"></script>