<form action="/users/add" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate id="addUserForm">
    <?=csrf_field()?>
    <div class="panel panel-default panel-block">
        <div class="list-group">
            <div class="list-group-item">
                <div class="form-group">

                    <label>Логин<span class="text-danger">*</span></label>

                    <input type="text" name="name" value="<?=$request['name']?>" class="form-control <?=($errors->has('name'))?'parsley-error':''?>" data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('name') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>
                    
                    <label>Почта<span class="text-danger">*</span></label>

                    <input type="text" name="email" value="<?=$request['email']?>" class="form-control <?=($errors->has('email'))?'parsley-error':''?>"  data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('email') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>
     
                    <label>Пароль<span class="text-danger">*</span></label>

                    <input type="password" name="password" value="<?=$request['password']?>" class="form-control <?=($errors->has('password'))?'parsley-error':''?>"  data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('password') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>

                    <label>Подтверждение пароля<span class="text-danger">*</span></label>

                    <input type="password" name="password_confirmation" value="<?=$request['password_confirmation']?>" class="form-control <?=($errors->has('password_confirmation'))?'parsley-error':''?>"  data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('password_confirmation') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>
     
                    <input type="hidden" id="role_id" name="role_id" value="4">
                    
                    <label>Название организации<span class="text-danger">*</span></label>

                    <input type="text" name="organization_name" value="<?=$request['organization_name']?>" class="form-control <?=($errors->has('organization_name'))?'parsley-error':''?>" data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('organization_name') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>
                    <label>Адрес организации<span class="text-danger">*</span></label>

                    <input type="text" name="organization_address" value="<?=$request['organization_address']?>" class="form-control <?=($errors->has('organization_address'))?'parsley-error':''?>"  data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('organization_address') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>
                    <label>Телефон<span class="text-danger">*</span></label>

                    <input type="text" name="phone" value="<?=$request['phone']?>" class="form-control <?=($errors->has('phone'))?'parsley-error':''?>" data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('phone') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>
                    <label>ИНН<span class="text-danger">*</span></label>

                    <input type="text" name="inn" value="<?=$request['inn']?>" class="form-control <?=($errors->has('inn'))?'parsley-error':''?>" data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('inn') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>
                    <label>КПП<span class="text-danger">*</span></label>

                    <input type="text" name="ppc" value="<?=$request['ppc']?>" class="form-control  <?=($errors->has('ppc'))?'parsley-error':''?>" data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('ppc') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>
                    
                    <label>ОГРН<span class="text-danger">*</span></label>

                    <input type="text" name="bin" value="<?=$request['bin']?>" class="form-control <?=($errors->has('bin'))?'parsley-error':''?>" data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('bin') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>
                    <label>Руководитель организации<span class="text-danger">*</span></label>

                    <input type="text" name="head" value="<?=$request['head']?>" class="form-control <?=($errors->has('head'))?'parsley-error':''?>" data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('head') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>
                    
                    <label>Главный бухгалтер организации<span class="text-danger">*</span></label>

                    <input type="text" name="chief_accountant" value="<?=$request['chief_accountant']?>" class="form-control <?=($errors->has('chief_accountant'))?'parsley-error':''?>" data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('chief_accountant') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>
                    <label>Расчетный счет<span class="text-danger">*</span></label>

                    <input type="text" name="checking_account" value="<?=$request['checking_account']?>" class="form-control <?=($errors->has('checking_account'))?'parsley-error':''?>" data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('checking_account') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>
                    <label>Наименование банка<span class="text-danger">*</span></label>

                    <input type="text" name="bank" value="<?=$request['bank']?>" class="form-control <?=($errors->has('bank'))?'parsley-error':''?>" data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('bank') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>
                    <label>БИК банка<span class="text-danger">*</span></label>

                    <input type="text" name="bic_bank" value="<?=$request['bic_bank']?>" class="form-control <?=($errors->has('bic_bank'))?'parsley-error':''?>" data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('bic_bank') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>
                    <label>ИНН Банка<span class="text-danger">*</span></label>

                    <input type="text" name="inn_bank" value="<?=$request['inn_bank']?>" class="form-control <?=($errors->has('inn_bank'))?'parsley-error':''?>" data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('inn_bank') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>
                    <label>Корреспондентсикй счет<span class="text-danger">*</span></label>

                    <input type="text" name="correspondent_account" value="<?=$request['correspondent_account']?>" class="form-control <?=($errors->has('correspondent_account'))?'parsley-error':''?>" data-parsley-required="true">
                    <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('correspondent_account') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>
                </div>

            </div>
        </div>
        <footer class="panel-footer text-right">
            <button type="button" id="addUserSubmit" class="btn btn-success">Сохранить</button>
        </footer>
    </div>
</form>                	
                    