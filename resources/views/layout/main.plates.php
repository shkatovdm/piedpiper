<!doctype html>
<!--[if lt IE 7]>      <html class="no-js lt-ie9 lt-ie8 lt-ie7 lt-ie10"> <![endif]-->
<!--[if IE 7]>         <html class="no-js lt-ie9 lt-ie8 lt-ie10"> <![endif]-->
<!--[if IE 8]>         <html class="no-js lt-ie9 lt-ie10"> <![endif]-->
<!--[if IE 9]>         <html class="no-js lt-ie10"> <![endif]-->
<!--[if gt IE 9]><!--> <html class="no-js"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        
        <title></title>
        <meta name="description" content="Page Description">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
        <!-- Place favicon.ico and apple-touch-icon.png in the root directory -->

        <link rel="stylesheet" href="/styles/bootstrap.css">
        

        <!-- Page-specific Plugin CSS: -->
        <link rel="stylesheet" href="/styles/vendor/select2/select2.css">
        <link rel="stylesheet" href="/styles/vendor/datatables.css" media="screen" />
        <link rel="stylesheet" href="/styles/vendor/jquery.pnotify.default.css" media="screen">


        <!-- Proton CSS: -->
        <link rel="stylesheet" href="/styles/proton.css">
        <link rel="stylesheet" href="/styles/vendor/animate.css">

        <!-- adds CSS media query support to IE8   -->
        <!--[if lt IE 9]>
            <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.6.2/html5shiv.js"></script>
            <script src="/scripts/vendor/respond.min.js"></script>
        <![endif]-->

        <!-- Fonts CSS: -->
        <link rel="stylesheet" href="/styles/font-awesome.css" type="text/css" />
        <link rel="stylesheet" href="/styles/font-titillium.css" type="text/css" />

        <!-- Common Scripts: -->
        <script>
        (function () {
          var js;
          if (typeof JSON !== 'undefined' && 'querySelector' in document && 'addEventListener' in window) {
            js = 'http://ajax.googleapis.com/ajax/libs/jquery/2.0.3/jquery.min.js';
          } else {
            js = 'http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js';
          }
          document.write('<script src="' + js + '"><\/script>');
        }());
        </script>
        <script src="/scripts/vendor/modernizr.js"></script>
        <script src="/scripts/vendor/jquery.cookie.js"></script>
    </head>
      <body>
        
        <script>
            var theme = $.cookie('protonTheme') || 'default';
            $('body').removeClass (function (index, css) {
                return (css.match (/\btheme-\S+/g) || []).join(' ');
            }); 
            if (theme !== 'default') $('body').addClass(theme);
        </script>
        <!--[if lt IE 8]>
        <p class="browsehappy">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->

        <script>
                if (!($('body').is('.dashboard-page') || $('body').is('.login-page'))){
                    if ($.cookie('protonSidebar') == 'retracted') {
                        $('.wrapper').removeClass('retracted').addClass('extended');
                    }
                    if ($.cookie('protonSidebar') == 'extended') {
                        $('.wrapper').removeClass('extended').addClass('retracted');
                    }
                }
        </script>

        <nav class="user-menu">
            
            <section class="user-menu-wrapper">
                <a href="<?=url('/profile')?>">
                    <?=Helpers::getCurrentUserEmail()?>
                </a>
            </section>

        </nav>


        <?=$this->section('content')?>
        
        <div id="myModal" tabindex="-1" role="dialog" class="modal fade">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title"><i class="icon-hdd"></i> Modal title</h4>
                    </div>
                    <div class="modal-body">
                        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fugiat, impedit, at corporis provident consectetur dolores sequi aliquam nam cupiditate labore ab eius mollitia veritatis perspiciatis quidem vitae officiis exercitationem tenetur cum voluptatem in recusandae consequatur magnam vero expedita aspernatur nihil et velit pariatur ratione fugit voluptatibus iure nulla maxime qui.</p>
                        <a href="javascript:;">
                            Some link here.
                        </a>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-lg btn-default" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-lg btn-primary">Save changes</button>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <!-- Notifications -->
        <!-- http://pinesframework.org/pnotify/ -->
        <script src="/scripts/vendor/jquery.pnotify.min.js"></script>
        
        <?php if($aNotify = session("notify")):?>
                <script>
                    $.pnotify({
                        title: '<?=$aNotify["title"]?>',
                        text: '<?=$aNotify["text"]?>',
                        history: false,
                        type: '<?=$aNotify["type"]?>',
                        hide: false
                    });    
                </script>
                
        <?php endif?>
    </body>
</html>
