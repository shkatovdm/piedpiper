<?php $this->layout('layout/main') ?>        
        <?=$main_menu?>
       
        <section class="wrapper retracted scrollable">
            
           <?=$panel?>            
            <div class="row">
                <div class="col-md-12">
                          
                    <div class="panel panel-default panel-block">
                    	
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>Начало периода</th>
                                            <th>Конец периода</th>
                                            <th>Сумма</th>
                                            <th>Отчет</th>
                                            <th></th>
                                            <th>Платежное поручение</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php foreach ($collection as $item):?>
                                            <tr class="gradeX">
                                                <td><?=$item->start?></td>
                                                <td><?=$item->end?></td>
                                                <td><?=$item->total?></td>
                                                <td>
                                                    <form action="<?=url('/print/report-shipments')?>" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate>
                                                        <?=csrf_field()?>
                                                        <input type="hidden" name="report_id" value="<?=$item->id?>" />
                                                        <button type="submit" class="btn btn-sm btn-primary">Скачать отчет</button>
                                                    </form>
                                
                                                <td>                                                    
                                                    <?php if(isset($item->confirmed_report) && !empty($item->confirmed_report)):?>
                                                        <a target="_blank" href="<?=url('/fileentry/get/' . $item->confirmed_report . '/origin')?>">Скачать скан</a>
                                                    <?php else:?>
                                                        <a class="btn btn-primary btn-sm" href="#uploadImg" data-toggle="modal">Загрузить скан</a>
                                                    <?php endif?>
                                                </td>
                                                <td>                                                    
                                                    <?php if(isset($item->payment_order) && !empty($item->payment_order)):?>
                                                        <a target="_blank" href="<?=url('/fileentry/get/' . $item->payment_order . '/origin')?>">Скачать отчет</a>

                                                    <?php else:?>
                                                        
                                                        <a class="btn btn-primary btn-sm" href="#paymentOrder" data-toggle="modal">Загрузить</a>
                                                    <?php endif?>
                                                </td>
                                            </tr>
                                            
        <div id="uploadImg" tabindex="-1" role="dialog" class="modal fade">
            <div class="modal-dialog big">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Загрузка изображения</h4>
                    </div>
                    <div class="modal-body">
                    <form action="<?=url('/add-confirmed-report')?>" method="post" enctype="multipart/form-data" data-parsley-namespace="data-parsley-" data-parsley-validate>
                        <input type="hidden" name="report_id" value="<?=$item->id?>" />
                        <?=csrf_field()?>
                        <label>Изображение<span class="text-danger">*</span></label>
                        
                        <div class="form-group">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                              <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 188px; height: 150px;"></div>
                              <div>
                                <span class="btn btn-primary btn-file"><span class="fileinput-new">Выбрать</span><span class="fileinput-exists">Изменить</span><input type="file" name="file"></span>
                                <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Удалить</a>
                              </div>
                            </div>
                        </div>
                        <ul id="parsley-6099321319480859" class="parsley-error-list">
                            <?php foreach ($errors->get('waybill') as $message):?>
                                <li class="required" style="display: list-item; text-align:left;"><?=$message?></li>
                            <?php endforeach?>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="-btn-confirm btn btn-sm btn-primary">Подтвердить</button>
                        <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Закрыть</button>
                    </div>
                    </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div id="paymentOrder" tabindex="-1" role="dialog" class="modal fade">
            <div class="modal-dialog big">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Загрузка платежного поручения</h4>
                    </div>
                    <div class="modal-body">
                    <form action="<?=url('/add-payment-order')?>" method="post" enctype="multipart/form-data" data-parsley-namespace="data-parsley-" data-parsley-validate>
                        <input type="hidden" name="report_id" value="<?=$item->id?>" />
                        <?=csrf_field()?>
                        <label>Изображение<span class="text-danger">*</span></label>
                        
                        <div class="form-group">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                              <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 188px; height: 150px;"></div>
                              <div>
                                <span class="btn btn-primary btn-file"><span class="fileinput-new">Выбрать</span><span class="fileinput-exists">Изменить</span><input type="file" name="file"></span>
                                <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Удалить</a>
                              </div>
                            </div>
                        </div>
                        <ul id="parsley-6099321319480859" class="parsley-error-list">
                            <?php foreach ($errors->get('waybill') as $message):?>
                                <li class="required" style="display: list-item; text-align:left;"><?=$message?></li>
                            <?php endforeach?>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="-btn-confirm btn btn-sm btn-primary">Подтвердить</button>
                        <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Закрыть</button>
                    </div>
                    </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
                                        <?php endforeach?>
                                        
                                    </tbody>
                                </table>
                    <?=$collection->render()?>
                    </div>
                </div>
            </div>
        </section>


        <script src="/scripts/bootstrap.min.js"></script>

		<!-- Proton base scripts: -->
        
        <script src="/scripts/main.js"></script>
		<script src="/scripts/proton/common.js"></script>
		<script src="/scripts/proton/main-nav.js"></script>
		<script src="/scripts/proton/user-nav.js"></script>
		


        <!-- Page-specific scripts: -->
        <script src="/scripts/proton/sidebar.js"></script>
        <script src="/scripts/proton/tables.js"></script>
        <!-- jsTree -->
        <script src="/scripts/vendor/jquery.jstree.js"></script>
        <!-- Data Tables -->
        <!-- http://datatables.net/ -->
        <script src="/scripts/vendor/jquery.dataTables.min.js"></script>
        
        <!-- Data Tables for BS3 -->
        <!-- https://github.com/Jowin/Datatables-Bootstrap3/ -->
        <!-- NOTE: Original JS file is modified -->
            <script src="/scripts/vendor/datatables.js"></script>
        <!-- Select2 Required To Style Datatable Select Box(es) -->
        <!-- https://github.com/fk/select2-bootstrap-css -->
            <script src="/scripts/vendor/select2.min.js"></script>
        <script src="/scripts/vendor/fileinput.js"></script>