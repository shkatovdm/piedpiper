<div class="modal-header">
    <strong>Склад</strong>
    <select class="select2" name="role_id">
        
        <?php foreach ($stores as $store):?>
            <option value="<?=$store->id?>"><?=$store->name?></option>
        <?php endforeach ?>
        
    </select>
</div>

<table class="table table-bordered table-striped">
    <thead>
        <tr>
            <th>Поставщик</th>
            <th>Склад</th>
            <th>Населенный пункт</th>
            <th>Кол-во</th>
            <th>Название</th>
            <th>Артикул</th>
            <th>Цена</th>
            <th></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($collection as $item):?>
            <tr class="gradeX">
                <td><a href="<?=url('/profile/' . $item->user_id)?>"><?=$item->org_name?></a></td>
                <td><?=$item->store_name?></td>
                <td><?=$item->city_name?></td>
                <td><?=$item->balance?></td>
                <td><?=$item->name?></td>
                <td><?=$item->article?></td>
                <td><?=$item->price?></td>
                <td>
                    <form action="<?=url('/orders/add/')?>" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate>
                        <?=csrf_field()?>
                        <input type="hidden" name="product_id" value="<?=$item->id?>">
                        <input type="hidden" name="sheet_id" value="<?=$sheet_id?>">
                        <button type="submit" class="btn-confirm btn btn-sm btn-primary">Выбрать</button>
                    </form>
                </td>
            </tr>
        <?php endforeach?>
    </tbody>
</table>
<div class="ajax_pagin">
    <?=$collection->render()?>
</div>
                   