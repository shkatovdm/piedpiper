
	<nav class="main-menu" data-step='2' data-intro='This is the extendable Main Navigation Menu.' data-position='right'>
		<ul>
			<li class="has-subnav">
				<a href="<?=url('/lots')?>">
					<i class="icon-suitcase nav-icon"></i>
					<span class="nav-text">
						Лоты
					</span>
					<i class="icon-angle-right"></i>
				</a>
				<ul>
					<li>
						<a class="subnav-text" href="<?=url('/lots/add');?>">
							Добавить
						</a>
					</li>
				</ul>
			</li>
			<li class="has-subnav">
				<a href="<?=url('/users')?>">
					<i class="icon-male nav-icon"></i>
					<span class="nav-text">
						Пользователи
					</span>
					<i class="icon-angle-right"></i>
				</a>
				<ul>
					<li>
						<a class="subnav-text" href="<?=url('/users/add');?>">
							Добавить
						</a>
					</li>
				</ul>
			</li>

			<li class="has-subnav">
				<a href="<?=url('/resources')?>">
					<i class="icon-upload-alt nav-icon"></i>
					<span class="nav-text">
						Коды ресурсов
					</span>
				</a>
			</li>
			
			<li class="has-subnav">
				<a href="<?=url('/executors')?>">
					<i class="icon-wrench nav-icon"></i>
					<span class="nav-text">
						Исполнители
					</span>
					<i class="icon-angle-right"></i>
				</a>
				<ul>
					<li>
						<a class="subnav-text" href="<?=url('/executors/add');?>">
							Добавить
						</a>
					</li>
				</ul>
			</li>

			<li class="has-subnav">
				<a href="<?=url('/providers')?>">
					<i class="icon-truck nav-icon"></i>
					<span class="nav-text">
						Поставщики
					</span>
					<i class="icon-angle-right"></i>
				</a>
				<ul>
					<li>
						<a class="subnav-text" href="<?=url('/providers/add');?>">
							Добавить
						</a>
					</li>
				</ul>
			</li>
			<li class="has-subnav">
				<a href="<?=url('/matching')?>">
					<i class="icon-money nav-icon"></i>
					<span class="nav-text">
						Согласование
					</span>
				</a>
			</li>

			<li class="has-subnav">
				<a href="<?=url('/objects')?>">
					<i class="icon-money nav-icon"></i>
					<span class="nav-text">
						Объекты
					</span>
				</a>
			</li>

			<li class="has-subnav">
				<a href="<?=url('/sheet')?>">
					<i class="icon-money nav-icon"></i>
					<span class="nav-text">
						Сводная ведомость
					</span>
				</a>
			</li>
		</ul>

		<ul class="logout">
			<li>
				<a href="<?=url('/auth/logout')?>">
					<i class="icon-off nav-icon"></i>
					<span class="nav-text">
						Logout
					</span>
				</a>
			</li>  
		</ul>
	</nav>