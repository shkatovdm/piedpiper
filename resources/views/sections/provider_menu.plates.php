
	<nav class="main-menu" data-step='2' data-intro='This is the extendable Main Navigation Menu.' data-position='right'>
		<ul>

			<li class="has-subnav">
				<a href="<?=url('/orders')?>">
					<i class="icon-money nav-icon"></i>
					<span class="nav-text">
						Заказы
					</span>
				</a>
			</li>
			<li class="has-subnav">
				<a href="<?=url('/approved')?>">
					<i class="icon-money nav-icon"></i>
					<span class="nav-text">
						Подтвержденные
					</span>
				</a>
			</li>
			<li class="has-subnav">
				<a href="<?=url('/awaiting-shipment')?>">
					<i class="icon-money nav-icon"></i>
					<span class="nav-text">
						Ожидающие отгрузки
					</span>
				</a>
			</li>
			<li class="has-subnav">
				<a href="<?=url('/shipped')?>">
					<i class="icon-money nav-icon"></i>
					<span class="nav-text">
						Отгруженные
					</span>
				</a>
			</li>
			<li class="has-subnav">
				<a href="<?=url('/report-shipments')?>">
					<i class="icon-money nav-icon"></i>
					<span class="nav-text">
						Отчеты
					</span>
				</a>
			</li>
			<li class="has-subnav">
				<a href="<?=url('/profile/edit')?>">
					<i class="icon-user nav-icon"></i>
					<span class="nav-text">
						Профиль
					</span>
				</a>
			</li>
		
		</ul>

		<ul class="logout">
			<li>
				<a href="<?=url('/auth/logout')?>">
					<i class="icon-off nav-icon"></i>
					<span class="nav-text">
						Logout
					</span>
				</a>
			</li>  
		</ul>
	</nav>