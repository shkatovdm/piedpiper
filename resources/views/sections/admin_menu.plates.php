
	<nav class="main-menu" data-step='2' data-intro='This is the extendable Main Navigation Menu.' data-position='right'>
		<ul>			
			<li class="has-subnav">
				<a href="javascript:;">
					<i class="icon-list nav-icon"></i>
					<span class="nav-text">
						Справочники
					</span>
					<i class="icon-angle-right"></i>
				</a>
				<ul>
					<li>
						<a class="subnav-text" href="/city">
							Города
						</a>
					</li>
				</ul>
			</li>
			<li class="has-subnav">
				<a href="<?=url('/users')?>">
					<i class="icon-male nav-icon"></i>
					<span class="nav-text">
						Пользователи
					</span>
					<i class="icon-angle-right"></i>
				</a>
				<ul>
					<li>
						<a class="subnav-text" href="<?=url('/users/add');?>">
							Добавить
						</a>
					</li>
				</ul>
			</li>
			<li class="has-subnav">
				<a href="<?=url('/objects')?>">
					<i class="icon-home nav-icon"></i>
					<span class="nav-text">
						Объекты
					</span>
					<i class="icon-angle-right"></i>
				</a>
				<ul>
					<li>
						<a class="subnav-text" href="<?=url('/objects/add');?>">
							Добавить
						</a>
					</li>
				</ul>
			</li>
			<li class="has-subnav">
				<a href="javascript:;">
					<i class="icon-folder-open nav-icon"></i>
					<span class="nav-text">
						Загрузка кодов ресурсов
					</span>
				</a>
			</li>
		</ul>

		<ul class="logout">
			<li>
				<a href="<?=url('/auth/logout')?>">
					<i class="icon-off nav-icon"></i>
					<span class="nav-text">
						Logout
					</span>
				</a>
			</li>  
		</ul>
	</nav>