<?php if(isset($update) && $update):?>
    <button class="btn btn-sm btn-primary btn-redirect btn-greed" data-href="<?=url('/' . $entity . '/edit/' . $model->id)?>">Редактировать</button>
<?php endif?>

<?php if(isset($delete) && $delete):?>
    <form action="<?=url('/' . $entity . '/remove/' . $model->id)?>" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate>
        <?=csrf_field()?>
        <button type="submit" class="btn-confirm btn btn-sm btn-primary">Удалить</button>
    </form>
<?php endif?>