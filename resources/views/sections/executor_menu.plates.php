
	<nav class="main-menu" data-step='2' data-intro='This is the extendable Main Navigation Menu.' data-position='right'>
		<ul>

			<li class="has-subnav">
				<a href="<?=url('/objects')?>">
					<i class="icon-home nav-icon"></i>
					<span class="nav-text">
						Мои объекты
					</span>
				</a>
			</li>

		</ul>

		<ul class="logout">
			<li>
				<a href="<?=url('/auth/logout')?>">
					<i class="icon-off nav-icon"></i>
					<span class="nav-text">
						Logout
					</span>
				</a>
			</li>  
		</ul>
	</nav>