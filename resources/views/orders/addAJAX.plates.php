<form action="/resources/add" method="post" enctype="multipart/form-data" data-parsley-namespace="data-parsley-" data-parsley-validate>
    <?=csrf_field()?>
    <div class="panel panel-default panel-block">
        <div class="list-group">
            <div class="list-group-item">
                <div class="form-group">

                    <label>Файл<span class="text-danger">*</span></label>
                   
                    <div class="form-control uneditable-input span3" data-trigger="fileinput">
                        <i class="icon-file fileinput-exists"></i> 
                        <span class="fileinput-filename"></span>
                    </div>
                    <span class="input-group-addon btn btn-primary btn-file">
                        <span class="fileinput-new">Select file</span>
                        <span class="fileinput-exists">Change</span>
                        <input type="file" name="userfile" >
                    </span>
                   <ul id="parsley-6099321319480859" class="parsley-error-list">
                        <?php foreach ($errors->get('userfile') as $message):?>
                            <li class="required" style="display: list-item;"><?=$message?></li>
                        <?php endforeach?>
                    </ul>                           
     
                </div>

            </div>
        </div>
        <footer class="panel-footer text-right">
            <button type="button" class="btn btn-success btn-redirect" data-href="<?=url('/sheet')?>">Назад</button>
            <button type="submit" class="btn btn-success">Сохранить</button>
        </footer>
    </div>
</form>