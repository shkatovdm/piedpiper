<?php $this->layout('layout/main') ?>        
<?=$main_menu?>

<section class="wrapper retracted scrollable">
       
    <div id="top" class="panel panel-default panel-block panel-title-block">
        <div class="panel-heading">
            <div>
                <h1>Объект: <?=$entity->street?> <?=$entity->house_number?> <?=$entity->housing?> <?=$entity->building?></h1>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <h3>Не выбрано</h3>      
            <div class="panel panel-default panel-block">
                
                <table class="table table-bordered table-striped">
                    <thead>
                        <tr>
                            <th>Код ресурса</th>
                            <th>Наименование</th>
                            <th>Единица измерения</th>
                            <th>Количество</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php foreach ($collection as $item):?>
                            <tr class="gradeX">
                                <td><?=$item->code_resource?></td>
                                <td><?=$item->name_resource?></td>
                                <td><?=$item->measurement?></td>
                                <td><?=$item->quantity?></td>
                                <td>
                                    <a class="btn btn-primary btn-sm" id="search-products" href="#addObjectModal" data-sheet-id="<?=$item->id?>" data-code-resource="<?=$item->code_resource?>" data-toggle="modal">Выбрать товар</a>
                                </td>
                            </tr>
                        <?php endforeach?>
                        
                    </tbody>
                </table>
            <?=$collection->render()?>
            </div>
        </div>
    </div>
    
    <?php foreach ($orders as $status):?>
           
            <div class="row">
                <div class="col-md-12">
                    <?php if($status['status_id'] == 3 || $status['status_id'] == 5 ):?>
                    
                        <h4>Статус заказа: <span class="red"><?=$status['status_name']?></span></h4>    
                        <div class="alert alert-dismissable alert-danger fade in">
                            <?=$status['comment']?>
                        </div>
                    <?php else:?>
                    
                        <h4>Статус заказа: <?=$status['status_name']?></h4>    
                    
                    <?php endif?>
                    <?php foreach ($status['stores'] as $store):?>
                        <strong>Склад: <?=$store['store']?></strong>
                        <div class="panel panel-default panel-block">
                            <table class="table table-bordered table-striped">
                                <thead>
                                    <tr>
                                        <th>Код ресурса</th>
                                        <th>Ресурс</th>
                                        <th>Ед. изм</th>
                                        <th>Кол-во</th>
                                        <th>Название</th>
                                        <th>Арт.</th>
                                        <th>Цена</th>
                                        <th>Итого</th>
                                        <th>Накладная</th>
                            
                                    </tr>
                                </thead>
                                <tbody>
                                    <?php foreach ($store['result'] as $item):?>
                                        <tr class="gradeX">
                                            <td><?=$item->code_resource?></td>
                                            <td><?=$item->name_resource?></td>
                                            <td><?=$item->measurement_resource?></td>
                                            <td><?=$item->count?></td>
                                            <td><?=$item->product_name?></td>
                                            <td><?=$item->product_article?></td>
                                            <td><?=$item->product_price?></td>
                                            <td><?=$item->total?></td>
                                            <td>
                                                <?php if(isset($item->waybill) && !empty($item->waybill)):?>
                                                    <a target="_blank" href="<?=url('/fileentry/get/' . $item->waybill . '/origin')?>">Скачать</a>
                                                <?php endif?>
                                            </td>
                            
                                        </tr>
                                    <?php endforeach?>
                                    
                                </tbody>
                            </table>
                        </div>
                        <?php if($status['status_id'] == 1):?>
                            <form action="<?=url('/orders/request')?>" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate>
                                <?=csrf_field()?>
                                <input type="hidden" name="order_id" value="<?=$store['order_id']?>" />
                                <button type="submit" class="btn-confirm btn btn-sm btn-primary">Запросить возможность</button>
                            </form>
                        <?php elseif($status['status_id'] == 6):?>
                                
                            <a class="btn btn-primary btn-sm" href="#ordering_<?=$store['order_id']?>" data-toggle="modal">Оформить заказ</a>
                            
                        <?php else:?>
                            <a class="btn btn-primary btn-sm" href="#ordering_<?=$store['order_id']?>" data-toggle="modal">Изменить заказ</a>
                        <?php endif?>
                        <br />
                        
                        <div id="ordering_<?=$store['order_id']?>" tabindex="-1" role="dialog" class="modal fade">
                        <div class="modal-dialog big">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                                    <h4 class="modal-title">Оформление заказа № <?=$store['order_id']?></h4>
                                </div>
                                <div class="modal-body">
                                
                                    <strong>Склад: <?=$store['store']?></strong>
                                    
                                    <div class="panel panel-default panel-block">

                                        <table class="table table-bordered table-striped">
                                            <thead>
                                                <tr>
                                                    <th>Код ресурса</th>
                                                    <th>Ресурс</th>
                                                    <th>Ед. изм</th>
                                                    <th>Кол-во</th>
                                                    <th>Название</th>
                                                    <th>Арт.</th>
                                                    <th>Цена</th>
                                                    <th>Итого</th>
                                        
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <?php foreach ($store['result'] as $item):?>
                                                    <tr class="gradeX">
                                                        <td><?=$item->code_resource?></td>
                                                        <td><?=$item->name_resource?></td>
                                                        <td><?=$item->measurement_resource?></td>
                                                        <td><?=$item->count?></td>
                                                        <td><?=$item->product_name?></td>
                                                        <td><?=$item->product_article?></td>
                                                        <td><?=$item->product_price?></td>
                                                        <td><?=$item->total?></td>
                                        
                                                    </tr>
                                                <?php endforeach?>
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                        <div class="form-group">
                                            <form action="<?=url('/print/waybill')?>" method="post" id="get-waybill-form">
                                                <?=csrf_field()?>
                                                <input type="hidden" name="order_id" value="<?=$store['order_id']?>" />
                                            </form>
                                            
                                            <form action="<?=url('/orders/buy')?>" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate>
                                            <input type="hidden" name="order_id" value="<?=$store['order_id']?>" />
                                            <?=csrf_field()?>
                                                <label>Обязуемся забрать товар:<span class="text-danger">*</span></label>
                                                <input type="text" value="<?=old('shipping_date')?>" name="shipping_date" class="datetimepicker-month form-control <?=($errors->has('shipping_date'))?'parsley-error':''?>">
                                                <ul id="parsley-6099321319480859" class="parsley-error-list">
                                                    <?php foreach ($errors->get('shipping_date') as $message):?>
                                                        <li class="required" style="display: list-item;"><?=$message?></li>
                                                    <?php endforeach?>
                                                </ul>

                                                <div class="modal-footer">
                                                    <button type="submit" class="-btn-confirm btn btn-sm btn-primary">Оформить заказ</button>
                                                    <button type="button" class="btn btn-sm btn-primary" id="getWaybill">Скачать товарную накладную</button>
                                                    <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Закрыть</button>
                                                </div>
                                            </form>
                                        </div>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->
                    <?php endforeach?>  
                </div>
            </div>

    <?php endforeach?>

</section>

<div id="addObjectModal" tabindex="-1" role="dialog" class="modal fade">
    <div class="modal-dialog big">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Подбор товара</h4>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-lg btn-default" data-dismiss="modal">Закрыть</button>
            </div>
        </div><!-- /.modal-content -->
    </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<script src="/scripts/bootstrap.min.js"></script>

<!-- Proton base scripts: -->

<script src="/scripts/main.js"></script>
<script src="/scripts/proton/common.js"></script>
<script src="/scripts/proton/main-nav.js"></script>
<script src="/scripts/proton/user-nav.js"></script>

<script src="/scripts/proton/forms.js"></script>
<!-- uniformJs -->
<script src="/scripts/vendor/jquery.uniform.min.js"></script>

<!-- Page-specific scripts: -->
<script src="/scripts/proton/sidebar.js"></script>
<script src="/scripts/proton/tables.js"></script>
<!-- jsTree -->
<script src="/scripts/vendor/jquery.jstree.js"></script>
<!-- Data Tables -->
<!-- http://datatables.net/ -->
<script src="/scripts/vendor/jquery.dataTables.min.js"></script>

<!-- Data Tables for BS3 -->
<!-- https://github.com/Jowin/Datatables-Bootstrap3/ -->
<!-- NOTE: Original JS file is modified -->
    <script src="/scripts/vendor/datatables.js"></script>
<!-- Select2 Required To Style Datatable Select Box(es) -->
<!-- https://github.com/fk/select2-bootstrap-css -->
    <script src="/scripts/vendor/select2.min.js"></script>

<!-- Date Time Picker -->
<!-- https://github.com/smalot/bootstrap-datetimepicker -->
<!-- NOTE: Original JS file is modified: Proton is forcing bootstrap 2 plugin mode in order to support font icons -->
<script src="/scripts/vendor/bootstrap-datetimepicker.js"></script>
