<?php $this->layout('layout/main') ?>        
        <?=$main_menu?>
       
        <section class="wrapper retracted scrollable">
            
           <?=$panel?>            
            <div class="row">
                <div class="col-md-12">
                    <h4>Получатель:</h4>    
                    <strong>Склад отгрузки: <?=$store->street?> <?=$store->store_house_number?> <?=$store->store_housing?> <?=$store->store_building?></strong>
                    <div class="panel panel-default panel-block">
                    	
                        <table class="table table-bordered table-striped">
                            <thead>
                                <tr>
                                    <th>Код ресурса</th>
                                    <th>Ресурс</th>
                                    <th>Ед. изм</th>
                                    <th>Кол-во</th>
                                    <th>Название</th>
                                    <th>Арт.</th>
                                    <th>Цена</th>
                                    <th>Итого</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach ($collection as $item):?>
                                    <tr class="gradeX">

                                        <td><?=$item->code_resource?></td>
                                        <td><?=$item->name_resource?></td>
                                        <td><?=$item->measurement_resource?></td>
                                        <td><?=$item->count?></td>
                                        <td><?=$item->product_name?></td>
                                        <td><?=$item->product_article?></td>
                                        <td><?=$item->product_price?></td>
                                        <td><?=$item->total?></td>
                                    </tr>
                                <?php endforeach?>
                                
                            </tbody>
                        </table>
                    </div>
                    <?php if($item->status_id == 2):?>
                    
                        <form action="<?=url('/orders/confirm')?>" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate>
                            <?=csrf_field()?>
                            <input type="hidden" name="order_id" value="<?=$order_id?>" />
                            <button type="submit" class="-btn-confirm btn btn-sm btn-primary">Подтвердить</button>
                            <a class="btn btn-primary btn-sm" href="#addComment" data-toggle="modal">Отказать</a>
                        </form>
                        <br />
                    
                    <?php elseif($item->status_id == 7 ):?>
                        <form action="<?=url('/print/waybill')?>" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate>
                            <?=csrf_field()?>
                            <input type="hidden" name="order_id" value="<?=$order_id?>" />
                            <button type="submit" class="-btn-confirm btn btn-sm btn-primary">Распечатать товарную накладную</button>
                            <a class="btn btn-primary btn-sm" href="#shipping" data-toggle="modal">Товар отгружен</a>
                        </form>

                        <br />

                    <?php endif?>
                </div>
            </div>
        </section>

        <div id="addComment" tabindex="-1" role="dialog" class="modal fade">
            <div class="modal-dialog big">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Укажите причину отказа</h4>
                    </div>
                    <div class="modal-body">
                    <form action="<?=url('/orders/refuse')?>" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate>
                        <input type="hidden" name="order_id" value="<?=$order_id?>" />
                        <?=csrf_field()?>

                        <textarea name="comment" class="form-control <?=($errors->has('comment'))?'parsley-error':''?>"  data-parsley-required="true"><?=old('street')?></textarea>
                        <ul id="parsley-6099321319480859" class="parsley-error-list">
                            <?php foreach ($errors->get('comment') as $message):?>
                                <li class="required" style="display: list-item;"><?=$message?></li>
                            <?php endforeach?>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="-btn-confirm btn btn-sm btn-primary">Подтвердить</button>
                        <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Закрыть</button>
                    </div>
                    </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

        <div id="shipping" tabindex="-1" role="dialog" class="modal fade">
            <div class="modal-dialog big">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                        <h4 class="modal-title">Отгрузка товара</h4>
                    </div>
                    <div class="modal-body">
                    <form action="<?=url('/orders/shipping')?>" method="post" enctype="multipart/form-data" data-parsley-namespace="data-parsley-" data-parsley-validate>
                        <input type="hidden" name="order_id" value="<?=$order_id?>" />
                        <?=csrf_field()?>
                        <label>Изображение<span class="text-danger">*</span></label>
                        
                        <div class="form-group">
                            <div class="fileinput fileinput-new" data-provides="fileinput">
                              <div class="fileinput-preview thumbnail" data-trigger="fileinput" style="width: 188px; height: 150px;"></div>
                              <div>
                                <span class="btn btn-primary btn-file"><span class="fileinput-new">Выбрать</span><span class="fileinput-exists">Изменить</span><input type="file" name="waybill"></span>
                                <a href="#" class="btn btn-primary fileinput-exists" data-dismiss="fileinput">Удалить</a>
                              </div>
                            </div>
                        </div>
                        <ul id="parsley-6099321319480859" class="parsley-error-list">
                            <?php foreach ($errors->get('waybill') as $message):?>
                                <li class="required" style="display: list-item; text-align:left;"><?=$message?></li>
                            <?php endforeach?>
                        </ul>
                    </div>
                    <div class="modal-footer">
                        <button type="submit" class="-btn-confirm btn btn-sm btn-primary">Подтвердить</button>
                        <button type="button" class="btn btn-sm btn-primary" data-dismiss="modal">Закрыть</button>
                    </div>
                    </form>
                    </div>
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

<script src="/scripts/bootstrap.min.js"></script>

<!-- Proton base scripts: -->

<script src="/scripts/main.js"></script>
<script src="/scripts/proton/common.js"></script>
<script src="/scripts/proton/main-nav.js"></script>
<script src="/scripts/proton/user-nav.js"></script>



<!-- Page-specific scripts: -->
<script src="/scripts/proton/sidebar.js"></script>
<script src="/scripts/proton/tables.js"></script>
<!-- jsTree -->
<script src="/scripts/vendor/jquery.jstree.js"></script>
<!-- Data Tables -->
<!-- http://datatables.net/ -->
<script src="/scripts/vendor/jquery.dataTables.min.js"></script>

<!-- Data Tables for BS3 -->
<!-- https://github.com/Jowin/Datatables-Bootstrap3/ -->
<!-- NOTE: Original JS file is modified -->
    <script src="/scripts/vendor/datatables.js"></script>
<!-- Select2 Required To Style Datatable Select Box(es) -->
<!-- https://github.com/fk/select2-bootstrap-css -->
    <script src="/scripts/vendor/select2.min.js"></script>
    <script src="/scripts/vendor/fileinput.js"></script>