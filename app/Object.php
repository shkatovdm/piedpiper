<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Object extends Model
{
	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'object';
	
	public static $visibleFields = [
              
							'district',
							'city_id',
							'street',
							'house_number',
							'housing',
							'building',
							'address_condominium',
							'year_commissioning',
							'area',
							'area_full',
							'floors_count',
							'entrances_count',
							'heat_supply',
						    'year_work_heat_meter',
						    'year_work_hot_water',
						    'year_work_hot_water_meter',
						    'year_work_cold_water',
						    'year_work_cold_water_meter',
						    'year_work_sewerage',
						    'year_work_electric',
						    'year_work_electric_meter',
						    'year_work_gas',
						    'year_work_gas_meter',
						    'year_work_roof',
						    'year_work_facade',
						    'year_work_lift',
						    'year_work_basement',
						    'year_work_foundation',
						    'total_cost',
						    'conclusion',
						    'estimated_cost_locally',
	];

	public static function getList()
	{   
		return DB::table('object')
			->join('city', 'city.id', '=', 'object.city_id')
			->select(
				'object.id', 
				'object.street', 
				'object.house_number', 
				'object.housing', 
				'object.building', 
				'object.person_charge', 
				'object.file_processed', 
				'object.city_id', 
				'city.name as city_name',
				'object.district',
				'object.address_condominium',
				'object.year_commissioning',
				'object.area',
				'object.area_full',
				'object.entrances_count',
				'object.floors_count',
				'object.heat_supply',
			    'object.year_work_heat_meter',
			    'object.year_work_hot_water',
			    'object.year_work_hot_water_meter',
			    'object.year_work_cold_water',
			    'object.year_work_cold_water_meter',
			    'object.year_work_sewerage',
			    'object.year_work_electric',
			    'object.year_work_electric_meter',
			    'object.year_work_gas',
			    'object.year_work_gas_meter',
			    'object.year_work_roof',
			    'object.year_work_facade',
			    'object.year_work_lift',
			    'object.year_work_basement',
			    'object.year_work_foundation',
			    'object.total_cost',
			    'object.conclusion',
			    'object.estimated_cost_locally'
			);
	}

	public static function getExecutorObjects($id)
	{   
		return DB::table('object')
			->join('city', 'city.id', '=', 'object.city_id')
			->select(
				'object.id', 
				'object.street', 
				'object.house_number', 
				'object.housing', 
				'object.building', 
				'object.person_charge', 
				'object.file_processed', 
				'object.city_id', 
				'city.name as city_name'
			);
	}

	public static function getFree()
	{   
		$exist = [];

		$object_lot = DB::table('object_lot')->select('object_id')->get();
		foreach ($object_lot as $value) {
			
			$exist[] = $value->object_id;
		}
		//Если еще ни один объект не привязан к лоту
		if(empty($exist)){

			return DB::table('object')
				->join('city', 'city.id', '=', 'object.city_id')
				->select(
					'object.id', 
					'object.street', 
					'object.house_number', 
					'object.housing', 
					'object.building', 
					'object.person_charge', 
					'object.file_processed', 
					'object.city_id', 
					'city.name as city_name'
				);

		}
		
		return DB::table('object')
			->join('city', 'city.id', '=', 'object.city_id')
			->select(
				'object.id', 
				'object.street', 
				'object.house_number', 
				'object.housing', 
				'object.building', 
				'object.person_charge', 
				'object.file_processed', 
				'object.city_id', 
				'city.name as city_name'
			)
			->whereNotIn('object.id', $exist);
	}
}
