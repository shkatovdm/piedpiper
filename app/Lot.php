<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Lot extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'lot';

    public static function getList()
    {   
        return DB::table('lot')
            ->join('users', 'users.id', '=', 'lot.executor_id')
            ->select(
                'lot.id', 
                'lot.number', 
                'lot.executor_id', 
                'lot.contract_number', 
                'lot.cost_contract', 
                'lot.cost_initial_maximum', 
                'users.name as user_name',
                DB::raw('lot.cost_contract/lot.cost_initial_maximum as fall_factor_cost')
            );
    }
    
    public static function GetByID($id)
    {   
        return DB::table('lot')
            ->select(
                'lot.id as id', 
                'lot.number as number', 
                'lot.executor_id as executor_id', 
                'lot.contract_number as contract_number', 
                'lot.cost_contract as cost_contract', 
                'lot.cost_initial_maximum as cost_initial_maximum', 
                DB::raw('lot.cost_contract/lot.cost_initial_maximum as fall_factor_cost')
            )
            ->where('lot.id', $id);
    }
}
