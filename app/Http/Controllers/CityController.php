<?php

namespace App\Http\Controllers;

use Auth;
use Helpers;
//use Pied\Helpers;
use App\User;
use App\City;
use View;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class CityController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'getLogout']);
        $this->middleware('isAdmin');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function index()
    {	
        $data = [];
        $panel['title'] = 'Города';
    	$panel['add_url'] = 'city/add';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        $data['city'] = City::orderBy('created_at', 'DESC')->paginate(10);

        return View::make('city/list', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }

    public function add()
    {   
        $data = [];
        $panel['back_url'] = 'city';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());        

        return View::make('city/add', $data)
                                        ->nest('panel', 'sections.panel', $panel)
                                        ->nest('main_menu', $sSidebar);
    }
    

    public function process(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                    'name' => 'required|unique:city|max:255',
        ]);

        if ($oValidator->fails()) {
            return redirect('city/add')
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $oCity = new City;
        $oCity->name = $request->name;
        $oCity->save();

        $request->session()->flash('notify', ['type' =>'Success', 'text'   =>'Данные успешно сохранены!']);
        return redirect('city');
    }

    public function detail(Request $request)
    {   
        $data  = [];
        $panel = [];

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        $data['city'] = City::find($request->id);

        return View::make('city/detail', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }
    
    public function update(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                    'name' => 'required|unique:city|max:255',
        ]);

        if ($oValidator->fails()) {
            return redirect('city/add')
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $oCity = City::find($request->id);
        $oCity->name = $request->name;
        $oCity->save();
        $request->session()->flash('notify', ['type' =>'Success', 'text'   =>'Данные успешно обновлены!']);

        return redirect('city');
    }    

    public function remove(Request $request)
    {   
        $oCity = City::find($request->id);
        $oCity->delete();
        $request->session()->flash('notify', ['type' =>'Success', 'text'   =>'Данные успешно удалены!']);

        return redirect('city');
    }
}
