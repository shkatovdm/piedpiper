<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Helpers;
use App\User;
use App\Resource;
use Storage;
use File;
use View;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class ResourceController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'getLogout']);

    }

    public function index()
    {   
        $data = [];
        $panel['title'] = 'Коды ресурсов';
        //$panel['add_url'] = 'resource/add';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        //$data['permission'] = Helpers::getPermission('Object', $oUser->getRoleCurrUser());
        $data['collection'] = Resource::orderBy('created_at', 'DESC')->paginate(10);        

        return View::make('resource/list', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }

    public function add()
    {   
        $data = [];
        $panel['title'] = 'Загрузка кодов ресурсов';
        //$panel['back_url'] = 'lots';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());   
        //$data['executor'] = User::where('role_id', 4)->get();

        return View::make('resource/add', $data)
                                        ->nest('panel', 'sections.panel', $panel)
                                        ->nest('main_menu', $sSidebar);
    }

    public function process(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                                    'userfile'            => 'required',
        ]);

        if ($oValidator->fails()) {

            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }

        $oFile = $request->file('userfile');
        $sOriginalFileName = $oFile->getClientOriginalName();
        $sExtension = $oFile->getClientOriginalExtension();
        Storage::disk('local')->put($oFile->getFilename() . '.' . $sExtension,  File::get($oFile)); 
        $sFileName = $oFile->getFilename() . '.' . $sExtension;

        $Object = new Object;
        
        $Object->city_id             = $request->city_id;
        $Object->street              = $request->street;
        $Object->house_number        = $request->house_number;
        $Object->housing             = $request->housing;
        $Object->building            = $request->building;
        $Object->person_charge       = $request->person_charge;
        $Object->phone_person_charge = $request->phone_person_charge;
        $Object->funding             = $request->funding;
        $Object->file_name           = $sFileName;
        $Object->original_file_name  = $sOriginalFileName;
        $Object->file_processed      = 0;

        $Object->save();
        
        $request->session()->flash('notify', ['type' =>'Success', 'text'   =>'Данные успешно сохранены!']);
        return redirect('objects');
    }
}
