<?php

namespace App\Http\Controllers;

use Auth;
use Helpers;
use App\User;
use App\Role;
use App\Org;
use View;
use Validator;
use Services\UserService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class UserController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    private $actions;
    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function index()
    {	
        $data = [];
        $panel['title'] = 'Пользователи';
    	$panel['add_url'] = 'users/add';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());

        $data['permission'] = Helpers::getPermission('Object', $oUser->getRoleCurrUser());
        $data['collection'] = $oUser->getUsers();

        return View::make('user/list', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }

    public function add()
    {   
        $data = [];
        $oUser = Auth::user();
        
        $data['roles']      = UserService::getRolesList();

        $panel['title'] = 'Пользователи';
        $panel['back_url'] = 'users';

        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());        

        return View::make('user/add', $data)
                                        ->nest('panel', 'sections.panel', $panel)
                                        ->nest('main_menu', $sSidebar);
    }
    
    public function process(Request $request)
    {   
        if($request->ajax()) return $this->processAddAjax($request);

        return $this->processAdd($request);
    }

    public function processAddAjax(Request $request)
    {   
        $data['roles'] = UserService::getRolesList();
        $data['request'] = $request->all();
        
        $oValidator = Validator::make($request->all(), [
                                                    'name'                  => 'required|max:255|unique:users',
                                                    'email'                 => 'required|email|max:255|unique:users',
                                                    'password'              => 'required|min:6|max:255|confirmed',
                                                    'password_confirmation' => 'required|min:6|max:255',
                                                    'role_id'               => 'required|integer|max:255',
                                                    'organization_name'     => 'required|max:255',
                                                    'organization_address'  => 'required|max:255',
                                                    'phone'                 => 'required|unique:executor|max:255',
                                                    'inn'                   => 'required|unique:executor|max:255',
                                                    'ppc'                   => 'required|unique:executor|max:255',
                                                    'bin'                   => 'required|unique:executor|max:255',
                                                    'head'                  => 'required|max:255',
                                                    'chief_accountant'      => 'required|max:255',
                                                    'checking_account'      => 'required|unique:executor|max:255',
                                                    'bank'                  => 'required|max:255',
                                                    'bic_bank'              => 'required|unique:executor|max:255',
                                                    'inn_bank'              => 'required|unique:executor|max:255',
                                                    'correspondent_account' => 'required|unique:executor|max:255',
        ]);
        
        $oUser = Auth::user();
        
        if ($oValidator->fails()) {

            return View::make('user/addForm', $data)
                            ->withErrors($oValidator);
        }

        $Org = new Org;
        $Org->name                    = $request->organization_name;
        $Org->address                 = $request->organization_address;
        $Org->phone                   = $request->phone;
        $Org->inn                     = $request->inn;
        $Org->ppc                     = $request->ppc;
        $Org->bin                     = $request->bin;
        $Org->head                    = $request->head;
        $Org->chief_accountant        = $request->chief_accountant;
        $Org->checking_account        = $request->checking_account;
        $Org->bank                    = $request->bank;
        $Org->bic_bank                = $request->bic_bank;
        $Org->inn_bank                = $request->inn_bank;
        $Org->correspondent_account   = $request->correspondent_account;
        $Org->type                    = 'executor';
        
        $Org->save();

        $user = new User;
        $user->name        = $request->name;
        $user->email       = $request->email;
        $user->password    = bcrypt($request->password);
        $user->role_id     = 4;
        $user->org_id      = $Org->id;
        $user->save();

        Helpers::showSuccess($request);

        return 'Готово';
    }

    public function processAdd(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                                    'name'		  			=> 'required|max:255|unique:users',
                                                    'email'  				=> 'required|email|max:255|unique:users',
                                                    'password'  			=> 'required|min:6|max:255|confirmed',
                                                    'password_confirmation' => 'required|min:6|max:255',
                                                    'role_id'   			=> 'required|integer|max:255',
        ]);
        
        $oUser = Auth::user();
        if (Helpers::getRoleByID($request->role_id) == 'admin' && $oUser->getRoleCurrUser() != 'admin'){
            
            abort(403, 'Forbidden!');
        }
        
        if ($oValidator->fails()) {

            return redirect('users/add')
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $oUser = new User;
        $oUser->name        = $request->name;
        $oUser->email       = $request->email;
        $oUser->password    = bcrypt($request->password);
        $oUser->role_id     = $request->role_id;
        $oUser->org_id      = 1;
        $oUser->save();

        Helpers::showSuccess($request);
        return redirect('users');
    }

    public function edit(Request $request)
    {   
        $data  = [];
        $data['roles']      = UserService::getRolesList();
        $panel['title']     = 'Пользователи';
        $panel['back_url']  = 'users';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        $data['user'] = User::find($request->id);

        return View::make('user/edit', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }
    
    public function update(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                 					'name'		            => 'required|max:255|unique:users,id,' . $request->id,
                                                    'email'  	            => 'required|email|max:255|unique:users,id,' . $request->id,
                                                    'password'              => 'min:6|max:255|confirmed',
                                                    'password_confirmation' => 'min:6|max:255',
                                                    'role_id'               => 'required|integer|max:255',
        ]);
        if ($oValidator->fails()) {
            return redirect('users/edit/' . $request->id)
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $user = User::find($request->id);

        $user->name                    = $request->name;
        $user->email                   = $request->email;
        $user->role_id                 = $request->role_id;
        $user->org_id      = 1;
        
        if(!empty($request->password)) {
            
            $user->password = bcrypt($request->password);
        }
        $user->save();
        Helpers::showSuccess($request);

        return redirect('users/edit/' . $request->id);
    }    

    public function remove(Request $request)
    {   
        $oUser = Auth::user();
        
        if($oUser->getRoleCurrUser() === 'admin') 
        {    
            $ouser = user::find($request->id);
            $ouser->delete();
            $request->session()->flash('notify', ['type' =>'Success', 'text'   =>'Данные успешно удалены!']);
            return redirect('users');
        
        } else {

            abort(403, 'Unauthorized!');
        }
    }

    public function profile(Request $request)
    {   
        if(isset($request->id)) {
            
            $oUser = user::find($request->id);
            
        } else{
            
            $oUser = Auth::user();
        }
        $data  = [];
        $panel['title'] = 'Профиль';

        
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        $data['user'] = $oUser;
        $data['org']  = Org::find($oUser->org_id);
        return View::make('user/public_profile', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }

    public function getExecutors()
    {   
        return response()->json(['executors' => User::getExecutors()->get()]);
    }
}
