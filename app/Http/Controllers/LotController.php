<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Helpers;
use App\User;
use App\Lot;
use App\City;
use App\Object;
use App\ObjectLot;
use Services\ObjectService;
use Services\LotService;
use Services\UserService;
use View;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class LotController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'getLogout']);

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function index()
    {	
        $data = [];
        $panel['title'] = 'Лоты';
    	$panel['add_url'] = 'lots/add';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        $data['collection'] = Lot::getList()->orderBy('lot.created_at', 'DESC')->paginate(10);

        return View::make('lot/list', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }

    public function add()
    {   
        $data = [];
        $panel['title'] = 'Лоты';
        $panel['back_url'] = 'lots';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());   
        $data['roles'] = UserService::getRolesList();
        $data['executor'] = User::getExecutors()->get();

        return View::make('lot/add', $data)
                                        ->nest('panel', 'sections.panel', $panel)
                                        ->nest('main_menu', $sSidebar);
    }
    

    public function process(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                                    'number'                => 'required|unique:lot|max:255',
                                                    'executor_id'           => 'required|integer|max:255',
                                                    'contract_number'       => 'required|max:255',
                                                    'cost_contract'         => 'required|max:255',
                                                    'cost_initial_maximum'  => 'required|max:255',
        ]);

        if ($oValidator->fails()) {

            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $Lot = new Lot;
        $Lot->number               = $request->number;
        $Lot->executor_id          = $request->executor_id;
        $Lot->contract_number      = $request->contract_number;
        $Lot->cost_contract        = $request->cost_contract;
        $Lot->cost_initial_maximum = $request->cost_initial_maximum;
        $Lot->save();

        Helpers::showSuccess($request);
        return redirect('lots/edit/' . $Lot->id);
    }

    public function edit(Request $request)
    {   
        $data  = [];
        $panel['title'] = 'Лоты';
        $panel['back_url'] = 'lots';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        $data['city'] = City::all();
        $data['lot'] = Lot::GetByID($request->id)->first();
        //dd($data['lot']);
        $data['executor'] = User::where('role_id', 4)->get();
        $data['collection'] = ObjectLot::getListForLot($request->id)->orderBy('object_lot.created_at', 'DESC')->paginate(10);
        $data['searchObject'] = Object::getFree()->orderBy('object.created_at', 'DESC')->paginate(5); 
        $data['searchObject']->setPath(url('/objects/listAJAX'));
        
        return View::make('lot/edit', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }
    
    public function update(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                                    'number'                => 'required|max:255|unique:lot,id,' . $request->id,
                                                    'executor_id'           => 'required|integer|max:255',
                                                    'contract_number'       => 'required|max:255',
                                                    'cost_contract'         => 'required|max:255',
                                                    'cost_initial_maximum'  => 'required|max:255',
        ]);

        if ($oValidator->fails()) {
            //dd($oValidator);
            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }

        $Lot = Lot::find($request->id);
        $Lot->number               = $request->number;
        $Lot->executor_id          = $request->executor_id;
        $Lot->contract_number      = $request->contract_number;
        $Lot->cost_contract        = $request->cost_contract;
        $Lot->cost_initial_maximum = $request->cost_initial_maximum;
        $Lot->save();
        $request->session()->flash('notify', [
                                            'type' =>'success', 
                                            'title' => 'Успешно', 
                                            'text'   =>'Данные успешно обновлены!']);

        return redirect('lots/edit/' . $request->id);
    }    

    public function remove(Request $request)
    {   
        $oUser = Auth::user();
        
            $Lot = Lot::find($request->id);
            $Lot->delete();
            $request->session()->flash('notify', [
                                                'type' =>'success', 
                                                'title' => 'Успешно', 
                                                'text'   =>'Данные успешно удалены!'
            ]);
            return redirect()->back();
    }

    public function addObject(Request $request)
    {       
        $oValidator = Validator::make($request->all(), [
                                                    'lot_id'    => 'required|integer|max:255',
                                                    'object_id' => 'required|integer|max:255',
        ]);

        if ($oValidator->fails()) {
            $request->session()->flash('notify', [
                                                'type' =>'error', 
                                                'title' => 'Ошибка', 
                                                'text'   =>'Не удалось добавить объект!'
            ]);
            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $ObjectLot = new ObjectLot;
        
        $ObjectLot->lot_id = $request->lot_id;
        $ObjectLot->object_id = $request->object_id;
        
        $ObjectLot->save();
        
        $request->session()->flash('notify', [
                                            'type'  => 'success', 
                                            'title' => 'Успешно', 
                                            'text'  =>'Объект успешно добавлен!'
        ]);
        return redirect()->back();
    }

    public function addObjectAJAX(Request $request)
    {   

        $data['city'] = City::all();
        $data['executor'] = User::where('role_id', 4)->get();
        $data['request'] = $request->all();
        $oValidator = Validator::make($request->all(), [
                                                    'city_id'             => 'required|integer',
                                                    'street'              => 'required|max:255',
                                                    'house_number'        => 'required|integer',
                                                    'housing'             => 'max:255',
                                                    'building'            => 'max:255',
                                                    'person_charge'       => 'max:255',
                                                    'phone_person_charge' => 'max:255',
                                                    'funding'             => 'max:255',
                                                    'userfile'            => 'required',
        ]);

        if ($oValidator->fails()) {

            return View::make('object/addAJAX', $data)
                            ->withErrors($oValidator);
        }

        $Object = ObjectService::add($request);
        LotService::addObjectToLot($request->lot_id, $Object->id);
        Helpers::showSuccess($request);

        return 'Готово';
    }

    public function removeObject(Request $request)
    {   
        $ObjectLot = ObjectLot::where('lot_id', $request->lot_id)->where('object_id', $request->id);
        $ObjectLot->delete();
        $request->session()->flash('notify', [
                                            'type' =>'success', 
                                            'title' => 'Успешно', 
                                            'text'   =>'Объект удален из лота!'
        ]);
        return redirect()->back();
    }
}
