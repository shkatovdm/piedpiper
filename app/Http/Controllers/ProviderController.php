<?php

namespace App\Http\Controllers;

use Auth;
use Helpers;
//use Pied\Helpers;
use App\User;
use App\Org;
use View;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class ProviderController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'getLogout']);

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function index()
    {	
        $data = [];
        $panel['title'] = 'Поставщики';
    	$panel['add_url'] = 'providers/add';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        $data['providers'] = Org::where('type', 'provider')->orderBy('created_at', 'DESC')->paginate(10);

        return View::make('provider/list', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }

    public function add()
    {   
        $data = [];
        $panel['title'] = 'Поставщики';
        $panel['back_url'] = 'providers';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());        

        return View::make('provider/add', $data)
                                        ->nest('panel', 'sections.panel', $panel)
                                        ->nest('main_menu', $sSidebar);
    }
    
    public function addUserAjax(Request $request)
    {   
        $data['roles']      = UserService::getRolesList();
        $data['request']    = $request->all();

        $oValidator = Validator::make($request->all(), [
                                                    'name'                  => 'required|max:255|unique:users',
                                                    'email'                 => 'required|email|max:255|unique:users',
                                                    'password'              => 'required|min:6|max:255|confirmed',
                                                    'password_confirmation' => 'required|min:6|max:255',
                                                    'role_id'               => 'required|integer|max:255',
                                                    'org_id'                => 'required|integer|max:255',
        ]);
        
        $oUser = Auth::user();
        if (Helpers::getRoleByID($request->role_id) == 'admin' && $oUser->getRoleCurrUser() != 'admin'){
            
            abort(403, 'Forbidden!');
        }
        
        if ($oValidator->fails()) {

            return View::make('executor/addUser', $data)
                            ->withErrors($oValidator);
        }

        $user = new User;
        $user->name        = $request->name;
        $user->email       = $request->email;
        $user->password    = bcrypt($request->password);
        $user->role_id     = $request->role_id;
        $user->org_id      = $request->org_id;
        $user->save();

        Helpers::showSuccess($request);

        return 'Готово';
    }

    public function process(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                                    'name'                  => 'required|max:255|unique:org',
                                                    'address'               => 'required|max:255',
                                                    'phone'                 => 'required|unique:provider|max:255',
                                                    'inn'                   => 'required|unique:provider|max:255',
                                                    'ppc'                   => 'required|unique:provider|max:255',
                                                    'bin'                   => 'required|unique:provider|max:255',
                                                    'head'                  => 'required|max:255',
                                                    'chief_accountant'      => 'required|max:255',
                                                    'checking_account'      => 'required|unique:org|max:255',
                                                    'bank'                  => 'required|max:255',
                                                    'bic_bank'              => 'required|unique:provider|max:255',
                                                    'inn_bank'              => 'required|unique:provider|max:255',
                                                    'correspondent_account' => 'required|unique:provider|max:255',
        ]);

        if ($oValidator->fails()) {

            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $Org = new Org;
        $Org->name                    = $request->name;
        $Org->address                 = $request->address;
        $Org->phone                   = $request->phone;
        $Org->inn                     = $request->inn;
        $Org->ppc                     = $request->ppc;
        $Org->bin                     = $request->bin;
        $Org->head                    = $request->head;
        $Org->chief_accountant        = $request->chief_accountant;
        $Org->checking_account        = $request->checking_account;
        $Org->bank                    = $request->bank;
        $Org->bic_bank                = $request->bic_bank;
        $Org->inn_bank                = $request->inn_bank;
        $Org->correspondent_account   = $request->correspondent_account;
        $Org->type                    = 'provider';
        
        $Org->save();

        Helpers::showSuccess($request);

        return redirect('providers/edit/' . $Org->id);
    }

    public function edit(Request $request)
    {   
        $data  = [];
        $panel['title'] = 'Поставщики';
        $panel['back_url'] = 'providers';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        $data['users']      = User::where('org_id', $request->id)->paginate(10);
        $data['entity']     = Org::find($request->id);
        $data['freeUser']   = User::where('org_id', 1)->where('role_id', 3)->paginate(10);
        $data['freeUser']->setPath(url('/user/listAJAX'));

        return View::make('provider/edit', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }
    
    public function update(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                                    'name'                  => 'required|max:255',
                                                    'address'               => 'required|max:255',
                                                    'phone'                 => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'inn'                   => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'ppc'                   => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'bin'                   => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'head'                  => 'required|max:255',
                                                    'chief_accountant'      => 'required|max:255',
                                                    'checking_account'      => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'bank'                  => 'required|max:255',
                                                    'bic_bank'              => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'inn_bank'              => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'correspondent_account' => 'required|max:255|unique:provider,id,' . $request->id,
        ]);

        if ($oValidator->fails()) {
            dd($oValidator->errors());
            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $Org = Org::find($request->id);
        $Org->name                    = $request->name;
        $Org->address                 = $request->address;
        $Org->phone                   = $request->phone;
        $Org->inn                     = $request->inn;
        $Org->ppc                     = $request->ppc;
        $Org->bin                     = $request->bin;
        $Org->head                    = $request->head;
        $Org->chief_accountant        = $request->chief_accountant;
        $Org->checking_account        = $request->checking_account;
        $Org->bank                    = $request->bank;
        $Org->bic_bank                = $request->bic_bank;
        $Org->inn_bank                = $request->inn_bank;
        $Org->correspondent_account   = $request->correspondent_account;
        $Org->description             = $request->description;
        $Org->save();
        
        Helpers::showSuccess($request);

        return redirect('providers/edit/' . $request->id);
    }    

    public function joinUser(Request $request)
    {       
        $oValidator = Validator::make($request->all(), [
                                                    'user_id' => 'required|integer|max:255',
                                                    'org_id'  => 'required|integer|max:255',
        ]);

        if ($oValidator->fails()) {
            $request->session()->flash('notify', [
                                                'type' =>'error', 
                                                'title' => 'Ошибка', 
                                                'text'   =>'Не удалось добавить объект!'
            ]);
            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $user = User::find($request->user_id);;
        $user->org_id      = $request->org_id;
        $user->save();
        
        Helpers::showSuccess($request);
        return redirect()->back();
    }

    public function unjoinUser(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                                    'user_id' => 'required|integer|max:255'
        ]);

        if ($oValidator->fails()) {
            $request->session()->flash('notify', [
                                                'type' =>'error', 
                                                'title' => 'Ошибка', 
                                                'text'   =>'Не удалось добавить объект!'
            ]);
            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $user = User::find($request->user_id);
        $user->org_id      = 1;
        $user->save();
        
        Helpers::showSuccess($request);
        return redirect()->back();
    }

    public function profile()
    {   
        $data  = [];
        $panel['title'] = 'Профиль';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        $data['user'] = $oUser;

        return View::make('user/myprofile', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }

    public function updateProfile(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                                    'name'                  => 'required|max:255',
                                                    'address'               => 'required|max:255',
                                                    'phone'                 => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'inn'                   => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'ppc'                   => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'bin'                   => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'head'                  => 'required|max:255',
                                                    'chief_accountant'      => 'required|max:255',
                                                    'checking_account'      => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'bank'                  => 'required|max:255',
                                                    'bic_bank'              => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'inn_bank'              => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'correspondent_account' => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'description' => 'max:255',
                                                    'logo'       => 'max:200000000|mimes:jpeg,jpg,JPG,JPEG,png,bmp',

        ]);

        if ($oValidator->fails()) {
        
            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }
        
        $logo = \Services\FileEntryService::add($request->file('logo'));
        
        $user = User::find($request->id);

        $user->name        = $request->name;
        $user->email       = $request->email;
        $user->logo        = $logo;
        $user->description = $request->description;
        $user->save();

        $Org = Org::find($user->org_id);
        $Org->name                    = $request->organization_name;
        $Org->address                 = $request->address;
        $Org->phone                   = $request->phone;
        $Org->inn                     = $request->inn;
        $Org->ppc                     = $request->ppc;
        $Org->bin                     = $request->bin;
        $Org->head                    = $request->head;
        $Org->chief_accountant        = $request->chief_accountant;
        $Org->checking_account        = $request->checking_account;
        $Org->bank                    = $request->bank;
        $Org->bic_bank                = $request->bic_bank;
        $Org->inn_bank                = $request->inn_bank;
        $Org->correspondent_account   = $request->correspondent_account;
        $Org->save();
        
        Helpers::showSuccess($request);

        return redirect('profile');
    }    
    public function remove(Request $request)
    {   
        $Org = Org::find($request->id);
        $Org->delete();
        
        Helpers::showSuccess($request);

        return redirect()->back();
    }
}
