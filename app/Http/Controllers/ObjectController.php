<?php

namespace App\Http\Controllers;

use Auth;
use File;
use Storage;
use Helpers;
use Fields;
use App\Lot;
use App\User;
use App\City;
use App\Sheet;
use App\Order;
use App\Object;
use App\Product;
use App\ObjectLot;
use View;
use Services\ObjectService;
use Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class ObjectController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'getLogout']);
        $this->middleware('permission');
    }

    public function index()
    {	
        $oUser = Auth::user();
        switch ($oUser->getRoleCurrUser()) {
            
            case 'executor': return $this->executorObjects(); break;
            
            default: return $this->operatorObjects(); break;
        }
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function operatorObjects()
    {

        $sort = Input::get('sort',false);
        $field = 'created_at';
        $direction = 'DESC';

        $data = [];
        $panel['title'] = 'Объекты';
        $panel['add_url'] = 'objects/add';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());

        $data['sort'] = $sort;

        if ($sort !== false) {
            $sort = explode(':', strtolower($sort));
            if (count($sort) > 1) {
                $field = $sort[0];
                $direction = 'DESC';
            } else {
                $field = $sort[0];
                $direction = 'ASC';
            }
        }

        $data['permission'] = Helpers::getPermission('Object', $oUser->getRoleCurrUser());
        $data['collection'] = Object::orderBy($field, $direction)->paginate(10);

        return View::make('object/list', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }

    /**
     * Объекты исполнителя
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function executorObjects()
    {   
        $data = [];
        $panel['title'] = 'Мои объекты';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        //$data['permission'] = Helpers::getPermission('Object', $oUser->getRoleCurrUser());
        $data['lots']       = Lot::where('executor_id', $oUser->id)->orderBy('created_at', 'DESC')->paginate(10);
        
        foreach ($data['lots'] as $value) {
            
            $lots[] = $value->id;
            //$iCountPositions = Lot::where('object_id', $oUser->id)->orderBy('created_at', 'DESC')->paginate(10);    
            $data['collection'][$value->id] = ObjectLot::getList($value->id)->orderBy('object.created_at', 'DESC')->paginate(10);
        }
        
        return View::make('object/my_object_list', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }

    public function add()
    {   
        $data = [];
        $oUser = Auth::user();

        $panel['title'] = 'Новый объект';
        $panel['back_url'] = 'objects';

        $data['city'] = City::all();
        $data['executor'] = User::where('role_id', 4)->get();

        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());

        return View::make('object/add', $data)
                                        ->nest('panel', 'sections.panel', $panel)
                                        ->nest('main_menu', $sSidebar);

    }

    public function process(Request $request)
    {

        //file_put_contents('1111.txt',var_export(Fields::check('object'),true));

        $oValidator = Validator::make($request->all(), Fields::check('object'));

        if ($oValidator->fails()) {

            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }

        Fields::add($request,'object');
        Helpers::showSuccess($request);

        return redirect('objects');
    }

    public function edit(Request $request)
    {   
        $data  = [];
        $panel['title'] = 'Объекты';
        $panel['back_url'] = 'objects';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        $data['city']       = City::all();
        $data['executor']   = User::where('role_id', 4)->get();
        $data['entity']     = Object::find($request->id);
        $data['collection'] = Sheet::where('object_id', $request->id)->orderBy('created_at', 'DESC')->paginate(10);    

        return View::make('object/newedit', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }

    public function update(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                                    'city_id'             => 'required|integer',
                                                    'street'              => 'required|max:255',
                                                    'house_number'        => 'required|integer',
                                                    'housing'             => 'max:255',
                                                    'building'            => 'max:255',
                                                    'person_charge'       => 'max:255',
                                                    'phone_person_charge' => 'max:255',
                                                    'funding'             => 'max:255'                                            
        ]);

        if ($oValidator->fails()) {
            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $Object = Object::find($request->id);
        
        $Object->city_id             = $request->city_id;
        $Object->street              = $request->street;
        $Object->house_number        = $request->house_number;
        $Object->housing             = $request->housing;
        $Object->building            = $request->building;
        $Object->person_charge       = $request->person_charge;
        $Object->phone_person_charge = $request->phone_person_charge;
        $Object->funding             = $request->funding;
        
        $Object->save();
        Helpers::showSuccess($request);

        return redirect('objects/edit/' . $request->id);
    }    
   
    public function removeFromLot(Request $request)
    {   
        $oprovider = Provider::find($request->id);
        $oprovider->delete();
        $request->session()->flash('notify', ['type' =>'Success', 'text'   =>'Данные успешно удалены!']);
        return redirect('objects');
    }
    
    public function getListAJAX(Request $request)
    {   
        $data['lot_id'] = $request->lot_id;
        $data['collection'] = Object::getFree()->orderBy('object.created_at', 'DESC')->paginate(5); 
        $data['collection']->setPath(url('/objects/listAJAX'));

        return View::make('object/listAJAX', $data);
    }

    public function getObjectLotAJAX(Request $request)
    {   
        $data['lot_id'] = $request->lot_id;
        $data['collection'] = ObjectLot::getList($request->lot_id)->orderBy('object.created_at', 'DESC')->paginate(10); 
        $data['collection']->setPath(url('/objects/listAJAX'));

        return View::make('object/listAJAX', $data);
    }

        public function getProductsListForAJAX(Request $request)
    {   
        $data['collection'] = Product::getByCode($code)->orderBy('object.created_at', 'DESC')->paginate(5); 
        $data['collection']->setPath(url('/products/listAJAX'));

        return View::make('products/listAJAX', $data);
    }
}
