<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Helpers;
use App\User;
use App\Order;
use App\Sheet;
use App\Object;
use App\Product;
use App\OrderRow;
use App\Status;
use View;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers,
    Services\FileEntryService;

class OrderController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'getLogout']);

    }

    /**
     * Список заказов поставщику
     *
     * @author Dmitriy Shkatov
     */
    public function index()
    {   
        $data = [];
        $panel['title'] = 'Заказы';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        $data['permission'] = Helpers::getPermission('Order', $oUser->getRoleCurrUser());
        $data['collection'] = Order::getOrderByProviderID($oUser->id)->orderBy('order.created_at', 'DESC')->paginate(10);
//dd($data['collection']);
        return View::make('orders/list', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }
    /**
     * Список заказов подтвержденных поставщиком
     *
     * @author Dmitriy Shkatov
     */
    public function approved()
    {   
        $data = [];
        $panel['title'] = 'Подтвержденные мной';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        $data['permission'] = Helpers::getPermission('Order', $oUser->getRoleCurrUser());
        $data['collection'] = Order::getOrderProviderByStatus($oUser->id, 4)->orderBy('order.created_at', 'DESC')->paginate(10);

        return View::make('orders/list', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }
    
    /**
     * Список заказов ожидающих отгрузки поставщиком
     *
     * @author Dmitriy Shkatov
     */
    public function awaitingShipment()
    {   
        $data = [];
        $panel['title'] = 'Ожидающие отргузки';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        $data['permission'] = Helpers::getPermission('Order', $oUser->getRoleCurrUser());
        $data['collection'] = Order::getOrderProviderByStatus($oUser->id, 7)->orderBy('order.created_at', 'DESC')->paginate(10);

        return View::make('orders/list', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }    

    /**
     * Список отгруженных заказов
     *
     * @author Dmitriy Shkatov
     */
    public function shipped()
    {	
        $data = [];
        $panel['title'] = 'Ожидающие отргузки';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        $data['permission'] = Helpers::getPermission('Order', $oUser->getRoleCurrUser());
        $data['collection'] = Order::getOrderProviderByStatus($oUser->id, 8)->orderBy('order.created_at', 'DESC')->paginate(10);

        return View::make('orders/list', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }
    
    /**
     * Страница редактирования закза поставщиком
     *
     * @author Dmitriy Shkatov
     */
    public function providerOrderEdit(Request $request)
    {   
        $data = [];
        $panel['title'] = 'Заказ №' . $request->id;
        $panel['back_url'] = 'orders';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        //$data['permission'] = Helpers::getPermission('Order', $oUser->getRoleCurrUser());
        $data['order_id']   = $request->id;
        $data['store']      = OrderRow::getStoreByOrderID($request->id)->first();
        $data['collection'] = OrderRow::getByID($request->id)->orderBy('order_row.created_at', 'DESC')->get();

        return View::make('orders/edit', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }

    public function processAdd(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                                    'product_id' => 'required|integer',
                                                    'sheet_id'   => 'required|integer',
        ]);

        if ($oValidator->fails()) {
            $request->session()->flash('notify', [
                                                'type' =>'error', 
                                                'title' => 'Ошибка', 
                                                'text'   =>'Не удалось выбрать товар!'
            ]);
            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }
        try {
            
            //ищем заказ от этого исполнителя на такой же склад
            $oUser = Auth::user();
            $orderExist = OrderRow::checkOrderExist($request->product_id, $oUser->id);
            if($orderExist && !empty($orderExist))
            {   
                //обновляем заказ                
                DB::beginTransaction();

                    $oOrderRow = new OrderRow;
                    $oOrderRow->product_id = $request->product_id;
                    $oOrderRow->sheet_id   = $request->sheet_id;
                    $oOrderRow->order_id   = $orderExist->order_id;
                    $oOrderRow->save();

                DB::commit();     
            
            } else { 

                //если не нашли, создаем новый заказ
                DB::beginTransaction();
                
                    $oOrder = new Order;
                    $oOrder->status_id  = 1;
                    $oOrder->customer_id  = $oUser->id;
                    $oOrder->save();

                    $oOrderRow = new OrderRow;
                    $oOrderRow->product_id = $request->product_id;
                    $oOrderRow->sheet_id   = $request->sheet_id;
                    $oOrderRow->order_id  = $oOrder->id;
                    $oOrderRow->save();
                
                DB::commit();
            }
        } catch (Exception $e) {
            
            DB::rollBack();

            $request->session()->flash('notify', [
                                            'type' =>'error', 
                                            'title' => 'Ошибка', 
                                            'text'   =>'Не удалось выбрать товар!'
            ]);
        }
        $request->session()->flash('notify', [
                                            'type' =>'success', 
                                            'title' => 'Успешно',
                                            'text'   =>'Данные успешно сохранены!']);
        return redirect()->back();
    }

    public function order(Request $request)
    {
        $data  = [];
        $panel['title'] = 'Объекты';
        $panel['back_url'] = 'objects';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        $status = Status::all()->toArray();
        $data['entity']     = Object::find($request->id);
        $data['collection'] = Sheet::getFree()->where('object_id', $request->id)->orderBy('sheet.created_at', 'DESC')->paginate(5);    
        $data['product']    = Product::getList()->orderBy('product.created_at', 'DESC')->paginate(10);    
        
        $data['orders'] = [];
        foreach ($status as $value) {
            
            $stores = OrderRow::getStoreOrderByStatusID($oUser->id, $value['id'])->orderBy('order_row.created_at', 'DESC')->get();
            
            if(!empty($stores)) {
                
                $orders = [];
                foreach ($stores as $store) {

                    $orders[] = [
                        'store'     => $store->name,
                        'order_id'  => $store->order_id,
                        'result'    => OrderRow::getByStoreID($oUser->id, $store->id)->orderBy('order_row.created_at', 'DESC')->get()
                    ];
                }
            
                $data['orders'][] = [
                    
                    'order_id'    => $store->order_id,
                    'status_id'   => $value['id'],
                    'status_name' => $value['name'],
                    'comment'     => $store->comment,
                    'stores'      => $orders
                ];
            }
        } 
        //dd($data['orders']);
        return View::make('orders/order', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }

    /**
     * Запрос поставщику возможности поставки заказа
     *
     * @author Dmitriy Shkatov
     */
    public function requestOrder(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                                    'order_id' => 'required|integer',
        ]);

        if ($oValidator->fails()) {
            $request->session()->flash('notify', [
                                                'type' =>'error', 
                                                'title' => 'Ошибка', 
                                                'text'   =>'Не удалось отправить запрос поставщику!'
            ]);
            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $oModel = Order::where('id', $request->order_id)->first();
        $oModel->status_id = 2;
        $oModel->save();

        $request->session()->flash('notify', [
                                            'type' =>'success', 
                                            'title' => 'Успешно',
                                            'text'   =>'Запрос отправлен поставщику!']);
        return redirect()->back();
    }

    /**
     * Отклонить заказ
     *
     * @author Dmitriy Shkatov
     */
    public function refuseOrder(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                                    'comment'  => 'required',
                                                    'order_id' => 'required|integer',
        ]);

        if ($oValidator->fails()) {
            $request->session()->flash('notify', [
                                                'type' =>'error', 
                                                'title' => 'Ошибка', 
                                                'text'   =>'Не удалось отклонить запрос!'
            ]);
            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $oModel = Order::where('id', $request->order_id)->first();
        $oModel->comment = $request->comment;
        $oModel->status_id = 3;
        $oModel->save();

        $request->session()->flash('notify', [
                                            'type' =>'success', 
                                            'title' => 'Успешно',
                                            'text'   =>'Запрос успешно отклонен!']);
        return redirect()->back();
    }

    /**
     * Подтвердить заказ
     *
     * @author Dmitriy Shkatov
     */
    public function confirmOrder(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                                    'order_id' => 'required|integer',
        ]);

        if ($oValidator->fails()) {
            $request->session()->flash('notify', [
                                                'type' =>'error', 
                                                'title' => 'Ошибка', 
                                                'text'   =>'Не удалось подтвердить заказ!'
            ]);
            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $oModel = Order::where('id', $request->order_id)->first();

        $oModel->status_id = 4;
        $oModel->save();

        $request->session()->flash('notify', [
                                            'type' =>'success', 
                                            'title' => 'Успешно',
                                            'text'   =>'Запрос успешно подтвержден!']);
        return redirect()->back();
    }

    /**
     * Заказ товаров исполнителем
     * status_id становится равным 7
     *
     * @author Dmitriy Shkatov
     */
    public function buyOrder(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                                    'order_id'      => 'required|integer',
                                                    'shipping_date' => 'required|max:256',
        ]);

        if ($oValidator->fails()) {
            $request->session()->flash('notify', [
                                                'type' =>'error', 
                                                'title' => 'Ошибка', 
                                                'text'   =>'Не удалось заказть!'
            ]);
            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $oModel = Order::where('id', $request->order_id)->first();

        $oModel->status_id = 7;
        $oModel->shipping_date = $request->shipping_date;
        $oModel->save();

        $request->session()->flash('notify', [
                                            'type' =>'success', 
                                            'title' => 'Успешно',
                                            'text'   =>'Заказ успешно оформлен!']);
        return redirect()->back();
    }

    /**
     * Отгрузка товара исполнителем
     * status_id становится равным 8
     *
     * @author Dmitriy Shkatov
     */
    public function shipping(Request $request)
    {  // dd($request->all());
        $oValidator = Validator::make($request->all(), [
                                                    'order_id' => 'required|integer',
                                                    'waybill'  => 'required|max:20000000|mimes:jpeg,jpg,JPG,png,bmp',
        ]);

        if ($oValidator->fails()) {
            $request->session()->flash('notify', [
                                                'type' =>'error', 
                                                'title' => 'Ошибка', 
                                                'text'   =>'Не удалось отгрузить!'
            ]);
            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }


        $waybill = FileEntryService::add($request->file('waybill'));

        $oModel = Order::where('id', $request->order_id)->first();
        $oModel->status_id = 8;
        $oModel->waybill = $waybill->id;
        $oModel->actual_date_shipment = date('Y-m-d G:i:s');
        $oModel->save();

        $request->session()->flash('notify', [
                                            'type' =>'success', 
                                            'title' => 'Успешно',
                                            'text'   =>'Заказ успешно отгружен!']);
        return redirect()->back();
    }

    /**
     * Список заказов координатору
     *
     * @author Dmitriy Shkatov
     */
    public function forCoordinator()
    {   
        $data = [];
        $panel['title'] = 'Заказы';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        $data['permission'] = Helpers::getPermission('Order', $oUser->getRoleCurrUser());
        $data['collection'] = Order::getOrderForCoordinator()->orderBy('order.created_at', 'DESC')->paginate(10);

        return View::make('orders/list', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }
}
