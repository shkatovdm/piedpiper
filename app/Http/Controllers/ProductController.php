<?php

namespace App\Http\Controllers;

use Auth;
use File;
use Storage;
use Helpers;
use App\Product;
use App\Store;
use View;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class ProductController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'getLogout']);
        $this->middleware('permission');
    }

   
    public function getProductsListForAJAX(Request $request)
    {   
        $data['collection'] = Product::getByCode($request->code)->orderBy('product.created_at', 'DESC')->paginate(10); 
        $data['sheet_id']   = $request->sheet;
        $data['stores']     = Store::getStoreByCode($request->code)->get();
        //$data['collection']->setPath(url('/products/listAJAX'));

        return View::make('products/listAJAX', $data);
    }
}
