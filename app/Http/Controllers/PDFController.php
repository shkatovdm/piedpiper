<?php

namespace App\Http\Controllers;

use Auth;
use Helpers;
use App\User;
use View;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins,
    Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers,
    PDF;

class PDFController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function waybill(Request $request)
    {   
        $data = [
            
            'order_id' => $request->order_id
        ];
        $pdf = PDF::make();
        $html = View::make('pdf/waybill', $data)->render();
        $pdf->addPage($html);
        $pdf->send();

    }
    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function reportShipments(Request $request)
    {   
        $data = [
            
            'report_id' => $request->report_id
        ];
        $pdf = PDF::make();
        $html = View::make('pdf/report_shipments', $data)->render();
        $pdf->addPage($html);
        $pdf->send('Отчет по продажам ' . date('Y-m-d') . '.pdf');

    }
}
