<?php

namespace App\Http\Controllers;

use Auth;
use Helpers;
//use Pied\Helpers;
use App\User;
use App\Org;
use View;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class OrgController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'getLogout']);

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function index()
    {	
        $data = [];
        $panel['title'] = 'Поставщики';
    	$panel['add_url'] = 'providers/add';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        $data['providers'] = Org::where('type', 'provider')->orderBy('created_at', 'DESC')->paginate(10);

        return View::make('provider/list', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }

    public function addProvider()
    {   
        $data = [];
        $panel['title'] = 'Поставщики';
        $panel['back_url'] = 'providers';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());        

        return View::make('org/add', $data)
                                        ->nest('panel', 'sections.panel', $panel)
                                        ->nest('main_menu', $sSidebar);
    }
    

    public function process(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                                    'name'                  => 'required|max:255',
                                                    'organization'          => 'required|max:255',
                                                    'phone'                 => 'required|unique:provider|max:255',
                                                    'inn'                   => 'required|unique:provider|max:255',
                                                    'ppc'                   => 'required|unique:provider|max:255',
                                                    'bin'                   => 'required|unique:provider|max:255',
                                                    'head'                  => 'required|max:255',
                                                    'chief_accountant'      => 'required|max:255',
                                                    'checking_account'      => 'required|unique:provider|max:255',
                                                    'bank'                  => 'required|max:255',
                                                    'bic_bank'              => 'required|unique:provider|max:255',
                                                    'inn_bank'              => 'required|unique:provider|max:255',
                                                    'correspondent_account' => 'required|unique:provider|max:255',
        ]);

        if ($oValidator->fails()) {

            return redirect('providers/add')
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $oProvider = new Org;
        $oProvider->name                    = $request->organization_name;
        $oProvider->address                 = $request->organization_address;
        $oProvider->phone                   = $request->phone;
        $oProvider->inn                     = $request->inn;
        $oProvider->ppc                     = $request->ppc;
        $oProvider->bin                     = $request->bin;
        $oProvider->head                    = $request->head;
        $oProvider->chief_accountant        = $request->chief_accountant;
        $oProvider->checking_account        = $request->checking_account;
        $oProvider->bank                    = $request->bank;
        $oProvider->bic_bank                = $request->bic_bank;
        $oProvider->inn_bank                = $request->inn_bank;
        $oProvider->correspondent_account   = $request->correspondent_account;
        $oProvider->save();

        $request->session()->flash('notify', ['type' =>'Success', 'text'   =>'Данные успешно сохранены!']);
        return redirect('providers');
    }

    public function edit(Request $request)
    {   
        $data  = [];
        $panel['title'] = 'Поставщики';
        $panel['back_url'] = 'providers';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        $data['provider'] = Provider::find($request->id);

        return View::make('provider/edit', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }
    
    public function update(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                                    'organization_name'     => 'required|max:255',
                                                    'organization_address'  => 'required|max:255',
                                                    'phone'                 => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'inn'                   => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'ppc'                   => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'bin'                   => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'head'                  => 'required|max:255',
                                                    'chief_accountant'      => 'required|max:255',
                                                    'checking_account'      => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'bank'                  => 'required|max:255',
                                                    'bic_bank'              => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'inn_bank'              => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'correspondent_account' => 'required|max:255|unique:provider,id,' . $request->id,
        ]);

        if ($oValidator->fails()) {
            return redirect('providers/edit/' . $request->id)
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $oProvider = Provider::find($request->id);
        $oProvider->organization_name       = $request->organization_name;
        $oProvider->organization_address    = $request->organization_address;
        $oProvider->phone                   = $request->phone;
        $oProvider->inn                     = $request->inn;
        $oProvider->ppc                     = $request->ppc;
        $oProvider->bin                     = $request->bin;
        $oProvider->head                    = $request->head;
        $oProvider->chief_accountant        = $request->chief_accountant;
        $oProvider->checking_account        = $request->checking_account;
        $oProvider->bank                    = $request->bank;
        $oProvider->bic_bank                = $request->bic_bank;
        $oProvider->inn_bank                = $request->inn_bank;
        $oProvider->correspondent_account   = $request->correspondent_account;
        $oProvider->save();
        $request->session()->flash('notify', ['type' =>'Success', 'text'   =>'Данные успешно обновлены!']);

        return redirect('providers/edit/' . $request->id);
    }    

    public function remove(Request $request)
    {   
        $oUser = Auth::user();
        
        if($oUser->getRoleCurrUser() === 'admin') 
        {    
            $oprovider = Provider::find($request->id);
            $oprovider->delete();
            $request->session()->flash('notify', ['type' =>'Success', 'text'   =>'Данные успешно удалены!']);
            return redirect('providers');
        
        } else {

            abort(403, 'Unauthorized!');
        }
    }
}
