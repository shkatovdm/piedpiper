<?php

namespace App\Http\Controllers;

use DB;
use Auth;
use Helpers;
use App\User;
use App\Order;
use App\Sheet;
use App\Object;
use App\Product;
use App\OrderRow;
use App\Status;
use App\CoordinationRow;
use View;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class MatchingController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'getLogout']);
        $this->middleware('Matching');
    }

    /**
     * Список заказов координатору
     *
     * @author Dmitriy Shkatov
     */
    public function index()
    {   
        $data = [];
        $panel['title'] = 'Заказы';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        $data['permission'] = Helpers::getPermission('Order', $oUser->getRoleCurrUser());
        $data['collection'] = Order::getOrderForCoordinator()->orderBy('order.created_at', 'DESC')->paginate(10);

        return View::make('matching/list', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }

    /**
     * Страница редактирования заказа согласующим пользователем
     *
     * @author Dmitriy Shkatov
     */
    public function matchingOrder(Request $request)
    {   
        $data = [];
        $panel['title'] = 'Заказ №' . $request->id;
        $panel['back_url'] = 'orders';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        //$data['permission'] = Helpers::getPermission('Order', $oUser->getRoleCurrUser());
        $data['order_id']   = $request->id;
        $data['store']      = OrderRow::getStoreByOrderID($request->id)->first();
        $data['collection'] = OrderRow::getByID($request->id)->orderBy('order_row.created_at', 'DESC')->get();

        return View::make('matching/edit', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }


    /**
     * Подтвердить заказ
     *
     * @author Dmitriy Shkatov
     */
    public function confirmOrder(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                                    'order_id' => 'required|integer',
        ]);

        if ($oValidator->fails()) {
            $request->session()->flash('notify', [
                                                'type' =>'error', 
                                                'title' => 'Ошибка', 
                                                'text'   =>'Не удалось подтвердить заказ!'
            ]);
            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $oUser = Auth::user();

        $approvs = CoordinationRow::where('order_id', $request->order_id)
                                    ->where('user_id', $oUser->id)
                                    ->where('role_id', $oUser->role_id)
                                    ->count();
        

        if($approvs)
        {
            $request->session()->flash('notify', [
                                        'type' =>'error', 
                                        'title' => 'Ошибка', 
                                        'text'   => 'Заказ уже подтвержден!'
            ]);
            return redirect()->back();
        }
        
        $CoordinationRow = new CoordinationRow();
        $CoordinationRow->approved = 1;
        $CoordinationRow->order_id = $request->order_id;
        $CoordinationRow->user_id = $oUser->id;
        $CoordinationRow->role_id = $oUser->role_id;
        $CoordinationRow->save();
        
        Helpers::isApproveOrder($request->order_id);

        $oModel = Order::where('id', $request->order_id)->first();
        $oModel->status_id = 6;
        $oModel->save();

        $request->session()->flash('notify', [
                                            'type' =>'success', 
                                            'title' => 'Успешно',
                                            'text'   =>'Запрос успешно подтвержден!']);
        return redirect()->back();
    }

    /**
     * Отклонить заказ
     *
     * @author Dmitriy Shkatov
     */
    public function refuseOrder(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                                    'comment'  => 'required',
                                                    'order_id' => 'required|integer',
        ]);

        if ($oValidator->fails()) {
            $request->session()->flash('notify', [
                                                'type' =>'error', 
                                                'title' => 'Ошибка', 
                                                'text'   =>'Не удалось отклонить запрос!'
            ]);
            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $oModel = Order::where('id', $request->order_id)->first();
        $oModel->comment = $request->comment;
        $oModel->status_id = 5;
        $oModel->save();

        $request->session()->flash('notify', [
                                            'type' =>'success', 
                                            'title' => 'Успешно',
                                            'text'   =>'Запрос успешно отклонен!']);
        return redirect()->back();
    }
}
