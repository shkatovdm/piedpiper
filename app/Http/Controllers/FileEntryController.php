<?php 
namespace App\Http\Controllers;
 
use App\Http\Controllers\Controller;
use App\Fileentry;
 
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Http\Response;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
 
class FileEntryController extends Controller {

    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

	public function get($id, $type){
		
		$entry  = Fileentry::where('id', $id)->firstOrFail();
		$file   = Storage::disk('local')->get($entry->filename);
        $img = $file;

        if($entry->mime != 'application/pdf') {

            $img = \Image::cache(function ($image) use ($file, $type) {

                switch ($type) {

                    case 'admin-preview':
                        $image->make($file)->resize(188, 150, function ($constraint) {
                            $constraint->aspectRatio();
                        })->resizeCanvas(188, 150, 'center', false, 'f6f7f7');
                        break;

                    case 'origin':
                    default:
                        $image->make($file);
                        break;
                }
            }, 60);// ttl â ìèíóòàõ
        }
 		return (new Response($img, 200))
              ->header('Content-Type', $entry->mime);
	}
}