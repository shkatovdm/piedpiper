<?php

namespace App\Http\Controllers;

use Auth;
use Helpers;
use App\User;
use App\Org;
use Services\UserService;
use View;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;

class ExecutorController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'getLogout']);

    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function index()
    {	
        $data = [];
        $panel['title'] = 'Исполнители';
    	$panel['add_url'] = 'executors/add';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        $data['executor'] = Org::where('type', 'executor')->orderBy('created_at', 'DESC')->paginate(10);

        return View::make('executor/list', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }

    public function add()
    {   
        $data = [];
        $panel['title'] = 'Исполнители';
        $panel['back_url'] = 'executors';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());        

        return View::make('executor/add', $data)
                                        ->nest('panel', 'sections.panel', $panel)
                                        ->nest('main_menu', $sSidebar);
    }
    

    public function process(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                                    'name'                  => 'required|max:255',
                                                    'address'               => 'required|max:255',
                                                    'phone'                 => 'required|unique:provider|max:255',
                                                    'inn'                   => 'required|unique:provider|max:255',
                                                    'ppc'                   => 'required|unique:provider|max:255',
                                                    'bin'                   => 'required|unique:provider|max:255',
                                                    'head'                  => 'required|max:255',
                                                    'chief_accountant'      => 'required|max:255',
                                                    'checking_account'      => 'required|unique:provider|max:255',
                                                    'bank'                  => 'required|max:255',
                                                    'bic_bank'              => 'required|unique:provider|max:255',
                                                    'inn_bank'              => 'required|unique:provider|max:255',
                                                    'correspondent_account' => 'required|unique:provider|max:255',
        ]);

        if ($oValidator->fails()) {

            return redirect('executors/add')
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $oProvider = new Org;
        $oProvider->name                    = $request->name;
        $oProvider->address                 = $request->address;
        $oProvider->phone                   = $request->phone;
        $oProvider->inn                     = $request->inn;
        $oProvider->ppc                     = $request->ppc;
        $oProvider->bin                     = $request->bin;
        $oProvider->head                    = $request->head;
        $oProvider->chief_accountant        = $request->chief_accountant;
        $oProvider->checking_account        = $request->checking_account;
        $oProvider->bank                    = $request->bank;
        $oProvider->bic_bank                = $request->bic_bank;
        $oProvider->inn_bank                = $request->inn_bank;
        $oProvider->correspondent_account   = $request->correspondent_account;
        $oProvider->type                    = 'executor';
        
        $oProvider->save();

        Helpers::showSuccess($request);
        return redirect('executors');
    }

    public function addUserAjax(Request $request)
    {   
        $data['roles']      = UserService::getRolesList();
        $data['request']    = $request->all();

        $oValidator = Validator::make($request->all(), [
                                                    'name'                  => 'required|max:255|unique:users',
                                                    'email'                 => 'required|email|max:255|unique:users',
                                                    'password'              => 'required|min:6|max:255|confirmed',
                                                    'password_confirmation' => 'required|min:6|max:255',
                                                    'role_id'               => 'required|integer|max:255',
                                                    'org_id'                => 'required|integer|max:255',
        ]);
        
        $oUser = Auth::user();
        if (Helpers::getRoleByID($request->role_id) == 'admin' && $oUser->getRoleCurrUser() != 'admin'){
            
            abort(403, 'Forbidden!');
        }
        
        if ($oValidator->fails()) {

            return View::make('executor/addUser', $data)
                            ->withErrors($oValidator);
        }

        $user = new User;
        $user->name        = $request->name;
        $user->email       = $request->email;
        $user->password    = bcrypt($request->password);
        $user->role_id     = $request->role_id;
        $user->org_id      = $request->org_id;
        $user->save();

        Helpers::showSuccess($request);

        return 'Готово';
    }

    public function edit(Request $request)
    {   
        $data  = [];
        $panel['title'] = 'Исполнители';
        $panel['back_url'] = 'executors';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        $data['users']      = User::where('org_id', $request->id)->paginate(10);
        $data['entity']     = Org::find($request->id);
        $data['freeUser']   = User::where('org_id', 1)->where('role_id', 4)->paginate(10);
        $data['freeUser']->setPath(url('/user/listAJAX'));

        return View::make('executor/edit', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }
    
    public function update(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                                    'name'                  => 'required|max:255',
                                                    'address'               => 'required|max:255',
                                                    'phone'                 => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'inn'                   => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'ppc'                   => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'bin'                   => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'head'                  => 'required|max:255',
                                                    'chief_accountant'      => 'required|max:255',
                                                    'checking_account'      => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'bank'                  => 'required|max:255',
                                                    'bic_bank'              => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'inn_bank'              => 'required|max:255|unique:provider,id,' . $request->id,
                                                    'correspondent_account' => 'required|max:255|unique:provider,id,' . $request->id,
        ]);

        if ($oValidator->fails()) {
            return redirect('executors/edit/' . $request->id)
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $Org = Org::find($request->id);
        $Org->name                    = $request->name;
        $Org->address                 = $request->address;
        $Org->phone                   = $request->phone;
        $Org->inn                     = $request->inn;
        $Org->ppc                     = $request->ppc;
        $Org->bin                     = $request->bin;
        $Org->head                    = $request->head;
        $Org->chief_accountant        = $request->chief_accountant;
        $Org->checking_account        = $request->checking_account;
        $Org->bank                    = $request->bank;
        $Org->bic_bank                = $request->bic_bank;
        $Org->inn_bank                = $request->inn_bank;
        $Org->correspondent_account   = $request->correspondent_account;
        $Org->save();
        
        Helpers::showSuccess($request);

        return redirect('executors/edit/' . $request->id);
    }    

    public function joinUser(Request $request)
    {       
        $oValidator = Validator::make($request->all(), [
                                                    'user_id' => 'required|integer|max:255',
                                                    'org_id'  => 'required|integer|max:255',
        ]);

        if ($oValidator->fails()) {
            $request->session()->flash('notify', [
                                                'type' =>'error', 
                                                'title' => 'Ошибка', 
                                                'text'   =>'Не удалось добавить объект!'
            ]);
            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $user = User::find($request->user_id);;
        $user->org_id      = $request->org_id;
        $user->save();
        
        Helpers::showSuccess($request);
        return redirect()->back();
    }

    public function unjoinUser(Request $request)
    {   
        $oValidator = Validator::make($request->all(), [
                                                    'user_id' => 'required|integer|max:255'
        ]);

        if ($oValidator->fails()) {
            $request->session()->flash('notify', [
                                                'type' =>'error', 
                                                'title' => 'Ошибка', 
                                                'text'   =>'Не удалось добавить объект!'
            ]);
            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }
        $user = User::find($request->user_id);
        $user->org_id      = 1;
        $user->save();
        
        Helpers::showSuccess($request);
        return redirect()->back();
    }

    public function remove(Request $request)
    {   
        $oUser = Auth::user();
        
        if($oUser->getRoleCurrUser() === 'admin') 
        {    
            $oexecutor = executor::find($request->id);
            $oexecutor->delete();
            Helpers::showSuccess($request);
            return redirect('executors');
        
        } else {

            abort(403, 'Unauthorized!');
        }
    }
}
