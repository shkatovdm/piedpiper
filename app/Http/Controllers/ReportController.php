<?php

namespace App\Http\Controllers;

use Auth;
use File;
use Storage;
use Helpers;
use View;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers,
    App\ReportShipments,
    Services\FileEntryService;

class ReportController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Registration & Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users, as well as the
    | authentication of existing users. By default, this controller uses
    | a simple trait to add these behaviors. Why don't you explore it?
    |
    */

    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * Create a new authentication controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => 'getLogout']);
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    public function reportShipments()
    {   
        $data = [];
        $panel['title'] = 'Отчеты о продажах ';

        $oUser = Auth::user();
        $sSidebar = Helpers::getSidebarByRole($oUser->getRoleCurrUser());
        
        $data['collection'] = ReportShipments::orderBy('created_at', 'DESC')->paginate(10);        

        return View::make('reports/list', $data)
                                ->nest('panel', 'sections.panel', $panel)
                                ->nest('main_menu', $sSidebar);
    }

    /**
     *
     * @author Dmitriy Shkatov
     */
    public function addConfirmedReport(Request $request)
    {  // dd($request->all());
        $oValidator = Validator::make($request->all(), [
                                                    'report_id' => 'required|integer',
                                                    'file'     => 'required|max:20000000|mimes:jpeg,jpg,JPG,png,bmp,pdf',
        ]);

        if ($oValidator->fails()) {
            $request->session()->flash('notify', [
                                                'type' =>'error', 
                                                'title' => 'Ошибка', 
                                                'text'   =>'Не удалось прикрепить!'
            ]);
            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }


        $file = FileEntryService::add($request->file('file'));

        $oModel = ReportShipments::where('id', $request->report_id)->first();
        $oModel->confirmed_report = $file->id;
        $oModel->save();

        $request->session()->flash('notify', [
                                            'type' =>'success', 
                                            'title' => 'Успешно',
                                            'text'   =>'Отчет успешно обновлен!']);
        return redirect()->back();
    }

    /**
     *
     * @author Dmitriy Shkatov
     */
    public function addPaymentOrder(Request $request)
    {  // dd($request->all());
        $oValidator = Validator::make($request->all(), [
                                                    'report_id' => 'required|integer',
                                                    'file'     => 'required|max:20000000|mimes:jpeg,jpg,JPG,png,bmp,pdf',
        ]);

        if ($oValidator->fails()) {
            $request->session()->flash('notify', [
                                                'type' =>'error', 
                                                'title' => 'Ошибка', 
                                                'text'   =>'Не удалось прикрепить!'
            ]);
            return redirect()
                        ->back()
                        ->withErrors($oValidator)
                        ->withInput();
        }


        $file = FileEntryService::add($request->file('file'));

        $oModel = ReportShipments::where('id', $request->report_id)->first();
        $oModel->payment_order = $file->id;
        $oModel->save();

        $request->session()->flash('notify', [
                                            'type' =>'success', 
                                            'title' => 'Успешно',
                                            'text'   =>'Отчет успешно обновлен!']);
        return redirect()->back();
    }
}
