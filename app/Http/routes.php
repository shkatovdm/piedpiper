<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

// Authentication routes...
Route::get('auth/login', 'Auth\AuthController@getLogin');
Route::post('auth/login', 'Auth\AuthController@postLogin');
Route::get('auth/logout', 'Auth\AuthController@getLogout');

// Registration routes...
//Route::get('auth/register', 'Auth\AuthController@getRegister');
//Route::post('auth/register', 'Auth\AuthController@postRegister');

// Password reset link request routes...
Route::get('password/email', 'Auth\PasswordController@getEmail');
Route::post('password/email', 'Auth\PasswordController@postEmail');

// Password reset routes...
Route::get('password/reset/{token}', 'Auth\PasswordController@getReset');
Route::post('password/reset', 'Auth\PasswordController@postReset');

Route::get('/', 'DashboardController@index');
Route::get('/profile', 'UserController@profile');
Route::get('/profile/{id}', 'UserController@profile');

Route::group(['middleware' => 'isAdmin'], function () {
	
	Route::get('/city', 'CityController@index');
	Route::get('/city/add', 'CityController@add');
	Route::post('/city/add', 'CityController@process');
	Route::get('/city/{id}', 'CityController@detail');
	Route::post('/city/{id}', 'CityController@update');
	Route::post('/city/remove/{id}', 'CityController@remove');

	Route::get('/users', 'UserController@index');
	Route::get('/users/add', 'UserController@add');
	Route::get('/users/executors', 'UserController@getExecutors');
	Route::post('/users/add', 'UserController@process');
	Route::get('/users/edit/{id}', 'UserController@edit');
	Route::post('/users/edit/{id}', 'UserController@update');
	Route::post('/users/remove/{id}', 'UserController@remove');
	
	Route::get('/providers', 'ProviderController@index');
	Route::get('/providers/add', 'ProviderController@add');
	Route::post('/providers/add', 'ProviderController@process');
	Route::get('/providers/edit/{id}', 'ProviderController@edit');
	Route::post('/providers/edit/{id}', 'ProviderController@update');
	Route::post('/providers/remove/{id}', 'ProviderController@remove');

	Route::get('/objects/add', 'ObjectController@add');
	Route::post('/objects/add', 'ObjectController@process');
	Route::get('/objects/edit/{id}', 'ObjectController@edit');
	Route::post('/objects/edit/{id}', 'ObjectController@update');
	Route::get('/objects/listAJAX', 'ObjectController@getListAJAX');
	Route::get('/objects/object_lot/{id}', 'ObjectController@getObjectLotAJAX');
});

Route::get('/objects', 'ObjectController@index');

Route::group(['middleware' => 'isOperator'], function () {

	Route::get('/objects/listAJAX', 'ObjectController@getListAJAX');
	Route::get('/objects/object_lot/{id}', 'ObjectController@getObjectLotAJAX');

	Route::get('/lots', 'LotController@index');
	Route::get('/lots/add', 'LotController@add');
	Route::post('/lots/add', 'LotController@process');
	Route::get('/lots/edit/{id}', 'LotController@edit');
	Route::post('/lots/edit/{id}', 'LotController@update');
	Route::post('/lots/add_object/{id}', 'LotController@addObject');
	Route::post('/lots/remove_object/{id}', 'LotController@removeObject');
	Route::post('/lots/addAJAX', 'LotController@addObjectAJAX');
	Route::post('/lots/remove/{id}', 'LotController@remove');

	Route::get('/resources', 'ResourceController@index');

	Route::get('/executors', 'ExecutorController@index');
	Route::get('/executors/add', 'ExecutorController@add');
	Route::post('/executors/add', 'ExecutorController@process');
	Route::get('/executors/edit/{id}', 'ExecutorController@edit');
	Route::post('/executors/edit/{id}', 'ExecutorController@update');
	Route::post('/executors/remove/{id}', 'ExecutorController@remove');
	Route::post('/executors/addUser', 'ExecutorController@addUserAjax');
	Route::post('/executors/joinUser', 'ExecutorController@joinUser');
	Route::post('/executors/unjoinUser', 'ExecutorController@unjoinUser');

	Route::get('/providers', 'ProviderController@index');
	Route::get('/providers/add', 'ProviderController@add');
	Route::post('/providers/add', 'ProviderController@process');
	Route::get('/providers/edit/{id}', 'ProviderController@edit');
	Route::post('/providers/edit/{id}', 'ProviderController@update');
	Route::post('/providers/remove/{id}', 'ProviderController@remove');
	Route::post('/executors/addUser', 'ExecutorController@addUserAjax');
	Route::post('/executors/joinUser', 'ExecutorController@joinUser');
	Route::post('/executors/unjoinUser', 'ExecutorController@unjoinUser');
	
	Route::get('/sheet', 'SheetController@index');
});

Route::group(['middleware' => 'isExecutor'], function () {

	Route::get('/objects/order/{id}', 'OrderController@order');
	Route::get('/products/listAJAX', 'ProductController@getProductsListForAJAX');
	Route::post('/orders/add/', 'OrderController@processAdd');
	Route::post('/orders/request', 'OrderController@requestOrder');
	Route::post('/orders/buy', 'OrderController@buyOrder');

});

Route::group(['middleware' => 'isProvider'], function () {

	Route::get('/orders', 'OrderController@index');
	Route::get('/approved', 'OrderController@approved');
	Route::get('/shipped', 'OrderController@shipped');
	Route::get('/awaiting-shipment', 'OrderController@awaitingShipment');
	Route::get('/orders/edit/{id}', 'OrderController@providerOrderEdit');
	Route::post('/orders/refuse', 'OrderController@refuseOrder');
	Route::post('/orders/confirm', 'OrderController@confirmOrder');
	Route::post('/orders/shipping', 'OrderController@shipping');
	Route::get('/report-shipments', 'ReportController@reportShipments');
	Route::post('/add-confirmed-report', 'ReportController@addConfirmedReport');
	Route::post('/add-payment-order', 'ReportController@addPaymentOrder');
	Route::post('/print/report-shipments', 'PDFController@reportShipments');
	Route::get('/profile/edit', 'UserController@profile');
	Route::post('/profile/edit/{id}', 'ProviderController@updateProfile');

});

Route::get('/matching', 'MatchingController@index');
Route::get('/matching/order/{id}', 'MatchingController@matchingOrder');
Route::post('/matching/confirm', 'MatchingController@confirmOrder');
Route::post('/matching/refuse', 'MatchingController@refuseOrder');
Route::get('/file/{name}', 'FileController@getByName');
Route::post('/print/waybill', 'PDFController@waybill');

Route::get('fileentry/get/{id}/{type}', 'FileEntryController@get');
