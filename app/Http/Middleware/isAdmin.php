<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $oUser = Auth::user();
        if($oUser->getRoleCurrUser() != 'admin' && $oUser->getRoleCurrUser() != 'customer') return abort(403, 'Forbidden!');

        return $next($request);
    }
}
