<?php

namespace App\Http\Middleware;

use Closure;
use Auth;
use Helpers;

class Permission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $oUser = Auth::user();
        $permissions = Helpers::getPermission('Object', $oUser->getRoleCurrUser());
        //dd($permissions);
        //if($oUser->getRoleCurrUser() != 'admin') return abort(403, 'Forbidden!');
        return $next($request);
    }
}
