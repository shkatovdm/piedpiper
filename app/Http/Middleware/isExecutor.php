<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class isExecutor
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::check()){
            $oUser = Auth::user();
            if($oUser->getRoleCurrUser() != 'executor') return abort(403, 'Forbidden!');
            
            return $next($request);

        } else {
            
            return redirect('auth/login');
        }
    }
}
