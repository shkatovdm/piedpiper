<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Store extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'store';

    public static function getStoreByProductID($product_id)
    {   
        return DB::table('order_row')
            ->join('product', 'product.id', '=', 'order_row.product_id')
            ->join('store', 'product.store_id', '=', 'store.id')
            ->select(
                'store.id', 
                'store.name'
            );
            //->where('order_row.product_id', $product_id);
    }

    public static function getStoreByCode($code)
    {   
        return DB::table('product')
            ->join('store', 'product.store_id', '=', 'store.id')
            ->select(
                'store.id',
                'store.name'
            )
            ->where('product.resource_code', $code);
    }
}
