<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Order extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order';
    
    /**
     * Заказы поставщику
     *
     */
    public static function getOrderByProviderID($provider_id)
    {   
        return DB::table('order')
            
            ->join('order_row', 'order.id',             '=', 'order_row.order_id')
            ->join('product',   'order_row.product_id', '=', 'product.id')
            ->join('store',     'product.store_id',     '=', 'store.id')
            ->join('users',     'order.customer_id',    '=', 'users.id')
            ->join('status',    'order.status_id',      '=', 'status.id')
            ->join('sheet',     'order_row.sheet_id',   '=', 'sheet.id')
            ->select(
                'order.id',
                'status.name as status',
                'users.name as client',
                'store.street as store_street',
                'store.house_number as store_house_number',
                'store.housing as store_housing',
                'store.building as store_building',
                DB::raw('SUM(product.price * sheet.quantity) as total')
            )
            ->groupBy('order.id')
            ->distinct()
            ->where('order.status_id', 2)
            ->where('store.provider_id', $provider_id);
    }

    /**
     * Подтвержденные поставщиком
     *
     */
    public static function getOrderProviderByStatus($provider_id, $order_id)
    {   
        return DB::table('order')
            
            ->join('order_row', 'order.id',             '=', 'order_row.order_id')
            ->join('product',   'order_row.product_id', '=', 'product.id')
            ->join('store',     'product.store_id',     '=', 'store.id')
            ->join('users',     'order.customer_id',    '=', 'users.id')
            ->join('status',    'order.status_id',      '=', 'status.id')
            ->join('sheet',     'order_row.sheet_id',   '=', 'sheet.id')
            ->select(
                'order.id',
                'status.name as status',
                'users.name as client',
                'store.street as store_street',
                'store.house_number as store_house_number',
                'store.housing as store_housing',
                'store.building as store_building',
                DB::raw('SUM(product.price * sheet.quantity) as total')
            )
            ->groupBy('order.id')
            ->distinct()
            ->where('order.status_id', $order_id)
            ->where('store.provider_id', $provider_id);
    }

    /**
     * Заказы согласующему
     *
     */
    public static function getOrderForCoordinator()
    {   
        return DB::table('order')
            
            ->join('order_row', 'order.id',             '=', 'order_row.order_id')
            ->join('product',   'order_row.product_id', '=', 'product.id')
            ->join('store',     'product.store_id',     '=', 'store.id')
            ->join('users',     'order.customer_id',    '=', 'users.id')
            ->join('status',    'order.status_id',      '=', 'status.id')
            ->join('sheet',     'order_row.sheet_id',   '=', 'sheet.id')
            ->select(
                'order.id',
                'status.name as status',
                'users.name as client',
                'store.street as store_street',
                'store.house_number as store_house_number',
                'store.housing as store_housing',
                'store.building as store_building',
                DB::raw('SUM(product.price * sheet.quantity) as total')
            )
            ->groupBy('order.id')
            ->distinct()
            ->where('order.status_id', 4);
    }

    /**
     * Заказы поставщику
     *
     */
    public static function getOrderForReport($start)
    {   
        return DB::table('order')
            
            ->join('order_row', 'order.id',             '=', 'order_row.order_id')
            ->join('product',   'order_row.product_id', '=', 'product.id')
            ->join('store',     'product.store_id',     '=', 'store.id')
            ->join('users',     'order.customer_id',    '=', 'users.id')
            ->join('status',    'order.status_id',      '=', 'status.id')
            ->join('sheet',     'order_row.sheet_id',   '=', 'sheet.id')
            ->select(
                
                'order.id',
                'order.customer_id as customer_id',
                'status.name as status',
                'users.name as client',
                'store.street as store_street',
                'store.house_number as store_house_number',
                'store.housing as store_housing',
                'store.building as store_building',
                DB::raw('SUM(product.price * sheet.quantity) as total')
            )
            ->groupBy('order.id')
            ->distinct()
            ->where('order.status_id', 8)
            ->where('order.actual_date_shipment', '>', $start);
    }


}
