<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Sheet extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'sheet';

    public static function getFree()
    {   
        $exist = [];

        $order = DB::table('order_row')->select('sheet_id')->get();
        foreach ($order as $value) {
            
            $exist[] = $value->sheet_id;
        }
        //Если еще ни один объект не привязан к лоту
        if(empty($exist)){

            return DB::table('sheet')
                ->select(
                    'sheet.id', 
                    'sheet.object_id', 
                    'sheet.code_resource', 
                    'sheet.name_resource', 
                    'sheet.measurement', 
                    'sheet.quantity', 
                    'sheet.base_price'
                );
        }
        
        return DB::table('sheet')
            ->select(
                    'sheet.id', 
                    'sheet.object_id', 
                    'sheet.code_resource', 
                    'sheet.name_resource', 
                    'sheet.measurement', 
                    'sheet.quantity', 
                    'sheet.base_price'
            )
            ->whereNotIn('sheet.id', $exist);
    }
}
