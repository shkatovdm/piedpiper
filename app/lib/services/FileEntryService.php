<?php 
namespace Services;

use App\Fileentry;
use Illuminate\Http\Request;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class FileEntryService
{ 
    public static function add($file)
    {
        $extension = $file->getClientOriginalExtension();
        Storage::disk('local')->put($file->getFilename() . '.' . $extension,  File::get($file));
        $entry = new Fileentry();
        $entry->mime = $file->getClientMimeType();
        $entry->original_filename = $file->getClientOriginalName();
        $entry->filename = $file->getFilename() . '.' . $extension;
 
        $entry->save();

        return $entry->id;
	}

    public static function saveToPublic($file)
    {
        $result = [];

        $extension = $file->getClientOriginalExtension();
        $fileName = $file->getFilename() . '.' . $file->getClientOriginalExtension();

        $previws = ['small', 'admin-list', 'news', 'main-news', 'last-news-right', 'admin-preview', 'origin'];

        foreach ($previws as $type) {
            
            switch ($type) {
                case 'small':
                    
                    $path = public_path("upload/small/" . $fileName, $file->getClientOriginalName());
                    \Image::make($file)->fit(108)->save($path);
                    break;

                case 'admin-list':

                    $path = public_path("upload/admin-list/" . $fileName, $file->getClientOriginalName());
                    \Image::make($file)->fit(100)->save($path);
                    break;

                case 'news':
                   
                    $path = public_path("upload/news/" . $fileName, $file->getClientOriginalName());
                    \Image::make($file)->resize(400, 300, function ($constraint) {
                        $constraint->aspectRatio();
                    })->resizeCanvas(390, 290, 'center', false, 'f6f7f7')->save($path);
                break;

                case 'main-news':
                    
                    $path = public_path("upload/main-news/" . $fileName, $file->getClientOriginalName());
                    \Image::make($file)->resize(300, 400, function ($constraint) {
                        $constraint->aspectRatio();
                    })->resizeCanvas(200, 200, 'center', false, 'f6f7f7')->save($path);

                break;

                case 'last-news-right':
                    
                    $path = public_path("upload/last-news-right/" . $fileName, $file->getClientOriginalName());
                    \Image::make($file)->resize(230, 153, function ($constraint) {
                        $constraint->aspectRatio();
                    })->resizeCanvas(230, 153, 'center', false, 'f6f7f7')->save($path);
                break;
                
                case 'top-banner':
                    
                    $path = public_path("upload/top-banner/" . $fileName, $file->getClientOriginalName());
                    \Image::make($file)->resize(500, 120, function ($constraint) {
                        $constraint->aspectRatio();
                    })->resizeCanvas(500, 120, 'center', false, 'f6f7f7')->save($path);
                break;

                case 'right-banner':
                    
                    $path = public_path("upload/right-banner/" . $fileName, $file->getClientOriginalName());
                    \Image::make($file)->resize(400, 300, function ($constraint) {
                        $constraint->aspectRatio();
                    })->resizeCanvas(400, 300, 'center', false, 'f6f7f7')->save($path);
                break;
                
                case 'admin-preview':
                    
                    $path = public_path("upload/admin-preview/" . $fileName, $file->getClientOriginalName());
                    \Image::make($file)->resize(200, 200, function ($constraint) {
                        $constraint->aspectRatio();
                    })->resizeCanvas(200, 200, 'center', false, 'f6f7f7')->save($path);
                    break;

                case 'origin':

                    $path = public_path("upload/origin/" . $fileName, $file->getClientOriginalName());
                    \Image::make($file)->save($path);

                default:

                    $path = public_path("upload/origin/" . $fileName, $file->getClientOriginalName());
                    \Image::make($file)->save($path);
            }
        }
        
        return $fileName;
    }
}