<?php 
namespace Services;

use Illuminate\Http\Request;
use App\Lot;
use App\ObjectLot;

class LotService
{ 
    public static function addObjectToLot($lot_id, $object_id)
    {
        $ObjectLot = new ObjectLot;
        
        $ObjectLot->lot_id = $lot_id;
        $ObjectLot->object_id = $object_id;
        
        $ObjectLot->save();
        
        return $ObjectLot;
    }
}