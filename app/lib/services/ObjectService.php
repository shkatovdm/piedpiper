<?php 
namespace Services;

use Illuminate\Http\Request;
use File;
use Storage;
use App\Object;

class ObjectService
{   
	public static function add(Request $request)
    {
        $oFile = $request->file('userfile');
        $sOriginalFileName = $oFile->getClientOriginalName();
        $sExtension = $oFile->getClientOriginalExtension();
        Storage::disk('local')->put($oFile->getFilename() . '.' . $sExtension,  File::get($oFile)); 
        $sFileName = $oFile->getFilename() . '.' . $sExtension;

        $Object = new Object;
        
        $Object->city_id             = $request->city_id;
        $Object->street              = $request->street;
        $Object->house_number        = $request->house_number;
        $Object->housing             = $request->housing;
        $Object->building            = $request->building;
        $Object->person_charge       = $request->person_charge;
        $Object->phone_person_charge = $request->phone_person_charge;
        $Object->funding             = $request->funding;
        $Object->file_name           = $sFileName;
        $Object->original_file_name  = $sOriginalFileName;
        $Object->file_processed      = 0;

        $Object->save();
        
        return $Object;
    }

    public static function addObjectToLot(Request $request)
    {
        
        
        return $Object;
    }
}