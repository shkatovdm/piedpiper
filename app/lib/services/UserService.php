<?php 
namespace Services;

use Illuminate\Http\Request;
use App\User;
use App\Role;
use Auth;

class UserService
{ 
    public static function getRolesList()
    {
        $oUser = Auth::user();
        
        if ($oUser->getRoleCurrUser() === 'admin'){
            
            return Role::get();
        
        }else{

            return Role::whereNotIn('name', ['admin'])->get();
        }
    }

    public static function add(Request $request)
    {
    	$oUser = new User;
        $oUser->name                    = $request->name;
        $oUser->email                   = $request->email;
        $oUser->password                = bcrypt($request->password);
        $oUser->role_id                 = $request->role_id;
        $oUser->organization_name       = $request->organization_name;
        $oUser->organization_address    = $request->organization_address;
        $oUser->phone                   = $request->phone;
        $oUser->inn                     = $request->inn;
        $oUser->ppc                     = $request->ppc;
        $oUser->bin                     = $request->bin;
        $oUser->head                    = $request->head;
        $oUser->chief_accountant        = $request->chief_accountant;
        $oUser->checking_account        = $request->checking_account;
        $oUser->bank                    = $request->bank;
        $oUser->bic_bank                = $request->bic_bank;
        $oUser->inn_bank                = $request->inn_bank;
        $oUser->correspondent_account   = $request->correspondent_account;
        $oUser->save();

        return $oUser;
	}
}