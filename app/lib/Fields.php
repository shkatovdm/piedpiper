<?php

class Fields
{

    public $errors = array();

    public static function getFieldsFor($collection) {
        return DB::select('
          SELECT f.id,f.name,f.params,fg.group,ft.type,fi.input FROM `fields` as f
          INNER JOIN `fields_group` as fg ON f.group = fg.id
          INNER JOIN `fields_type` as ft ON f.type = ft.id
          INNER JOIN `fields_input` as fi ON f.input = fi.id
          WHERE `collection` = "'.addslashes($collection).'"
          ORDER by f.group ASC, f.sort ASC');
    }

    public static function createField($id,$input = 'text',$param = '',$class = '', $value='') {
        switch($input) {
            case 'checkbox':
                return '<input class="'.$class.'" type="' . $input . '" name="field'. $id .'" id="field'. $id .'" value="1" />';
                break;
            case 'text':
            case 'file':
                return '<input class="'.$class.'" type="' . $input . '" name="field'. $id .'" id="field'. $id .'" value="'.$value.'" />';
                break;
            case 'select':
                $field = '<select class="'.$class.'" name="field'.$id.'" id="field'.$id.'">';
                $params = explode(';',$param);
                foreach ($params as $key => $p) {
                    $field .= '<option value="'.($key+1).'">'.$p.'</option>';
                }
                $field .= '</select>';
                return $field;
                break;
        }
    }

    public function error($field) {
        if (isset($this->errors[$field])) {
            return true;
        }
    }


    public static function check($collection) {

        $query = DB::select('
          SELECT f.id, f.required FROM `fields` as f
          WHERE f.`collection` = "'.addslashes($collection).'" AND f.`required` > 0
          ORDER by f.id ASC');

        $fields = array();

        foreach ($query as $field) {
            $fields['field'.$field->id] = 'required';
        }

        return $fields;


    }

    public static function add($request,$collection) {

        $query = DB::select('
          SELECT f.id, ft.type FROM `fields` as f
          INNER JOIN `fields_type` as ft ON f.type = ft.id
          WHERE f.`collection` = "'.addslashes($collection).'"
          ');

        $fields = array();

        foreach ($query as $field) {
            $fields['field'.$field->id] = $field->type;
        }

        $inputs = $request->input();

        foreach ($inputs as $field => $input) {
            if ($field != '_token') {
                if (!empty($input)) {
                    DB::insert('INSERT INTO `fields_value`(`field`,`' . $fields[$field] . '`) VALUES(' . intval(str_replace('field', '', $field)) . ',"' . addslashes($input) . '")');
                }
            }
        }

        $files = $request->files->all();
    }
}