<?php 
use App\User;
use App\Role;
use App\Model;
//use Auth;
use App\CoordinationRow;
use Illuminate\Http\Request;

class Helpers
{   
    private static $permissions = [
            
            'User' => [
                    'admin'     => ['read', 'create', 'update', 'delete'],
                    'customer'  => ['read', 'create', 'update'],
                    //'entity'    => 'users'
            ],    
            'Object' => [
                    'admin'         => ['read', 'create', 'update', 'delete'],
                    'customer'      => ['read', 'create', 'update'],
                    'provider'      => ['read', 'create', 'update'],
                    'executor'      => ['read', 'create', 'update'],
                    //'entity'        => 'objects'
            ],
            'Order' => [
                    'admin'         => ['read', 'create', 'update', 'delete'],
                    'customer'      => ['read', 'create', 'update'],
                    'provider'      => ['read', 'update'],
                    'coordinator'   => ['read', 'update'],
                    //'entity'    => 'orders'
            ],
    ];

    private static $approvedRoles = [5];

    public static function getSidebarByRole($sRole)
    {
        $sSidebar = 'sections.main_menu';
        
        if($sRole == 'admin')           $sSidebar = 'sections.admin_menu';
        elseif($sRole == 'customer')    $sSidebar = 'sections.customer_menu';
        elseif($sRole == 'executor')    $sSidebar = 'sections.executor_menu';
        elseif($sRole == 'provider')    $sSidebar = 'sections.provider_menu';
        elseif($sRole == 'coordinator') $sSidebar = 'sections.coordinator_menu';
        
        return $sSidebar;
    }
    /**
     * Get a role user.
     *
     * @return string
     */
    public static function getRoleByID($iRoleID)
    {   
        $oRole = Role::find($iRoleID);
        
        return $oRole->name;
    }
    
    public static function getPermission($model, $role)
    {  
        foreach (self::$permissions[$model][$role] as $value) {
            $permissions[$value] = true;
        }
        return $permissions;
    }

    /**
    * @todo Вынести в сервис
    */
    public static function isApproveOrder($order_id){

        $approvs = CoordinationRow::where('order_id', $order_id)
                                    ->where('approved', 1)
                                    ->whereIn('role_id', self::$approvedRoles)
                                    ->count();
        
        return $approvs == count(self::$approvedRoles);
    }

    public static function showSuccess(Request $request){

        $request->session()->flash('notify', [
                                            'type' =>'success', 
                                            'title' => 'Успешно',
                                            'text'   =>'Данные успешно сохранены!'
                                    ]
                            );
        return true;
    }

    public static function getCurrentUserEmail(){

        $oUser = Auth::user();
        return $oUser->email;
    }

    public static function getInputType($field){

        $field = explode(":", $field);

        if(!isset($field[1])) return 'text';
        
        return $field[1];
    }
}