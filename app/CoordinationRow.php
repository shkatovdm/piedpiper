<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CoordinationRow extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'coordination_row';
}
