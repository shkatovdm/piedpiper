<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ReportShipments extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'report_shipments';
}
