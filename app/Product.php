<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Product extends Model
{
	/**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'product';

    public static function getList()
    {   
        return DB::table('product')
            ->join('store', 'product.store_id', '=', 'store.id')
            ->select(
                'product.id', 
                'product.store_id', 
                'product.article', 
                'product.resource_code', 
                'product.name', 
                'product.price', 
                'product.balance', 
                'store.name as store_name'
            );
    }
    
    public static function getByCode($code)
    {   
        return DB::table('product')
            ->join('store', 'product.store_id', '=', 'store.id')
            ->join('city',  'store.city_id', '=', 'city.id')
            ->join('users', 'store.provider_id', '=', 'users.id')
            ->join('org',   'users.org_id', '=', 'org.id')
            ->select(
                'product.id', 
                'product.store_id', 
                'product.article', 
                'product.resource_code', 
                'product.name', 
                'product.price', 
                'product.balance', 
                'store.name as store_name',
                'org.name as org_name',
                'city.name as city_name',
                'users.id as user_id'
            )
            ->where('product.resource_code', $code);
    }
/*
    public static function getStoreByProductID($product_id)
    {   
        return DB::table('order_row')
            ->join('product', 'product.id', '=', 'order_row.product_id')
            ->join('store', 'product.store_id', '=', 'store.id')
            ->select(
                'store.id', 
                'store.name'
            );
            //->where('order_row.product_id', $product_id);
    }*/
}
