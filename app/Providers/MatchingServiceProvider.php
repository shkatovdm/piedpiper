<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Services\MatchingService;

class MatchingServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('Services\MatchingService', function ($app) {
            return new MatchingService();
        });
    }
}
