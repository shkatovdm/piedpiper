<?php

namespace App;

use DB;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Model;

class ObjectLot extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'object_lot';

    public static function getList($lot_id)
    {   

        if(!is_array($lot_id)) {
        
            $aLotIDs[] = $lot_id;
        
        } else {

            $aLotIDs = $lot_id;
        }
        return DB::table('object_lot')
            ->join('object', 'object.id', '=', 'object_lot.object_id')
            ->join('city', 'city.id', '=', 'object.city_id')
            ->join('sheet', 'object_lot.object_id', '=', 'sheet.object_id')
            ->select(
                'object.id', 
                'object.street', 
                'object.house_number', 
                'object.housing', 
                'object.building', 
                'object.person_charge', 
                'object.file_processed', 
                'object.city_id', 
                'object_lot.id as object_lot_id',
                'city.name as city_name',
                DB::raw('count(sheet.id) as total')
            )
            ->distinct()
            ->groupBy('sheet.object_id')
            ->whereIn('lot_id', $aLotIDs);
    }

    public static function getListForLot($lot_id)
    {   

        if(!is_array($lot_id)) {
        
            $aLotIDs[] = $lot_id;
        
        } else {

            $aLotIDs = $lot_id;
        }
        return DB::table('object_lot')
            ->join('object', 'object.id', '=', 'object_lot.object_id')
            ->join('city', 'city.id', '=', 'object.city_id')
            //->join('sheet', 'object_lot.object_id', '=', 'sheet.object_id')
            ->select(
                'object.id', 
                'object.street', 
                'object.house_number', 
                'object.housing', 
                'object.building', 
                'object.person_charge', 
                'object.file_processed', 
                'object.city_id', 
                'object_lot.id as object_lot_id',
                'city.name as city_name'
                //DB::raw('count(sheet.id) as total')
            )
            ->distinct()
            //->groupBy('sheet.object_id')
            ->whereIn('lot_id', $aLotIDs);
    }
}
