<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use DB;
use Monolog\Logger;
use Monolog\Handler\StreamHandler,
    App\Order,
    App\ReportShipments;

class MakeReportShipments extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'report:shipments';

    /**
     * 
     * @var obj
     */
    protected $log;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        // create a log channel
        $this->log = new Logger('name');
        $this->log->pushHandler(new StreamHandler(config('app.sheet_process_log'), Logger::INFO));
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->log->addInfo('----------------- Make report -----------------');
        $start = date('Y-m-d G:i:s', (time()-604800));
        $orders = Order::getOrderForReport($start)->get();
        //dd($orders);
        if($orders){
            try {
                
                DB::beginTransaction();
                foreach ($orders as $order) {
                
                    $report = new ReportShipments();
                    $report->customer_id = $order->customer_id;
                    $report->start       = $start;
                    $report->end         = date('Y-m-d G:i:s');
                    $report->total       = $order->total;
                    $report->save();
                }

                DB::commit();
            
            } catch (Exception $e) {
                
                DB::rollBack();
                $this->log->addError($e->getMessage());
            }
        }
        $this->log->addInfo("End make report");
    }
}
