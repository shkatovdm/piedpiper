<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Object;
use App\Resource;
use DB;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class ImportProducts extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'import:products';

    /**
     * 
     * @var obj
     */
    protected $log;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        // create a log channel
        $this->log = new Logger('name');
        $this->log->pushHandler(new StreamHandler(config('app.import_products_log'), Logger::INFO));
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->log->addInfo('----------------- Start import -----------------');
        
        $sFilePath = public_path('upload/import_product.xml');
        
        if (file_exists($sFilePath)) {                       

            $callStartTime = microtime(true);

            $xml = simplexml_load_file($sFilePath);

            try {
                
                DB::beginTransaction();

	                foreach ($xml->product as $product) {
	                	var_dump($product);
	                }

	                //unlink($sFilePath); 
                
                DB::commit();

            } catch (Exception $e) {
                
                DB::rollBack();
                $this->log->addError($e->getMessage());
            }
            
            $callEndTime = microtime(true);
            $callTime = $callEndTime - $callStartTime;
            
            $this->log->addInfo('Total time: ' . sprintf('%.4f', $callTime) .  " seconds");
            $this->log->addInfo('Memory usage: ' . (memory_get_usage(true) / 1024 / 1024) . " MB");
            $this->log->addInfo("End parse");
        
        }else{
            
            $this->log->addInfo("No new file!");
            $this->log->addInfo("End parse");
        }
    }
}
