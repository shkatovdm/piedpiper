<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Object;
use App\Resource;
use DB;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class SheetProcessor extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sheet:process';

    /**
     * 
     * @var obj
     */
    protected $log;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        // create a log channel
        $this->log = new Logger('name');
        $this->log->pushHandler(new StreamHandler(config('app.sheet_process_log'), Logger::INFO));
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $this->log->addInfo('----------------- Start parse -----------------');
        //$this->log->addInfo('Get info on object');
        
        $Object = Object::where('file_processed', 0)
                        ->orderBy('created_at', 'DESC')
                        ->first();
//$str = '458-8794';
                        /*if(preg_match("/^\d{3}-\d{4}/", $str)) {

                            echo $str . "\n";
                        }*/
//$Object = false;        
        if ($Object) {            

            $sFilePath = storage_path('app/' . $Object->file_name);
            
            /** Include PHPExcel_IOFactory */
            require_once dirname(__FILE__) . '/../../lib/phpexcel/Classes/PHPExcel/IOFactory.php';

            $callStartTime = microtime(true);

            $oPHPExcel = \PHPExcel_IOFactory::load($sFilePath);
            
            //  Get worksheet dimensions
            $sheet = $oPHPExcel->getSheet(0);
            //svar_dump($sheet->getCell('D17')->getValue());
            $highestRow = $sheet->getHighestRow();
            $highestColumn = $sheet->getHighestColumn();

            //  Loop through each row of the worksheet in turn

            try {
                
                DB::beginTransaction();

                    for ($row = 12; $row <= $highestRow; $row++) {
                        
                        $base_price    = $sheet->getCell('E' . $row)->getValue();
                        $code_resource = $sheet->getCell('A' . $row)->getValue();
                        $name_resource = $sheet->getCell('B' . $row)->getValue();
                        $measurement   = $sheet->getCell('C' . $row)->getValue();

                        //проверяем код на соответсвие шабону
                      /*  if(preg_match("/^\d{3}-\d{4}/", $code_resource)) {

                            echo $code_resource . "\n";
                        }*/
                        if($base_price && preg_match("/^\d{3}-\d{4}/", $code_resource)) {
                            DB::table('sheet')->insert([
                                                'object_id'     => $Object->id, 
                                                'code_resource' => $code_resource,
                                                'name_resource' => $name_resource,
                                                'measurement'   => $measurement,
                                                'quantity'      => $sheet->getCell('D' . $row)->getValue(),
                                                'base_price'    => $base_price
                            ]);
                        
                            //сохряняем ресурс
                            if(!Resource::where('code', $code_resource)->first()){
                                
                                DB::table('resource')->insert([
                                                    'code'        => $code_resource,
                                                    'name'        => $name_resource,
                                                    'measurement' => $measurement,
                                ]);

                            }
                        }
                    }

                $Object->file_processed = 1;
                $Object->save();
                DB::commit();

            } catch (Exception $e) {
                
                DB::rollBack();
                $this->log->addError($e->getMessage());
            }
            
            $callEndTime = microtime(true);
            $callTime = $callEndTime - $callStartTime;
            
            $this->log->addInfo('Total time: ' . sprintf('%.4f', $callTime) .  " seconds");
            $this->log->addInfo('Memory usage: ' . (memory_get_usage(true) / 1024 / 1024) . " MB");
            $this->log->addInfo("End parse");
        
        }else{
            
            $this->log->addInfo("No new sheets!");
            $this->log->addInfo("End parse");
        }
    }
}
