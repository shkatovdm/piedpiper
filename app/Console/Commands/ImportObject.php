<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Object;
use App\Resource;
use DB;
use Monolog\Logger;
use Monolog\Handler\StreamHandler;

class ImportObject extends Command
{
	/**
	 * The name and signature of the console command.
	 *
	 * @var string
	 */
	protected $signature = 'import:objects';

	/**
	 * 
	 * @var obj
	 */
	protected $log;

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
		// create a log channel
		$this->log = new Logger('name');
		$this->log->pushHandler(new StreamHandler(config('app.import_objects_log'), Logger::INFO));
	}

	/**
	 * Execute the console command.
	 *
	 * @return mixed
	 */
	public function handle()
	{
		$this->log->addInfo('----------------- Start import -----------------');
		
		$sFilePath = public_path('upload/import_object.xlsx');
		
		$this->log->addInfo('Поиск файла импорта');

		if (file_exists($sFilePath)) {                       

			$this->log->addInfo('Файл найден. Старт импорта');

			/** Include PHPExcel_IOFactory */
			require_once dirname(__FILE__) . '/../../lib/phpexcel/Classes/PHPExcel/IOFactory.php';

			$callStartTime = microtime(true);

			$oPHPExcel = \PHPExcel_IOFactory::load($sFilePath);
			
			//  Get worksheet dimensions
			$sheet = $oPHPExcel->getSheet(0);

			$highestRow = $sheet->getHighestRow();
			$highestColumn = $sheet->getHighestColumn();

			//  Loop through each row of the worksheet in turn

			try {
				
				DB::beginTransaction();

					for ($row = 5; $row <= $highestRow; $row++) {
						
						$district = $sheet->getCell('A' . $row)->getValue();
						$address_condominium = $sheet->getCell('D' . $row)->getValue();
						$city = $sheet->getCell('E' . $row)->getValue();
						$street = $sheet->getCell('F' . $row)->getValue();
						$house_number = $sheet->getCell('G' . $row)->getValue();
						$year_commissioning = $sheet->getCell('I' . $row)->getValue();
						$area = $sheet->getCell('J' . $row)->getValue();                        
						$area_full = $sheet->getCell('K' . $row)->getValue();                        
						$floors_count = $sheet->getCell('L' . $row)->getValue();
						$entrances_count = $sheet->getCell('M' . $row)->getValue();
						$heat_supply = $sheet->getCell('N' . $row)->getValue();
						$year_work_heat_meter = $sheet->getCell('O' . $row)->getValue();
						$year_work_hot_water = $sheet->getCell('P' . $row)->getValue();
						$year_work_hot_water_meter = $sheet->getCell('Q' . $row)->getValue();
						$year_work_cold_water = $sheet->getCell('R' . $row)->getValue();
						$year_work_cold_water_meter = $sheet->getCell('S' . $row)->getValue();
						$year_work_sewerage = $sheet->getCell('T' . $row)->getValue();
						$year_work_electric = $sheet->getCell('U' . $row)->getValue();
						$year_work_electric_meter = $sheet->getCell('V' . $row)->getValue();
						$year_work_gas = $sheet->getCell('W' . $row)->getValue();
						$year_work_gas_meter = $sheet->getCell('X' . $row)->getValue();
						$year_work_roof = $sheet->getCell('Y' . $row)->getValue();
						$year_work_facade = $sheet->getCell('Z' . $row)->getValue();
						$year_work_lift = $sheet->getCell('AA' . $row)->getValue();
						$year_work_basement = $sheet->getCell('AB' . $row)->getValue();
						$year_work_foundation = $sheet->getCell('AC' . $row)->getValue();
						$total_cost = $sheet->getCell('AD' . $row)->getValue();
						$conclusion = $sheet->getCell('AE' . $row)->getValue();

						//сохряняем объект   
						DB::table('object')->insert([
												'district' => $district,
												'city' => $city,
												'street' => $street,
												'house_number' => $house_number,
												'address_condominium' => $address_condominium,
												'year_commissioning' => $year_commissioning,
												'area' => $area,
												'area_full' => $area_full,
												'floors_count' => $floors_count,
												'entrances_count' => $entrances_count,
												'heat_supply' => $heat_supply,
											    'year_work_heat_meter' => $year_work_heat_meter,
											    'year_work_hot_water' => $year_work_hot_water,
											    'year_work_hot_water_meter' => $year_work_hot_water_meter,
											    'year_work_cold_water' => $year_work_cold_water,
											    'year_work_cold_water_meter' => $year_work_cold_water_meter,
											    'year_work_sewerage' => $year_work_sewerage,
											    'year_work_electric' => $year_work_electric,
											    'year_work_electric_meter' => $year_work_electric_meter,
											    'year_work_gas' => $year_work_gas,
											    'year_work_gas_meter' => $year_work_gas_meter,
											    'year_work_roof' => $year_work_roof,
											    'year_work_facade' => $year_work_facade,
											    'year_work_lift' => $year_work_lift,
											    'year_work_basement' => $year_work_basement,
											    'year_work_foundation' => $year_work_foundation,
											    'total_cost' => $total_cost,
											    'conclusion' => $conclusion,
						]);

					}

				DB::commit();

			} catch (Exception $e) {
				
				DB::rollBack();
				$this->log->addError($e->getMessage());
			}
			
			$callEndTime = microtime(true);
			$callTime = $callEndTime - $callStartTime;
			
			$this->log->addInfo('Total time: ' . sprintf('%.4f', $callTime) .  " seconds");
			$this->log->addInfo('Memory usage: ' . (memory_get_usage(true) / 1024 / 1024) . " MB");
			$this->log->addInfo("End parse");
		
		}else{
			
			$this->log->addInfo("Файл импорта объектов не найден");
			$this->log->addInfo("End parse");
		}
	}
}
