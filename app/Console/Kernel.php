<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        \App\Console\Commands\Inspire::class,
        \App\Console\Commands\SheetProcessor::class,
        \App\Console\Commands\MakeReportShipments::class,
        \App\Console\Commands\ImportProducts::class,
        \App\Console\Commands\ImportObject::class,
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('inspire')->hourly();
        $schedule->command('sheet:process')->everyFiveMinutes();
        $schedule->command('import:products')->everyFiveMinutes();
        $schedule->command('report:shipments')->weekly()->mondays()->at('00:00');
    }
}
