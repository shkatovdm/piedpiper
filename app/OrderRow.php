<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class OrderRow extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'order_row';

    public static function getByID($order_id)
    {   
        return DB::table('order_row')
            ->join('product', 'product.id', '=', 'order_row.product_id')
            ->join('order', 'order.id', '=', 'order_row.order_id')
            ->join('resource', 'product.resource_code', '=', 'resource.code')
            ->join('store', 'product.store_id', '=', 'store.id')
            ->join('sheet', 'order_row.sheet_id', '=', 'sheet.id')
            ->select(
                'order_row.id', 
                'order_row.sheet_id', 
                'order_row.product_id', 
                'sheet.quantity as count',
                'order.id as order_id',
                'order.status_id',
                'store.id', 
                'store.name',
                'store.street as store_street',
                'store.house_number as store_house_number',
                'store.housing as store_housing',
                'store.building as store_building',
                'resource.name as name_resource',
                'resource.code as code_resource',
                'resource.measurement as measurement_resource',
                'product.article as product_article',
                'product.name as product_name',
                'product.price as product_price',
                DB::raw('product.price * sheet.quantity as total')
            )
            ->where('order.id', $order_id);
    }

    public static function getStoreByOrderID($order_id)
    {   
        return DB::table('order_row')
            ->join('product', 'product.id', '=', 'order_row.product_id')
            ->join('order', 'order.id', '=', 'order_row.order_id')
            ->join('resource', 'product.resource_code', '=', 'resource.code')
            ->join('store', 'product.store_id', '=', 'store.id')
            ->select(
                'store.id', 
                'store.name',
                'store.street as street',
                'store.house_number as store_house_number',
                'store.housing as store_housing',
                'store.building as store_building'
            )
            ->where('order.id', $order_id)
            ->groupBy('store.id');
    }

    public static function getByStoreID($user_id, $store)
    {   
        return DB::table('order_row')
            ->join('product', 'product.id', '=', 'order_row.product_id')
            ->join('order', 'order.id', '=', 'order_row.order_id')
            ->join('resource', 'product.resource_code', '=', 'resource.code')
            ->join('store', 'product.store_id', '=', 'store.id')
            ->join('sheet', 'order_row.sheet_id', '=', 'sheet.id')
            ->select(
                'order_row.id', 
                'order_row.sheet_id', 
                'order_row.product_id', 
                'sheet.quantity as count',
                'order.id as order_id',
                'order.status_id',
                'order.waybill as waybill',
                'store.id', 
                'store.name',
                'resource.name as name_resource',
                'resource.code as code_resource',
                'resource.measurement as measurement_resource',
                'product.article as product_article',
                'product.name as product_name',
                'product.price as product_price',
                DB::raw('product.price * sheet.quantity as total')
            )
            ->where('order.customer_id', $user_id)
            ->where('store.id', $store);
            //->groupBy('store.id');
    }

    public static function getStoreOrderByStatusID($user_id, $status)
    {   
        return DB::table('order_row')
            ->join('product', 'product.id', '=', 'order_row.product_id')
            ->join('order', 'order.id', '=', 'order_row.order_id')
            ->join('store', 'product.store_id', '=', 'store.id')
            ->select(
                'store.id', 
                'store.name',
                'order.waybill as waybill',
                'order.comment as comment',
                'order.id as order_id'
            )
            ->where('order.status_id', $status)
            ->where('order.customer_id', $user_id)
            ->groupBy('store.id');
    }

	public static function checkOrderExist($product_id, $user_id)
    {   
    	//получаем склад нового продукта
        $store =  DB::table('product')
            ->join('store', 'product.store_id', '=', 'store.id')
            ->select(
                'store.id', 
                'store.name'
            )
            ->where('product.id', $product_id)
            ->first();

        //если заказов еще нет
        if(empty($store)) return false;
    	
    	//проверяем, есть ли такой склад в заказах текущего пользователя
    	return DB::table('order_row')
            ->join('product', 'product.id', '=', 'order_row.product_id')
            ->join('store', 'product.store_id', '=', 'store.id')
            ->join('order', 'order_row.order_id', '=', 'order.id')
            ->select(
                'store.id', 
                'store.name',
                'order.id as order_id'
            )
            ->where('store.id', $store->id)
            ->where('order.status_id', 1) 
            ->where('order.customer_id', $user_id)
            ->first();
		}
/*
    public static function getOrderByProductID($product_id)
    {   
        return DB::table('order_row')
            ->join('product', 'product.id', '=', 'order_row.product_id')
            ->join('order', 'order.id', '=', 'order_row.order_id')
            //->join('resource', 'product.resource_code', '=', 'resource.code')
            ->join('store', 'product.store_id', '=', 'store.id')
            ->select(
                'order.id', 
                'order.name'
            )
            ->where('order_row.product_id', $product_id);
    }*/
}
