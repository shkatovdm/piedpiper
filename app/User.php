<?php

namespace App;

use App\Role;
use DB;
use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'email', 'password', 'role'];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = ['password', 'remember_token'];

    /**
     * The role current user.
     *
     * @var string
     */
    public $role = '';

    public $permissions = [

            'admin'     => ['read', 'create', 'update', 'delete'],
            'operator'  => ['read', 'create', 'update']
    ];

    /**
     * Get a role user.
     *
     * @return string
     */
    public function getRoleCurrUser()
    {   
        if($this->role) return $this->role;
            
        $iRoleID = $this->role_id;
        $oRole = Role::find($iRoleID);
        $this->role = $oRole->name;
        
        return $this->role;
    }
/*
    public function getActions($oModel)
    {   
        foreach ($oModel->permissions[$this->getRoleCurrUser()] as $value) {
            $permissions[$value] = true;
        }
        return array_merge(['entity' => $oModel->table, 'model' => $oModel], $permissions);   
    }
  */  
    public function getUsers()
    {   
        return DB::table('users')
            ->join('roles', 'roles.id', '=', 'users.role_id')
            ->select('users.id', 'users.name', 'users.email', 'users.role_id', 'roles.name as role_name')
            ->orderBy('users.created_at', 'DESC')
            ->paginate(10);
    }

    public static function getExecutors()
    {   
        return DB::table('users')
            ->join('roles', 'roles.id', '=', 'users.role_id')
            ->join('org',   'org.id',   '=', 'users.org_id')
            ->select(
                'users.id', 
                'users.name', 
                'users.email', 
                'users.role_id', 
                'roles.name as role_name',
                'org.name as org_name'
            )
            ->where('roles.id', 4);
    }
}
