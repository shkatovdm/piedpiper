var proton = {};
var verboseBuild = true;
var screenXs = 480; // JS equivalent to @screen-xs less variable, if you change this in proton-variables.less, you must also change it here
var screenMd = 992; // JS equivalent to @screen-md less variable, if you change this in proton-variables.less, you must also change it here

// Check if browser is internet explorer later than version 9
var ltIE9 = false;
!$('html').is('.lt-ie9') || (ltIE9 = true);
if(ltIE9){
	verboseBuild = false;
}

// Adds iOS modernizr tests
Modernizr.addTest('ipad', function () {
  return !!navigator.userAgent.match(/iPad/i);
});
 
Modernizr.addTest('iphone', function () {
  return !!navigator.userAgent.match(/iPhone/i);
});
 
Modernizr.addTest('ipod', function () {
  return !!navigator.userAgent.match(/iPod/i);
});
 
Modernizr.addTest('appleios', function () {
  return (Modernizr.ipad || Modernizr.ipod || Modernizr.iphone);
});

// add ios-device class to html if ios is used to view
if(Modernizr.appleios){
	$('html').addClass('ios-device');
}
$('#modalForm').modal()                      // initialized with defaults
$('#modalForm').modal('show')                // initializes and invokes show immediately

!verboseBuild || console.log('Starting builds:');

$('.btn-redirect').on('click', function() {
	//console.log($(this).attr("data-href"));
	window.location.replace($(this).attr("data-href"));
});

$('.btn-confirm').on('click', function() {
	if(!confirm("Вы уверены?")){
		
		return false;
	}
});

$(document).on('click', '.ajax_pagin .pagination a', function (e) {
	
	getObjects($(this).attr('href').split('page=')[1]);
	e.preventDefault();

});

function getObjects(page) {
	
	$.ajax({
		url: '/objects/listAJAX?&page=' + page,
		dataType: 'html',	
		data:{
			lot_id: $('#lot_id').val(),
		}
	
	}).done(function(data) {
		//console.log(data.data);
		
		$('.ajax_pagin ul.pagination li').removeClass('active');
		$('.ajax_pagin ul.pagination li').eq(data.current_page).addClass('active');
		
		$('#addObjectModal .modal-body').html(data);
		/*var key = 0;
		$('.modal-dialog tbody tr.gradeX').each(function(index) {
			
			var row = '';
			row += '<td><a href="http://piedpiper.localhost/objects/edit/3">3</a></td>';
			row += '<td>' + data.data[key].id + '</td>';
			row += '<td>' + data.data[key].city_name + '</td>';
			row += '<td>' + data.data[key].street + '</td>';
			row += '<td>' + data.data[key].house_number + '</td>';
			row += '<td><form action="/lots/add_object/' + data.data[key].id + '" method="post" data-parsley-namespace="data-parsley-" data-parsley-validate>';
			row += '<?=csrf_field()?>';
			row += '<input type="hidden" name="lot_id" value="<?=$lot->id?>">';
			row += '<input type="hidden" name="object_id" value="<?=$item->id?>">';
			row += '<button type="submit" class="btn btn-sm btn-primary">Выбрать</button>';
			row += '</form></td>';

			$(this).html(row);
			
			key++;
		});*/
	
	}).fail(function () {
		alert('Не удалось загрузить данные!');
	});
}

$(document).on('click', '.sorting th', function(e){

	var sort = $(this).data('sort');
	var params = [];
	var url = document.location.href;
	var change = false;

	if (sort != undefined) {
		url.replace(/[?&]+([^=&]+)=([^&]*)/gi, function (m, key, value) {
			params[key] = value;
		});
		if (url.indexOf('?') != -1) {
			url = url.substr(0, url.indexOf('?'));
		}
		if (params['sort'] !== undefined) {
			if (params['sort'] == sort) {
				if (params['sort'].indexOf(':')) change = true;
			}
		}
		if (change) {
			document.location.href = url + '?sort=' + sort + ':desc';
		} else {
			document.location.href = url + '?sort=' + sort;
		}
	}
});

$(document).on('click', '#search-products', function (e) {
	
	$.ajax({
		url: '/products/listAJAX',
		dataType: 'html', 
		data:{
		  sheet: $(this).data('sheet-id'),
		  code: $(this).data('code-resource'),
	  }
	
	}).done(function(data) {
	  
	  //$('.ajax_pagin ul.pagination li').removeClass('active');
	  //$('.ajax_pagin ul.pagination li').eq(data.current_page).addClass('active');
	
	  $('#addObjectModal .modal-body').html(data);
	
	}).fail(function () {
		alert('Не удалось загрузить данные!');
	});
});

$(document).on('click', '#btn-create-attribute-save', function (e) {
	
	data = $(this).parents('form').serialize();
	url  = $(this).parents('form').attr('action');
	
	var formData = new FormData($(this).parents('form')[0]);
	//$(".loading").show();
console.log(formData);
	$.ajax({
		url: url,
		method: 'POST',
		enctype: 'multipart/form-data',
		dataType: 'html', 
		processData: false,
		contentType: false,
		data:formData
	
	}).done(function(data) {
	
	  $('#add_object').html(data);
	  $(".loading").hide();
	
	}).fail(function () {
		alert('Не удалось загрузить данные!');
		//$(".loading").hide();
	});
});

$(document).on('click', '#addObjectModalClose, #addUserModalClose', function (e) {
	
	location.reload();
});

$(document).on('click', '#addUserSubmit', function (e) {
	
	data = $(this).parents('form').serialize();
	url  = $(this).parents('form').attr('action');
	
	var formData = new FormData($(this).parents('form')[0]);
	
	$.ajax({
		url: url,
		method: 'POST',
		enctype: 'multipart/form-data',
		dataType: 'html', 
		processData: false,
		contentType: false,
		data:formData
	
	}).done(function(data) {
	
	  //console.log(data);
	  $('#addUserForm').html(data);
	
	}).fail(function () {
		alert('Не удалось загрузить данные!');
	});
});

$(document).on('click', '#addUserModalClose', function (e) {
	
	$.ajax({
		url: '/users/executors',
		method: 'get',
		dataType: 'json', 
	
	}).done(function(data) {

		var str = '';

		$.each(data.executors, function(key, executors){

			str += '<option value="' + executors.id + '">' + executors.name + '</option>';
			
		});

		$('#executor_select').html(str);
	
	}).fail(function () {
		alert('Не удалось загрузить данные!');
	})   
});
$(document).on('click', '#getWaybill', function (e) {
	
	$('#get-waybill-form').submit(); 
});