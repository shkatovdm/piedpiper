<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableExecutor extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('executor', function (Blueprint $table) {
            $table->increments('id');
            $table->string('organization_name');
            $table->string('organization_address');
            $table->string('phone');
            $table->string('inn');
            $table->string('ppc');
            $table->string('bin');
            $table->string('head');
            $table->string('chief_accountant');
            $table->string('checking_account');
            $table->string('bank');
            $table->string('bic_bank');
            $table->string('inn_bank');
            $table->string('correspondent_account');

            $table->integer('user_id')->unsigned();
            $table->foreign('user_id')->references('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('executor');
    }
}
