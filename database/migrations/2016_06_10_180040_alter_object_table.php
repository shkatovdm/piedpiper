<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterObjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('object', function ($table) {
            
            $table->string('district');
            $table->string('address_condominium');
            $table->string('year_commissioning');
            $table->string('area');
            $table->string('floors_count');
            $table->string('heat_supply');
            $table->string('year_work_heat_meter');
            $table->string('year_work_hot_water');
            $table->string('year_work_hot_water_meter');
            $table->string('year_work_cold_water');
            $table->string('year_work_cold_water_meter');
            $table->string('year_work_sewerage');
            $table->string('year_work_electric');
            $table->string('year_work_electric_meter');
            $table->string('year_work_gas');
            $table->string('year_work_gas_meter');
            $table->string('year_work_roof');
            $table->string('year_work_facade');
            $table->string('year_work_lift');
            $table->string('year_work_basement');
            $table->string('year_work_foundation');
            $table->string('total_cost');
            $table->string('conclusion');
            $table->string('estimated_cost_locally');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        return true;
    }
}
