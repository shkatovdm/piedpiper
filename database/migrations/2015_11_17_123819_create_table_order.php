<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrder extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order', function (Blueprint $table) {
            
            $table->increments('id');
            $table->integer('status_id')->unsigned();
            $table->integer('customer_id')->unsigned();
            $table->string('comment');
            //$table->integer('provider_id')->unsigned();
            $table->timestamps();

            $table->foreign('status_id')->references('id')->on('status');
            $table->foreign('customer_id')->references('id')->on('users');
            //$table->foreign('provider_id')->references('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('order');
    }
}
