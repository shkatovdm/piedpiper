<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableObject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('object', function ($table) {
            
            $table->string('area_full')->nullable();
            $table->string('entrances_count')->nullable();
            $table->string('city')->nullable();
            $table->string('area')->nullable()->change();
            $table->string('street')->nullable()->change();
            $table->string('house_number')->nullable()->change();
            $table->string('housing')->nullable()->change();
            $table->string('building')->nullable()->change();
            $table->string('year_commissioning')->nullable()->change();
            $table->string('floors_count')->nullable()->change();
            $table->string('heat_supply')->nullable()->change();
            $table->string('year_work_heat_meter')->nullable()->change();
            $table->string('year_work_hot_water')->nullable()->change();
            $table->string('year_work_hot_water_meter')->nullable()->change();
            $table->string('year_work_cold_water')->nullable()->change();
            $table->string('year_work_cold_water_meter')->nullable()->change();
            $table->string('year_work_sewerage')->nullable()->change();
            $table->string('year_work_electric')->nullable()->change();
            $table->string('year_work_electric_meter')->nullable()->change();
            $table->string('year_work_gas')->nullable()->change();
            $table->string('year_work_gas_meter')->nullable()->change();
            $table->string('year_work_roof')->nullable()->change();
            $table->string('year_work_facade')->nullable()->change();
            $table->string('year_work_lift')->nullable()->change();
            $table->string('year_work_basement')->nullable()->change();
            $table->string('year_work_foundation')->nullable()->change();
            $table->string('total_cost')->nullable()->change();
            $table->string('conclusion')->nullable()->change();
            $table->string('estimated_cost_locally')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        return true;
    }
}
