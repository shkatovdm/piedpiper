<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ObjectsStringsToInteger extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('object', function ($table) {
            $table->integer('floors_count')->change();
            $table->integer('heat_supply')->change();
            $table->integer('year_work_heat_meter')->change();
            $table->integer('year_work_hot_water')->change();
            $table->integer('year_work_hot_water_meter')->change();
            $table->integer('year_work_cold_water')->change();
            $table->integer('year_work_cold_water_meter')->change();
            $table->integer('year_work_sewerage')->change();
            $table->integer('year_work_electric')->change();
            $table->integer('year_work_electric_meter')->change();
            $table->integer('year_work_gas')->change();
            $table->integer('year_work_gas_meter')->change();
            $table->integer('year_work_roof')->change();
            $table->integer('year_work_facade')->change();
            $table->integer('year_work_lift')->change();
            $table->integer('year_work_basement')->change();
            $table->integer('year_work_foundation')->change();
            $table->integer('total_cost')->change();
            $table->integer('entrances_count')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
