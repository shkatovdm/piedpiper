<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableStore extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('store', function (Blueprint $table) {
            
            $table->increments('id');
            $table->string('name');
            $table->integer('city_id')->unsigned();
            $table->string('street');
            $table->string('house_number');
            $table->string('housing');
            $table->string('building');
            $table->integer('provider_id')->unsigned();
            $table->timestamps();

            $table->foreign('provider_id')->references('id')->on('users');
            $table->foreign('city_id')->references('id')->on('city');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('store');
    }
}
