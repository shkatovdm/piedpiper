<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProduct extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product', function (Blueprint $table) {
            $table->increments('id');
            $table->string('article');
            $table->string('resource_code')->unique();
            $table->string('name');
            $table->string('price');
            $table->string('balance');
            $table->integer('store_id')->unsigned();
            $table->timestamps();

            $table->foreign('store_id')->references('id')->on('store');
            $table->foreign('resource_code')->references('code')->on('resource');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('product');
    }
}
