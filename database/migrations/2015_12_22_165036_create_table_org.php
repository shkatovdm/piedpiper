<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableOrg extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('org', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('address');
            $table->string('phone');
            $table->string('inn');
            $table->string('ppc');
            $table->string('bin');
            $table->string('head');
            $table->string('chief_accountant');
            $table->string('checking_account');
            $table->string('bank');
            $table->string('bic_bank');
            $table->string('inn_bank');
            $table->string('correspondent_account');
            $table->enum('type', ['system', 'executor', 'provider'])->default('system');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('org');
    }
}
