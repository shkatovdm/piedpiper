<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFieldsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::create('fields_type', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type');
        });

        Schema::create('fields_input', function (Blueprint $table) {
            $table->increments('id');
            $table->string('input');
        });

        Schema::create('fields_group', function (Blueprint $table) {
            $table->increments('id');
            $table->string('group');
            $table->integer('parent')->nullable()->unsigned();
            $table->integer('sort');
        });

        Schema::create('fields', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->integer('type')->unsigned();
            $table->integer('input')->unsigned();
            $table->integer('group')->unsigned();
            $table->string('collection');
            $table->text('params')->nullable();
            $table->integer('sort');
            $table->integer('required');
        });

        Schema::create('fields_value', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('field')->unsigned();
            $table->string('string')->nullable();
            $table->integer('integer')->nullable();
            $table->float('float')->nullable();
        });

        Schema::table('fields',function ($table){
            $table->foreign('type')->references('id')->on('fields_type');
            $table->foreign('input')->references('id')->on('fields_input');
            $table->foreign('group')->references('id')->on('fields_group');
        });

        Schema::table('fields_value',function ($table){
            $table->foreign('field')->references('id')->on('fields');
        });

        Schema::table('fields_group',function ($table){
            $table->foreign('parent')->references('id')->on('fields_group');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fileds');
        Schema::drop('fields_type');
        Schema::drop('fields_input');
        Schema::drop('fields_group');
        Schema::drop('fields_value');
    }
}
