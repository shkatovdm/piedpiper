<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableObject extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('object', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('city_id');
            $table->string('street');
            $table->string('house_number');
            $table->string('housing');
            $table->string('building');
            $table->string('person_charge');
            $table->string('phone_person_charge');
            $table->string('funding');
            $table->string('file_name');
            $table->string('original_file_name');
            $table->integer('file_processed');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('object');
    }
}
