<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        DB::table('org')->insert([
            'name'                  => 'Системное',
            'address'               => 'Системное',
            'phone'                 => 'Системное',
            'inn'                   => 'Системное',
            'ppc'                   => 'Системное',
            'bin'                   => 'Системное',
            'head'                  => 'Системное',
            'chief_accountant'      => 'Системное',
            'checking_account'      => 'Системное',
            'bank'                  => 'Системное',
            'bic_bank'              => 'Системное',
            'inn_bank'              => 'Системное',
            'correspondent_account' => 'Системное'
        ]);

        DB::table('roles')->insert([
            ['name' => 'admin'],
            ['name' => 'customer'],
            ['name' => 'provider'],
            ['name' => 'executor'],
            ['name' => 'coordinator'],
        ]);

        DB::table('users')->insert([
            'name'      => 'shkatov.dmitry',
            'email'     => 'shkatov.dmitry@gmail.com',
            'password'  => bcrypt('15jwdi'),
            'role_id'   => 1,
            'org_id'   => 1
        ]);

        DB::table('users')->insert([
            'name'      => 'customer',
            'email'     => 'customer@gmail.com',
            'password'  => bcrypt('asjmel22'),
            'role_id'   => 2,
            'org_id'   => 1
        ]);

        DB::table('users')->insert([
            'name'      => 'тестовый поставщик',
            'email'     => 'provider@gmail.com',
            'password'  => bcrypt('asjmelwd22'),
            'role_id'   => 3,
            'org_id'   => 1
        ]);

        DB::table('users')->insert([
            'name'      => 'тестовый исполнитель',
            'email'     => 'executor@gmail.com',
            'password'  => bcrypt('asdfjmel22'),
            'role_id'   => 4,
            'org_id'   => 1
        ]);

        DB::table('users')->insert([
            'name'      => 'тестовый согласующий',
            'email'     => 'coordinator@gmail.com',
            'password'  => bcrypt('efjowie56378'),
            'role_id'   => 5,
            'org_id'   => 1
        ]);
        
        DB::table('city')->insert([
            'name' => 'Оренбург',
        ]);

        DB::table('store')->insert([
            [
                'provider_id'  => 3,
                'name'         => 'Тестовый склад 1',
                'city_id'      => 1,
                'street'       => 'Малая Бронная',
                'house_number' => '3',
                'housing'      => '',
                'building'     => '',
            ],[
                'provider_id'   => 3,
                'name' => 'Тестовый склад 2',
                'city_id'      => 1,
                'street'       => 'Моховая',
                'house_number' => '114',
                'housing'      => '',
                'building'     => '',
            ],[
                'provider_id'   => 3,
                'name' => 'Тестовый склад 3',
                'city_id'      => 1,
                'street'       => 'Кима',
                'house_number' => '77',
                'housing'      => '',
                'building'     => '',
            ]
        ]);

        DB::table('status')->insert([
            ['name' => 'подбор товара'],                        //1
            ['name' => 'ожидает подтверждения поставщиком'],    //2
            ['name' => 'не подтвержден поставщиком'],           //3
            ['name' => 'ожидает согласования заказчиком'],      //4
            ['name' => 'не согласован заказчиком'],             //5
            ['name' => 'согласован'],                           //6
            ['name' => 'ожидает отгрузки'],                     //7
            ['name' => 'отгружен']                              //8
        ]);

        DB::table('resource')->insert([
            [
                'code'        => '101-0063',
                'name'        => 'Ацетилен растворенный технический марки А',
                'measurement' => '2589',
            ],[
                'code'        => '101-0078',
                'name'        => 'Битумы нефтяные строительные кровельные марки БНК-45/190, БНК-45/180',
                'measurement' => '1800',
            ],[
                'code'        => '101-0092',
                'name'        => 'Болты с шестигранной головкой диаметром резьбы 16 (18) мм',
                'measurement' => '3000',
            ],[
                'code'        => '101-0115',
                'name'        => 'Винты с полукруглой головкой длиной 50 мм',
                'measurement' => '2450',
            ]
        ]);

        DB::table('product')->insert([
            [
                'article'       => '4589785',
                'resource_code' => '101-0063',
                'name'          => 'Ацетилен растворенный технический марки А',
                'price'         => '2589',
                'balance'       => '15',
                'store_id'      => 1,
            ],[
                'article'       => '4589786',
                'resource_code' => '101-0078',
                'name'          => 'Битумы нефтяные строительные кровельные марки БНК-45/190, БНК-45/180',
                'price'         => '1800',
                'balance'       => '10',
                'store_id'      => 1,
            ],[
                'article'       => '4589787',
                'resource_code'          => '101-0092',
                'name'          => 'Болты с шестигранной головкой диаметром резьбы 16 (18) мм',
                'price'         => '3000',
                'balance'       => '150',
                'store_id'      => 2,
            ],[
                'article'       => '4589788',
                'resource_code'          => '101-0115',
                'name'          => 'Винты с полукруглой головкой длиной 50 мм',
                'price'         => '2450',
                'balance'       => '250',
                'store_id'      => 3,
            ]
        ]);

        DB::table('fields_group')->insert([
            [
                'group' => 'Общая информация',
                'parent' => NULL,
                'sort' => 1
            ],[
                'group' => 'План проведения капремонта',
                'parent' => NULL,
                'sort' => 2
            ],[
                'group' => 'Конструктивные элементы объекта',
                'parent' => 2,
                'sort' => 1
            ],[
                'group' => 'Инженерные сети объекта',
                'parent' => 2,
                'sort' => 2
            ],[
                'group' => 'Оборудование объекта',
                'parent' => 2,
                'sort' => 3
            ],[
                'group' => 'Крыша',
                'parent' => 3,
                'sort' => 1
            ],[
                'group' => 'Фасад',
                'parent' => 3,
                'sort' => 2
            ],[
                'group' => 'Подвальное помещение',
                'parent' => 3,
                'sort' => 3
            ],[
                'group' => 'Фундамент',
                'parent' => 3,
                'sort' => 4
            ],[
                'group' => 'Теплоснабжение',
                'parent' => 4,
                'sort' => 1
            ],[
                'group' => 'Горячее водоснабжение',
                'parent' => 4,
                'sort' => 2
            ],[
                'group' => 'Холодное водоснабжение',
                'parent' => 4,
                'sort' => 3
            ],[
                'group' => 'Водоотведение',
                'parent' => 4,
                'sort' => 4
            ],[
                'group' => 'Электроснабжение',
                'parent' => 4,
                'sort' => 5
            ],[
                'group' => 'Газоснабжение',
                'parent' => 4,
                'sort' => 6
            ],[
                'group' => 'Пожаротушение',
                'parent' => 4,
                'sort' => 7
            ],[
                'group' => 'Лифтовое оборудование',
                'parent' => 5,
                'sort' => 1
            ],[
                'group' => 'Коллективный ПУ теплоснабжения',
                'parent' => 5,
                'sort' => 2
            ],[
                'group' => 'Коллективный ПУ ГВС',
                'parent' => 5,
                'sort' => 3
            ],[
                'group' => 'Коллективный ПУ ХВС',
                'parent' => 5,
                'sort' => 4
            ],[
                'group' => 'Коллективный ПУ электроснабжения',
                'parent' => 5,
                'sort' => 5
            ],[
                'group' => 'Коллективный ПУ газоснабжения',
                'parent' => 5,
                'sort' => 6
            ],[
                'group' => 'Сведения о проведённых работах по капитальному ремонту',
                'parent' => 5,
                'sort' => 7
            ]
        ]);

        DB::table('fields_input')->insert([
            ['input' => 'text'],
            ['input' => 'select'],
            ['input' => 'file'],
            ['input' => 'checkbox']
        ]);

        DB::table('fields_type')->insert([
            ['type' => 'string'],
            ['type' => 'integer'],
            ['type' => 'float'],
        ]);

        DB::table('fields')->insert([
            [
                'name' => 'Муниципальный округ',
                'input' => '1',
                'type' => '1',
                'group' => '1',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '1',
                'required' => 0
            ],[
                'name' => 'Город',
                'input' => '1',
                'type' => '1',
                'group' => '1',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '2',
                'required' => 0
            ],[
                'name' => 'Адрес объекта',
                'input' => '1',
                'type' => '1',
                'group' => '1',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '3',
                'required' => 0
            ],[
                'name' => 'Год ввода в эксплуатацию',
                'input' => '1',
                'type' => '2',
                'group' => '1',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '4',
                'required' => 0
            ],[
                'name' => 'Общая площадь МКД - всего (м.кв.)',
                'input' => '1',
                'type' => '2',
                'group' => '1',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '5',
                'required' => 0
            ],[
                'name' => 'Площадь помещений МКД (м.кв.)',
                'input' => '1',
                'type' => '2',
                'group' => '1',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '6',
                'required' => 0
            ],[
                'name' => 'Колличество этажей',
                'input' => '1',
                'type' => '2',
                'group' => '1',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '7',
                'required' => 0
            ],[
                'name' => 'Колличество подъездов',
                'input' => '1',
                'type' => '2',
                'group' => '1',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '8',
                'required' => 0
            ],[
                'name' => 'Колличество жителей',
                'input' => '1',
                'type' => '2',
                'group' => '1',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '9',
                'required' => 0
            ],[
                'name' => 'Объект культурного наследия',
                'input' => '4',
                'type' => '2',
                'group' => '1',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '10',
                'required' => 0
            ],[
                'name' => 'Плановый год проведения работ',
                'input' => '1',
                'type' => '2',
                'group' => '6',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '1',
                'required' => 0
            ],[
                'name' => 'Год последнего капитального ремонта',
                'input' => '1',
                'type' => '2',
                'group' => '6',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '2',
                'required' => 0
            ],[
                'name' => 'По конструктивному исполнению',
                'input' => '2',
                'type' => '2',
                'group' => '6',
                'collection' => 'object',
                'params' => 'Скатная;Плоская;Плоская с вентилируем чердачным пространством',
                'sort' => '3',
                'required' => 0
            ],[
                'name' => 'По виду покрытия',
                'input' => '2',
                'type' => '2',
                'group' => '6',
                'collection' => 'object',
                'params' => 'Мягая рулонная;Листовое покрытие',
                'sort' => '4',
                'required' => 0
            ],[
                'name' => 'По организации водостока',
                'input' => '2',
                'type' => '2',
                'group' => '6',
                'collection' => 'object',
                'params' => 'Не организованный;Организованный;Организованный с внутренним водостоком',
                'sort' => '5',
                'required' => 0
            ],[
                'name' => 'По составу работ',
                'input' => '2',
                'type' => '2',
                'group' => '6',
                'collection' => 'object',
                'params' => 'С заменой утеплителя;Без замены утеплителя',
                'sort' => '6',
                'required' => 0
            ],[
                'name' => 'Сметная стоимость капитального ремонта (тыс.рублей)',
                'input' => '1',
                'type' => '2',
                'group' => '6',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '7',
                'required' => 0
            ],[
                'name' => 'Проектная документация',
                'input' => '3',
                'type' => '1',
                'group' => '6',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '8',
                'required' => 0
            ],[
                'name' => 'Сметная документация',
                'input' => '3',
                'type' => '1',
                'group' => '6',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '9',
                'required' => 0
            ],[
                'name' => 'Загрузить ресурсную ведомость',
                'input' => '3',
                'type' => '1',
                'group' => '6',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '10',
                'required' => 0
            ],[
                'name' => 'Плановый год проведения работ',
                'input' => '1',
                'type' => '2',
                'group' => '7',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '1',
                'required' => 0
            ],[
                'name' => 'Год последнего капитального ремонта',
                'input' => '1',
                'type' => '2',
                'group' => '7',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '2',
                'required' => 0
            ],[
                'name' => 'По конструктивному исполнению',
                'input' => '2',
                'type' => '2',
                'group' => '7',
                'collection' => 'object',
                'params' => 'Панельный;Кирпичный;Крупноблочный;Блочный;Деревянный',
                'sort' => '3',
                'required' => 0
            ],[
                'name' => 'По лицевой отделки фасада',
                'input' => '2',
                'type' => '2',
                'group' => '7',
                'collection' => 'object',
                'params' => 'Оштукатуренный;Облицованный штучным материалом;По материалу конструктивного исполнения стен',
                'sort' => '4',
                'required' => 0
            ],[
                'name' => 'По категории сложности',
                'input' => '2',
                'type' => '2',
                'group' => '7',
                'collection' => 'object',
                'params' => 'Нормальный;Сложный',
                'sort' => '5',
                'required' => 0
            ],[
                'name' => 'По составу работ',
                'input' => '2',
                'type' => '2',
                'group' => '7',
                'collection' => 'object',
                'params' => 'С дополнительным утеплением;Без дополнительного утепления',
                'sort' => '6',
                'required' => 0
            ],[
                'name' => 'Сметная стоимость капитального ремонта (тыс.рублей)',
                'input' => '1',
                'type' => '2',
                'group' => '7',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '7',
                'required' => 0
            ],[
                'name' => 'Проектная документация',
                'input' => '3',
                'type' => '1',
                'group' => '7',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '8',
                'required' => 0
            ],[
                'name' => 'Сметная документация',
                'input' => '3',
                'type' => '1',
                'group' => '7',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '9',
                'required' => 0
            ],[
                'name' => 'Загрузить ресурсную ведомость',
                'input' => '3',
                'type' => '1',
                'group' => '7',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '10',
                'required' => 0
            ],[
                'name' => 'Отметка об отсутствии подвального помещения',
                'input' => '4',
                'type' => '2',
                'group' => '8',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '0',
                'required' => 0
            ],[
                'name' => 'Плановый год проведения работ',
                'input' => '1',
                'type' => '2',
                'group' => '8',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '1',
                'required' => 0
            ],[
                'name' => 'Год последнего капитального ремонта',
                'input' => '1',
                'type' => '2',
                'group' => '8',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '2',
                'required' => 0
            ],[
                'name' => 'По конструктивному исполнению',
                'input' => '2',
                'type' => '2',
                'group' => '8',
                'collection' => 'object',
                'params' => 'Панельный;Кирпичный;Крупноблочный;Блочный;Деревянный',
                'sort' => '3',
                'required' => 0
            ],[
                'name' => 'По категории сложности планируемых работ',
                'input' => '2',
                'type' => '2',
                'group' => '8',
                'collection' => 'object',
                'params' => 'Нормальный;Сложный',
                'sort' => '4',
                'required' => 0
            ],[
                'name' => 'Сметная стоимость капитального ремонта (тыс.рублей)',
                'input' => '1',
                'type' => '2',
                'group' => '8',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '5',
                'required' => 0
            ],[
                'name' => 'Проектная документация',
                'input' => '3',
                'type' => '1',
                'group' => '8',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '6',
                'required' => 0
            ],[
                'name' => 'Сметная документация',
                'input' => '3',
                'type' => '1',
                'group' => '8',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '7',
                'required' => 0
            ],[
                'name' => 'Загрузить ресурсную ведомость',
                'input' => '3',
                'type' => '1',
                'group' => '8',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '8',
                'required' => 0
            ],[
                'name' => 'Плановый год проведения работ',
                'input' => '1',
                'type' => '2',
                'group' => '9',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '1',
                'required' => 0
            ],[
                'name' => 'Год последнего капитального ремонта',
                'input' => '1',
                'type' => '2',
                'group' => '9',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '2',
                'required' => 0
            ],[
                'name' => 'По конструктивному исполнению',
                'input' => '2',
                'type' => '2',
                'group' => '9',
                'collection' => 'object',
                'params' => 'Ленточный;Свайный;Плита',
                'sort' => '3',
                'required' => 0
            ],[
                'name' => 'По материалу',
                'input' => '2',
                'type' => '2',
                'group' => '9',
                'collection' => 'object',
                'params' => 'Железобетонный;Блочный;Бутовый',
                'sort' => '4',
                'required' => 0
            ],[
                'name' => 'По категории сложности планируемых работ',
                'input' => '2',
                'type' => '2',
                'group' => '9',
                'collection' => 'object',
                'params' => 'Нормальный;Сложный',
                'sort' => '5',
                'required' => 0
            ],[
                'name' => 'Сметная стоимость капитального ремонта (тыс.рублей)',
                'input' => '1',
                'type' => '2',
                'group' => '9',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '6',
                'required' => 0
            ],[
                'name' => 'Проектная документация',
                'input' => '3',
                'type' => '1',
                'group' => '9',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '7',
                'required' => 0
            ],[
                'name' => 'Сметная документация',
                'input' => '3',
                'type' => '1',
                'group' => '9',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '8',
                'required' => 0
            ],[
                'name' => 'Загрузить ресурсную ведомость',
                'input' => '3',
                'type' => '1',
                'group' => '9',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '9',
                'required' => 0
            ],[
                'name' => 'Отметка об отсутствии теплоснабжения',
                'input' => '4',
                'type' => '2',
                'group' => '10',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '0',
                'required' => 0
            ],[
                'name' => 'Плановый год проведения работ',
                'input' => '1',
                'type' => '2',
                'group' => '10',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '1',
                'required' => 0
            ],[
                'name' => 'Год последнего капитального ремонта',
                'input' => '1',
                'type' => '2',
                'group' => '10',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '2',
                'required' => 0
            ],[
                'name' => 'По материалу исполнения',
                'input' => '2',
                'type' => '2',
                'group' => '10',
                'collection' => 'object',
                'params' => 'Трубопровод стальной;Трубопровод ПП;Смешанный',
                'sort' => '3',
                'required' => 0
            ],[
                'name' => 'По наличию оборудования',
                'input' => '2',
                'type' => '2',
                'group' => '10',
                'collection' => 'object',
                'params' => 'ИТП;ИТП отсутвует',
                'sort' => '4',
                'required' => 0
            ],[
                'name' => 'Сметная стоимость капитального ремонта (тыс.рублей)',
                'input' => '1',
                'type' => '2',
                'group' => '10',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '5',
                'required' => 0
            ],[
                'name' => 'Проектная документация',
                'input' => '3',
                'type' => '1',
                'group' => '10',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '6',
                'required' => 0
            ],[
                'name' => 'Сметная документация',
                'input' => '3',
                'type' => '1',
                'group' => '10',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '7',
                'required' => 0
            ],[
                'name' => 'Загрузить ресурсную ведомость',
                'input' => '3',
                'type' => '1',
                'group' => '10',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '8',
                'required' => 0
            ],[
                'name' => 'Отметка об отсутствии горячего водоснабжения',
                'input' => '4',
                'type' => '2',
                'group' => '11',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '0',
                'required' => 0
            ],[
                'name' => 'Плановый год проведения работ',
                'input' => '1',
                'type' => '2',
                'group' => '11',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '1',
                'required' => 0
            ],[
                'name' => 'Год последнего капитального ремонта',
                'input' => '1',
                'type' => '2',
                'group' => '11',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '2',
                'required' => 0
            ],[
                'name' => 'По материалу исполнения',
                'input' => '2',
                'type' => '2',
                'group' => '11',
                'collection' => 'object',
                'params' => 'Трубопровод стальной;Трубопровод ПП;Смешанный',
                'sort' => '3',
                'required' => 0
            ],[
                'name' => 'По наличию оборудования',
                'input' => '2',
                'type' => '2',
                'group' => '11',
                'collection' => 'object',
                'params' => 'ИТП;ИТП отсутвует',
                'sort' => '4',
                'required' => 0
            ],[
                'name' => 'Сметная стоимость капитального ремонта (тыс.рублей)',
                'input' => '1',
                'type' => '2',
                'group' => '11',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '5',
                'required' => 0
            ],[
                'name' => 'Проектная документация',
                'input' => '3',
                'type' => '1',
                'group' => '11',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '6',
                'required' => 0
            ],[
                'name' => 'Сметная документация',
                'input' => '3',
                'type' => '1',
                'group' => '11',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '7',
                'required' => 0
            ],[
                'name' => 'Загрузить ресурсную ведомость',
                'input' => '3',
                'type' => '1',
                'group' => '11',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '8',
                'required' => 0
            ],[
                'name' => 'Отметка об отсутствии холодного водоснабжения',
                'input' => '4',
                'type' => '2',
                'group' => '12',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '0',
                'required' => 0
            ],[
                'name' => 'Плановый год проведения работ',
                'input' => '1',
                'type' => '2',
                'group' => '12',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '1',
                'required' => 0
            ],[
                'name' => 'Год последнего капитального ремонта',
                'input' => '1',
                'type' => '2',
                'group' => '12',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '2',
                'required' => 0
            ],[
                'name' => 'По материалу исполнения',
                'input' => '2',
                'type' => '2',
                'group' => '12',
                'collection' => 'object',
                'params' => 'Трубопровод стальной;Трубопровод ПП;Смешанный',
                'sort' => '3',
                'required' => 0
            ],[
                'name' => 'Сметная стоимость капитального ремонта (тыс.рублей)',
                'input' => '1',
                'type' => '2',
                'group' => '12',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '4',
                'required' => 0
            ],[
                'name' => 'Проектная документация',
                'input' => '3',
                'type' => '1',
                'group' => '12',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '5',
                'required' => 0
            ],[
                'name' => 'Сметная документация',
                'input' => '3',
                'type' => '1',
                'group' => '12',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '6',
                'required' => 0
            ],[
                'name' => 'Загрузить ресурсную ведомость',
                'input' => '3',
                'type' => '1',
                'group' => '12',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '7',
                'required' => 0
            ],[
                'name' => 'Отметка об отсутствии водоотведения',
                'input' => '4',
                'type' => '2',
                'group' => '13',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '0',
                'required' => 0
            ],[
                'name' => 'Плановый год проведения работ',
                'input' => '1',
                'type' => '2',
                'group' => '13',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '1',
                'required' => 0
            ],[
                'name' => 'Год последнего капитального ремонта',
                'input' => '1',
                'type' => '2',
                'group' => '13',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '2',
                'required' => 0
            ],[
                'name' => 'По материалу исполнения',
                'input' => '2',
                'type' => '2',
                'group' => '13',
                'collection' => 'object',
                'params' => 'Трубопровод стальной;Трубопровод ПП;Смешанный',
                'sort' => '3',
                'required' => 0
            ],[
                'name' => 'Сметная стоимость капитального ремонта (тыс.рублей)',
                'input' => '1',
                'type' => '2',
                'group' => '13',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '4',
                'required' => 0
            ],[
                'name' => 'Проектная документация',
                'input' => '3',
                'type' => '1',
                'group' => '13',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '5',
                'required' => 0
            ],[
                'name' => 'Сметная документация',
                'input' => '3',
                'type' => '1',
                'group' => '13',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '6',
                'required' => 0
            ],[
                'name' => 'Загрузить ресурсную ведомость',
                'input' => '3',
                'type' => '1',
                'group' => '13',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '7',
                'required' => 0
            ],[
                'name' => 'Отметка об отсутствии электроснабжения',
                'input' => '4',
                'type' => '2',
                'group' => '14',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '0',
                'required' => 0
            ],[
                'name' => 'Плановый год проведения работ',
                'input' => '1',
                'type' => '2',
                'group' => '14',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '1',
                'required' => 0
            ],[
                'name' => 'Год последнего капитального ремонта',
                'input' => '1',
                'type' => '2',
                'group' => '14',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '2',
                'required' => 0
            ],[
                'name' => 'По материалу исполнения',
                'input' => '2',
                'type' => '2',
                'group' => '14',
                'collection' => 'object',
                'params' => 'Медь;Алюминий',
                'sort' => '3',
                'required' => 0
            ],[
                'name' => 'Сметная стоимость капитального ремонта (тыс.рублей)',
                'input' => '1',
                'type' => '2',
                'group' => '14',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '4',
                'required' => 0
            ],[
                'name' => 'Проектная документация',
                'input' => '3',
                'type' => '1',
                'group' => '14',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '5',
                'required' => 0
            ],[
                'name' => 'Сметная документация',
                'input' => '3',
                'type' => '1',
                'group' => '14',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '6',
                'required' => 0
            ],[
                'name' => 'Загрузить ресурсную ведомость',
                'input' => '3',
                'type' => '1',
                'group' => '14',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '7',
                'required' => 0
            ],[
                'name' => 'Отметка об отсутствии газоснабжения',
                'input' => '4',
                'type' => '2',
                'group' => '15',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '0',
                'required' => 0
            ],[
                'name' => 'Плановый год проведения работ',
                'input' => '1',
                'type' => '2',
                'group' => '15',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '1',
                'required' => 0
            ],[
                'name' => 'Год последнего капитального ремонта',
                'input' => '1',
                'type' => '2',
                'group' => '15',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '2',
                'required' => 0
            ],[
                'name' => 'Сметная стоимость капитального ремонта (тыс.рублей)',
                'input' => '1',
                'type' => '2',
                'group' => '15',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '3',
                'required' => 0
            ],[
                'name' => 'Проектная документация',
                'input' => '3',
                'type' => '1',
                'group' => '15',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '4',
                'required' => 0
            ],[
                'name' => 'Сметная документация',
                'input' => '3',
                'type' => '1',
                'group' => '15',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '5',
                'required' => 0
            ],[
                'name' => 'Загрузить ресурсную ведомость',
                'input' => '3',
                'type' => '1',
                'group' => '15',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '6',
                'required' => 0
            ],[
                'name' => 'Отметка об отсутствии пожаротушения',
                'input' => '4',
                'type' => '2',
                'group' => '16',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '0',
                'required' => 0
            ],[
                'name' => 'Плановый год проведения работ',
                'input' => '1',
                'type' => '2',
                'group' => '16',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '1',
                'required' => 0
            ],[
                'name' => 'Год последнего капитального ремонта',
                'input' => '1',
                'type' => '2',
                'group' => '16',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '2',
                'required' => 0
            ],[
                'name' => 'Сметная стоимость капитального ремонта (тыс.рублей)',
                'input' => '1',
                'type' => '2',
                'group' => '16',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '4',
                'required' => 0
            ],[
                'name' => 'Проектная документация',
                'input' => '3',
                'type' => '1',
                'group' => '16',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '5',
                'required' => 0
            ],[
                'name' => 'Сметная документация',
                'input' => '3',
                'type' => '1',
                'group' => '16',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '6',
                'required' => 0
            ],[
                'name' => 'Загрузить ресурсную ведомость',
                'input' => '3',
                'type' => '1',
                'group' => '16',
                'collection' => 'object',
                'params' => NULL,
                'sort' => '7',
                'required' => 0
            ]
        ]);

        Model::reguard();
    }
}
